﻿<%@ page title="" language="C#" autoeventwireup="true" inherits="Pages_Quatation, App_Web_tcx0gt55" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>--%>
     <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="assets/Scripts/script.js"></script>
    <%--<link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
     <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <style type="text/css">
        .style1 {
            height: 20px;
        }

        table, th, td {
            border: 1px solid black;
        }

            td:hover {
                background-color: #f5f5f5;
            }

        /*tr:nth-child(even) {background-color: #f2f2f2}
        .auto-style1 {
            width: 13px;
        }*/






        /*TABLE THEME*/
        /*
        
            table a:link {
                color: #666;
                font-weight: bold;
                text-decoration: none;
            }

            table a:visited {
                color: #999999;
                font-weight: bold;
                text-decoration: none;
            }

            table a:active,
            table a:hover {
                color: #bd5a35;
                text-decoration: underline;
            }

        table {
            font-family: Arial, Helvetica, sans-serif;
            color: #666;
            font-size: 15px !important;
            text-shadow: 1px 1px 0px #fff;
            background: #eaebec;
            margin: 20px;
            border: #ccc 1px solid;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            -moz-box-shadow: 0 1px 2px #d1d1d1;
            -webkit-box-shadow: 0 1px 2px #d1d1d1;
            box-shadow: 0 1px 2px #d1d1d1;
        }

            table th {
                padding: 21px 25px 22px 25px;
                border-top: 1px solid #fafafa;
                border-bottom: 1px solid #e0e0e0;
                background: #ededed;
                background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
                background: -moz-linear-gradient(top, #ededed, #ebebeb);
            }

                table th:first-child {
                    text-align: left;
                    padding-left: 20px;
                }

            table tr:first-child th:first-child {
                -moz-border-radius-topleft: 3px;
                -webkit-border-top-left-radius: 3px;
                border-top-left-radius: 3px;
            }

            table tr:first-child th:last-child {
                -moz-border-radius-topright: 3px;
                -webkit-border-top-right-radius: 3px;
                border-top-right-radius: 3px;
            }

            table tr {
                text-align: center;
                padding-left: 20px;
            }

            table td:first-child {
                text-align: left;
                padding-left: 20px;
                border-left: 0;
            }

            table td {
                padding: 12px;
                border-top: 1px solid #ffffff;
                border-bottom: 1px solid #e0e0e0;
                border-left: 1px solid #e0e0e0;
                background: #fafafa;
                background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
                background: -moz-linear-gradient(top, #fbfbfb, #fafafa);
            }

            table tr.even td {
                background: #f6f6f6;
                background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
                background: -moz-linear-gradient(top, #f8f8f8, #f6f6f6);
            }

            table tr:last-child td {
                border-bottom: 0;
            }

                table tr:last-child td:first-child {
                    -moz-border-radius-bottomleft: 3px;
                    -webkit-border-bottom-left-radius: 3px;
                    border-bottom-left-radius: 3px;
                }

                table tr:last-child td:last-child {
                    -moz-border-radius-bottomright: 3px;
                    -webkit-border-bottom-right-radius: 3px;
                    border-bottom-right-radius: 3px;
                }

            table tr:hover td {
                background: #f2f2f2;
                background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
                background: -moz-linear-gradient(top, #f2f2f2, #f0f0f0);
            }

            table td:hover {
                box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,5);
            }
            */


        /*specific theme*/

        td {
            font-size: 14px !important;
        }

        .W3_Title {
            color: #666;
            font-weight: bold !important;
            font-size: 20px !important;
            text-decoration: none;
        }

        .W3_Header {
            color: #666;
            font-weight: bold !important;
            font-size: 30px !important;
            text-decoration: none;
            text-align: center !important;
        }

        .W3_Warning {
            color: #C7310C;
            font-weight: bold !important;
            font-size: 20px !important;
            text-decoration: none;
            text-align: center !important;
        }

        /*PRINT DIV*/
        @media print {
            .myDivToPrint {
                background-color: white;
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                margin: 0;
                padding: 15px;
                font-size: 14px;
                line-height: 18px;
            }
        }

         #scrolly{
            width: 1000px;
            height: 190px;
            overflow: auto;
            overflow-y: hidden;
            margin: 0 auto;
            white-space: nowrap
        }
    </style>

    <script language="javascript" type="text/javascript">
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head><title></title></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;


        }

        //Enable Disable Textbox for EDIT D2Remark
        function EnableDisableTextBoxD2Remark(chkPassport) {
            if (chkPassport.checked == true) {
                document.getElementById("MainContent_txtD2Remark").disabled = false;
            }
            else {
                document.getElementById("MainContent_txtD2Remark").disabled = true;
            }
        }

    </script>

    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section id="main-content">
        <section class="wrapper">
                <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
    </telerik:RadStyleSheetManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
    <center>
        <div style="padding:10px; text-align:end;"><input type="button" value="Print" onclick="javascript: printDiv('FormDiv')" /></div>

        <div style="text-align:left; font-size:large"><a href="CARListAuditSpecific.aspx" runat="server" id="BackID"><b><u>Back</u></b></a></div>
 <div id="FormDiv" class ="divForm myDivToPrint" style="padding: 1px; border: thin solid #000000; position: relative; " align="center"  >
        

      

     <table id="Table1" width="100%" runat="server" style="overflow-x:scroll;  table-layout:fixed;"  >
         <tr>
             <td colspan="5" align="center" class="W3_Header">NCCA</td>
             
         </tr>
         <tr id="CAR" runat="server" visible="true">
             <%--<td align="right" colspan="5">
                 <a href="Dashboard.aspx">Manage CAR</a>
             </td>--%>
             <td colspan="5">
                  
             </td>
         </tr>
        
         <tr>
             <td colspan="5">
                 <telerik:RadGrid ID="RadGrid1" runat="server"  ClientSettings-Scrolling-AllowScroll="true" Height="100px" Skin="Simple">
                 </telerik:RadGrid>
             </td>
         </tr>
          <tr>
             <td colspan="5"> 
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                 <asp:Label ID="Label1" runat="server" Text="Object Evidence: " Font-Bold="true"></asp:Label>
                  <asp:Label ID="lblObjectEvidence" runat="server"></asp:Label>
                        </td>
         </tr>

         <tr>
             <td colspan="5" class="W3_Warning">(D7/Completion Date) :
                 <asp:Label ID="lblResolutionDate" runat="server"></asp:Label>
             </td>
         </tr>

         <tr>
             <td rowspan="9" >&nbsp;</td>
             <td align="justify"> D1 Target Date:
                 <asp:Label ID="lblD1TargetDate" runat="server"></asp:Label>
             </td>
             <td align="justify" colspan="2"> D2 Target Date: 
                 <asp:Label ID="lblD2TargetDate" runat="server"></asp:Label>
             </td>
             <td align="justify">
                 D3 Target Date: 
                        <asp:Label ID="lblD3TargetDate" runat="server"></asp:Label>
                        </td>
         </tr>
         <tr>
              <td align="justify"> D4 Target Date:
                 <asp:Label ID="lblD4TargetDate" runat="server"></asp:Label>
             </td>
             <td align="justify" colspan="2"> D5 Target Date: 
                 <asp:Label ID="lblD5TargetDate" runat="server"></asp:Label>
             </td>
             <td align="justify">
                 D6 Target Date: 
                        <asp:Label ID="lblD6TargetDate" runat="server"></asp:Label>
                        </td>
             <%--<td align="justify"> Severity Rating : </td>
             <td align="justify" colspan="2"> &nbsp;<asp:Label ID="lblCARSeverityRating" runat="server"></asp:Label>
             </td>
             <td>&nbsp;</td>--%>
         </tr>
         <tr>
             <td align="justify"> D7 Target Date: 
                 <asp:Label ID="lblD7TargetDate" runat="server"></asp:Label>
             </td>
             <td align="justify" colspan="2">&nbsp;</td>
             <td>
                 &nbsp;</td>
         </tr>
         <%--<tr >
             <td align="justify"> D3 Verification:
                 <asp:Label ID="lblD3VerificationNo" runat="server"></asp:Label>
             </td>
             <td align="justify" colspan="2"> D4 Verification:
                 <asp:Label ID="lblD4VerificationNo" runat="server"></asp:Label>
             </td>
             <td align="justify">
                 D5 Verification:
                 <asp:Label ID="lblD5VerificationNo" runat="server"></asp:Label>
                        </td>
         </tr>--%>
         <tr>
             <td align="justify"> Car State : </td>
             <td align="justify" colspan="2">  
                 <asp:Label ID="lblCARStatus" runat="server"></asp:Label>
             </td>
             <td>
                 &nbsp;</td>
         </tr>
         <tr>
            <%-- <td align="justify"> Auditee : </td>
             <td align="justify" colspan="2">  
                 <asp:Label ID="lblCarChampion" runat="server"></asp:Label>
             </td>
             <td>
                    &nbsp;</td>--%>
         </tr>
         <tr>
             <td align="justify"> HOD : </td>
             <td align="justify" colspan="2">  
                 <asp:Label ID="lblCARVerifier" runat="server"></asp:Label>
             </td>
             <td>
                    &nbsp;</td>
         </tr>
         <tr>
             <td align="justify" class="style1"> Auditor : </td>
             <td align="justify" class="style1" colspan="2"> &nbsp;<asp:Label ID="lblCARPrimaryInvestigator" runat="server"></asp:Label>
             </td>
             <td class="style1"></td>
         </tr>
         <tr>
             <td align="justify"> MR : </td>
             <td align="justify" colspan="2"> &nbsp;<asp:Label ID="lblCARSecondaryInvestigator" runat="server"></asp:Label>
             </td>
             <td>&nbsp;</td>
         </tr>
         







         <tr>
            <%-- <td colspan="4" align="justify"> 
                 <asp:Label ID="lblMeasurableGoalStatement" runat="server" Text=""></asp:Label>
             </td>--%>
         </tr>

          <tr>
             
                  <td rowspan="3" class="W3_Title">D1</td>
              <td align="justify"> Auditee : </td>
             <td align="justify" colspan="2">  
                 <asp:Label ID="lblCarChampion" runat="server"></asp:Label>
             </td>
             <td>
                    &nbsp;</td>
         </tr>
          <tr>
            
              <%--<td colspan="4"></td>--%>
              <%--<td colspan="4"></td>--%>
             <td colspan="2" align="justify">Non Confirmative Statement :</td>
             <td>&nbsp;<asp:Label ID="lblD1" runat="server" Text="" ForeColor="Red"></asp:Label></td>
             <td align="left">
                 <asp:TextBox ID="txtD1CompletionDate" runat="server" CssClass="WebHR_TextBox" Height="16px" Visible="false"></asp:TextBox>
                  <%--<ajaxToolkit:CalendarExtender ID="txtD1CompletionDate_CalendarExtender" runat="server" TargetControlID="txtD1CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </ajaxToolkit:CalendarExtender>--%>
                  <cc1:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="txtD1CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
             </td>
         </tr>
        
         <tr>
             <td colspan="4" align="justify">
                 <asp:TextBox ID="txtD1Remark" runat="server" Height="100%" TextMode="MultiLine" Width="100%" Enabled="true"></asp:TextBox>
             </td>
         </tr>
           <tr>
             <td colspan="5" align="right">
                 <asp:LinkButton ID="LinkButton7" runat="server" OnClick="LinkButton7_Click" >Update</asp:LinkButton>
             </td>
         </tr>
         <tr>
             <td rowspan="2" class="W3_Title">D2</td>
             <td align="justify" colspan="2">CAR ­ Additional Problem Identification Information :</td>
             <td >
                 &nbsp;<asp:Label ID="lblD2" runat="server" Text="" ForeColor="Red"></asp:Label>
                 <%--<label for="chkPassport"><input type="checkbox" id="chkPassport" onclick="EnableDisableTextBoxD2Remark(this)" /> Update</label>--%>
             </td>             
             
             <td align="left">Completed Date :<asp:TextBox ID="txtD2CompletionDate" runat="server" CssClass="WebHR_TextBox" Height="16px"></asp:TextBox>
                 <%-- <ajaxToolkit:CalendarExtender ID="txtD2CompletionDate_CalendarExtender" runat="server" TargetControlID="txtD2CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </ajaxToolkit:CalendarExtender>--%>
                   <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtD2CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
             </td>
         </tr>
         <tr>
             <td colspan="4" align="justify" class="w3-text">
                 <%--<asp:TextBox ID="txtD2Remark" runat="server" Height="100%" TextMode="MultiLine" Width="100%" Enabled="false"></asp:TextBox>--%>
                 <asp:TextBox ID="txtD2Remark" runat="server" Height="100%" TextMode="MultiLine" Width="100%" ></asp:TextBox>
             </td>
         </tr>
         <tr>
             <td colspan="5" align="right">
                 <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Update</asp:LinkButton>
             </td>
         </tr>
         <tr>
             <td rowspan="2" class="W3_Title">D3</td>
             <td colspan="2" align="justify">Containment Action and Short Term Corrective Action :</td>
             <td>&nbsp;<asp:Label ID="lblD3" runat="server" Text="" ForeColor="Red"></asp:Label></td>
             <td align="left">Completed Date :
                 <asp:TextBox ID="txtD3CompletionDate" runat="server" CssClass="WebHR_TextBox" Height="16px"></asp:TextBox>
                 <%-- <ajaxToolkit:CalendarExtender ID="txtD3CompletionDate_CalendarExtender" runat="server" TargetControlID="txtD3CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </ajaxToolkit:CalendarExtender>--%>
                  <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtD3CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
             </td>
         </tr>
         <tr>
             <td colspan="4" align="justify">
                 <asp:TextBox ID="txtD3Remark" runat="server" Height="100%" TextMode="MultiLine" Width="100%" Enabled="true"></asp:TextBox>
             </td>
         </tr>
         <tr>
             <td colspan="5" align="right">
                 <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">Update</asp:LinkButton>
             </td>
         </tr>
         <tr>
             <td class="W3_Title" rowspan="2">D4</td>
             <td align="justify" colspan="2">Define and Verify Root Cause :</td>
             <td>&nbsp;<asp:Label ID="lblD4" runat="server" Text="" ForeColor="Red"></asp:Label></td>
             <td align="left">Completed Date :
                 <asp:TextBox ID="txtD4CompletionDate" runat="server" CssClass="WebHR_TextBox" Height="16px"></asp:TextBox>
                  <%--<ajaxToolkit:CalendarExtender ID="txtD4CompletionDate_CalendarExtender" runat="server" TargetControlID="txtD4CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </ajaxToolkit:CalendarExtender>--%>
                 <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtD4CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
             </td>
         </tr>
         <tr>
             <td colspan="4" align="justify">
                 <asp:TextBox ID="txtD4Remark" runat="server" Height="100%" TextMode="MultiLine" Width="100%" Enabled="true"></asp:TextBox>
             </td>
         </tr>
         <tr>
             <td colspan="5" align="right">
                 <asp:LinkButton ID="LinkButton3" runat="server" OnClick="LinkButton3_Click">Update</asp:LinkButton>
             </td>
         </tr>
         <tr>
             <td class="W3_Title" rowspan="2">D5</td>
             <td align="justify" colspan="2">Choose and Verify Solution :</td>
             <td>
                 &nbsp;<asp:Label ID="lblD5" runat="server" Text="" ForeColor="Red"></asp:Label>
             </td>
             <td align="left">Completed Date :
                 <asp:TextBox ID="txtD5CompletionDate" runat="server" CssClass="WebHR_TextBox" Height="16px"></asp:TextBox>
                  <%--<ajaxToolkit:CalendarExtender ID="txtD5CompletionDate_CalendarExtender" runat="server" TargetControlID="txtD5CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </ajaxToolkit:CalendarExtender>--%>
                 <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtD5CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
             </td>
         </tr>
         <tr>
             <td colspan="4" align="justify">
                 <asp:TextBox ID="txtD5Remark" runat="server" Height="100%" TextMode="MultiLine" Width="100%" Enabled="true"></asp:TextBox>
             </td>
         </tr>
         <tr>
             <td colspan="5" align="right">
                 <asp:LinkButton ID="LinkButton4" runat="server" OnClick="LinkButton4_Click">Update</asp:LinkButton>
             </td>
         </tr>
         <tr>
             <td class="W3_Title" rowspan="2">D6</td>
             <td align="justify" colspan="2">Implement Permanent Corrective Action :</td>
             <td>&nbsp;<asp:Label ID="lblD6" runat="server" Text="" ForeColor="Red"></asp:Label></td>
             <td align="left">Completed Date :
                 <asp:TextBox ID="txtD6CompletionDate" runat="server" CssClass="WebHR_TextBox" Height="16px"></asp:TextBox>
                  <%--<ajaxToolkit:CalendarExtender ID="txtD6CompletionDate_CalendarExtender" runat="server" TargetControlID="txtD6CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </ajaxToolkit:CalendarExtender>--%>
                 <cc1:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txtD6CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
             </td>
         </tr>
         <tr>
             <td colspan="4" align="justify">
                 <asp:TextBox ID="txtD6Remark" runat="server" Height="100%" TextMode="MultiLine" Width="100%" Enabled="true"></asp:TextBox>
             </td>
         </tr>
         <tr>
             <td colspan="5" align="right">
                 <asp:LinkButton ID="LinkButton5" runat="server" OnClick="LinkButton5_Click">Update</asp:LinkButton>
             </td>
         </tr>
         <tr>
             <td class="W3_Title" rowspan="2">D7</td>
             <td align="justify" colspan="2">Prevent Recurrence :</td>
             <td>&nbsp;<asp:Label ID="lblD7" runat="server" Text="" ForeColor="Red"></asp:Label></td>
             <td align="left">Completed Date :
                 <asp:TextBox ID="txtD7CompletionDate" runat="server" CssClass="WebHR_TextBox" Height="16px"></asp:TextBox>
                 <%-- <ajaxToolkit:CalendarExtender ID="txtD7CompletionDate_CalendarExtender" runat="server" TargetControlID="txtD7CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </ajaxToolkit:CalendarExtender>--%>
                 <cc1:CalendarExtender ID="CalendarExtender6" runat="server" TargetControlID="txtD7CompletionDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
             </td>
         </tr>
        <%-- <tr style="visibility:hidden;">
             <td align="justify" colspan="2">Classify Root Cause Ownership:</td>
             <td>&nbsp;</td>
             <td>
                 <asp:RadioButton ID="RadioButtonExternalSupplier" runat="server" Text="External/Supplier" GroupName="InternalExternal" />
                 <asp:RadioButton ID="RadioButtonInternalDeere" runat="server" Text="Internal/Deere" GroupName="InternalExternal" />
             </td>
         </tr>--%>
         <tr>
             <td colspan="4" align="justify">
                 <asp:TextBox ID="txtD7Remark" runat="server" Height="100%" TextMode="MultiLine" Width="100%" Enabled="true"></asp:TextBox>
             </td>
         </tr>
         <tr>
             <td align="justify" colspan="5">Comment :
                 
             </td>
         </tr>
         <tr>
             <td colspan="5" align="justify">
                 <asp:TextBox ID="txtD7Comment" runat="server" Height="100%" TextMode="MultiLine" Width="100%" Enabled="true"></asp:TextBox>
             </td>
         </tr>
         <tr>
             <td colspan="5" align="right">
                 <asp:LinkButton ID="LinkButton6" runat="server" OnClick="LinkButton6_Click">Update</asp:LinkButton>
             </td>
         </tr>
         <%--<asp:Label ID="lblUpdate" runat="server" Text="" ></asp:Label>--%>
          <tr>
             <td colspan="5" align="center">
                 <asp:Label ID="lblUpdate" runat="server" Text="All the details updated successfully." ForeColor="Green" Font-Size="Large" Font-Bold="true" Visible="false"></asp:Label>
             </td>
         </tr>
          <tr>
             <td colspan="5" align="center">
                 <asp:Label ID="lblCarStateFinal" runat="server" Text="CAR STATE: Pending " ForeColor="Blue" Font-Size="Large" Font-Bold="true"></asp:Label>
             </td>
         </tr>



         <tr>
             <td  colspan="5" align="center" style="height:auto">&nbsp;
                  <asp:Panel ID="pnlUpload" runat="server" Width="50%" BorderColor="Black">
                    <asp:FileUpload ID="FileUploadControl" runat="server" Enabled="false" />
                      <asp:Label ID="Label2" runat="server" Text="." Font-Bold="True" 
                        ForeColor="White"></asp:Label><br />
                    <asp:Button ID="btnUpload" runat="server" onclick="btnUpload_Click"  
                        Text="Upload" Width="70px" Height="30px" Font-Bold="true" Enabled="false" />
                    <asp:Label ID="StatusLabel" runat="server" Text="Upload Status" Font-Bold="True" 
                        ForeColor="#3333FF"></asp:Label>
                 </asp:Panel>&nbsp;
             </td>
         </tr>

         <tr>
             <td colspan="5" align="center" style="height:auto;">&nbsp;
                  <asp:Panel ID="pnlUser" runat="server" Width="90%" >
                       <%-- <table style="width: 100%;">
                            <tr>
                                <td>--%>
                      <asp:TextBox ID="txtRemarkAuditee" runat="server"  Text="Auditee Remark:"  TextMode="MultiLine"  Width="100%" ></asp:TextBox>
                      <asp:Label ID="lblSendForApproval" runat="server" Text="Status" Font-Bold="true" Font-Size="Medium" ForeColor="Green"  ></asp:Label>
                      <br />

                                    <asp:Button ID="btnSendforApproval" runat="server"  
                                        onclick="btnSendforApproval_Click" OnClientClick="javascript:ShowOverlay();"  Text="Send for Approval" Width="142px"  Height="30px" Font-Bold="true" Enabled="false" /><br />
                                    <asp:TextBox ID="txtDelayedReasonByAssignee" runat="server"  Text="Delayed Reason:" Visible="false" TextMode="MultiLine"  ></asp:TextBox><br />
                      
                               <%-- </td>
                            </tr>
                        </table>--%>
                    </asp:Panel>
             </td>
         </tr>

         <tr>
             <td colspan="5" align="center" style="height:auto">&nbsp;
                  <asp:Panel ID="PanelApprove" runat="server" Width="90%" >
                       <%-- <table style="width: 100%;">
                            <tr>
                                <td>--%>
                      <asp:TextBox ID="txtRemarkAuditor" runat="server"  Text="Approver Remark:"  TextMode="MultiLine" Width="100%"  ></asp:TextBox>
                      <asp:Label ID="lblApproverRemark" runat="server" Text="Status" Font-Bold="true" Font-Size="Medium" ForeColor="Green" ></asp:Label>
                      <br />
                                   <asp:Button ID="btnApproved" runat="server" onclick="btnApproved_Click" OnClientClick="javascript:ShowOverlay();"  
                                        Text="Approve" Width="70px" Height="30px" Font-Bold="true" Enabled="false" />
                      <asp:Button ID="btnReject" runat="server" onclick="btnReject_Click"  OnClientClick="javascript:ShowOverlay();"  
                                        Text="Reject"  Width="70px" Height="30px" Font-Bold="true" Enabled="false" />
                               
                                    <asp:TextBox ID="txtRejectReason" runat="server" visible="false" Text="Reason:"></asp:TextBox><%--<br />--%>
                               
                    </asp:Panel>&nbsp;
             </td>
         </tr>



         <tr>
             <td colspan="5" align="center" style="height:auto;">
                         <asp:GridView ID="RadGrid2" runat="server" AutoGenerateColumns="False" 
                           
                            Width="100%" onrowcommand="RadGrid2_RowCommand"  
                            onselectedindexchanged="RadGrid2_SelectedIndexChanged"  BorderStyle="Solid" 
                            BorderColor="#333333" BorderWidth="1px" Font-Names="Verdana" 
                            Font-Overline="False" Font-Size="12px" GridLines="None" ShowFooter="True" 
                            onprerender="RadGrid2_PreRender"  EnableViewState="false">
                            <AlternatingRowStyle BackColor="#EBEBEB" Font-Names="Verdana" Font-Size="16px" 
                                Height="30px" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <asp:BoundField DataField="Name" HeaderText="File Name" >
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:HyperLinkField DataNavigateUrlFields="Path1" text ="Download"
                                    DataNavigateUrlFormatString="{0}"  HeaderText="Download" >
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:HyperLinkField>
                                <asp:HyperLinkField DataNavigateUrlFields="Path2" text ="Remove"
                                    DataNavigateUrlFormatString="{0}"  HeaderText="Remove" >
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:HyperLinkField>
                                <asp:BoundField DataField="Status" HeaderText="Status" Visible="false" >
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ModifiedDate" HeaderText="Uploaded Date" >
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DirectoryId" HeaderText="DirectoryId" 
                                    Visible="False" >
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ActivityRemark" HeaderText="Reason" Visible="false" >
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle BackColor="#EBEBEB" BorderStyle="Solid" BorderWidth="2px" 
                                Font-Names="Verdana" Height="30px" BorderColor="#333333" />
                            <PagerSettings Mode="NumericFirstLast" />
                        </asp:GridView>
        
             </td>
         </tr>







      <%--   <tr>
             <td >&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
         </tr>
         <tr>
             <td >&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
         </tr>
         <tr>
             <td >&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
         </tr>
         <tr>
             <td >&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
         </tr>
         <tr>
             <td >&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
         </tr>--%>
         <tr>
             <td colspan="5" >&nbsp;</td>
            <%-- <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>--%>
         </tr>
     </table>
        



    </div>
    
     <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
    </telerik:RadWindowManager>
        <telerik:RadSkinManager ID="RadSkinManager1" Runat="server" ShowChooser="false">
        </telerik:RadSkinManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
     
    </center>
            </section>
        </section>

 </asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
 </asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
 </asp:Content>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">


</asp:Content>--%>


<%--<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server" >
    
</asp:Content>--%>

