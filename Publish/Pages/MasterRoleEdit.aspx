﻿<%@ page title="" language="C#" masterpagefile="~/Pages/Admin.master" autoeventwireup="true" inherits="Pages_MasterRoleEdit, App_Web_ok35y50u" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script runat="server">

    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">
    <%-- <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
      <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
        <style type="text/css">
        .rgRow, .rgAltRow {
            height: 40px;
        }
    </style>
    <script type="text/javascript">
        //<![CDATA[
        function ParameterMap(sender, args) {
            //If you want to send a parameter to the select call you can modify the if 
            //statement to check whether the request type is 'read':
            //if (args.get_type() == "read" && args.get_data()) {
            if (args.get_type() != "read" && args.get_data()) {
                args.set_parameterFormat({ customersJSON: kendo.stringify(args.get_data().models) });
            }
        }

        function Parse(sender, args) {
            var response = args.get_response().d;
            if (response) {
                args.set_parsedData(response.Data);
            }
        }

        function UserAction(sender, args) {
            if (sender.get_batchEditingManager().hasChanges(sender.get_masterTableView()) &&
                !confirm("Any changes will be cleared. Are you sure you want to perform this action?")) {
                args.set_cancel(true);
            }
        }
        //]]>
    </script>

    
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
  
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="WebBlue" />
    
        <telerik:RadWindowManager ID="rmw1" runat="server">
        </telerik:RadWindowManager>

    <section id="main-content">
        <section class="wrapper">
            
            <br/>
              <br/>

              <div style="padding-top: 20px; padding-left: 10px">
            </div>
            <div style="align-items: center; text-align: center;">
                <asp:Label ID="Label1" Font-Size="Large" Font-Bold="true" runat="server" CssClass="WebHR_Heading1"
                    Text="MANAGE ROLE MASTER"></asp:Label>
            </div>

            <br/>
              <br/>
             <div class="demo-container no-bg">
        <%--<h3>RadGrid bound to RadClientDataSource</h3>--%>
        <div id="grid">
            <telerik:RadGrid RenderMode="Lightweight" ID="rgvRoleMaster" runat="server"  AllowPaging="true" AllowSorting="true" AllowFilteringByColumn="true" AutoGenerateColumns="false"  OnNeedDataSource="rgvRoleMaster_NeedDataSource"   OnBatchEditCommand="rgvRoleMaster_BatchEditCommand" OnItemDataBound="rgvRoleMaster_ItemDataBound" OnItemCreated="rgvRoleMaster_ItemCreated"   ><%--ClientDataSourceID="RadClientDataSource1"--%>
                <ClientSettings AllowKeyboardNavigation="true">
                            <KeyboardNavigationSettings AllowActiveRowCycle="true"/>

                            <Selecting AllowRowSelect="true"/>
                        </ClientSettings>
                   <GroupingSettings CaseSensitive="false"></GroupingSettings>
                 <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" IgnorePaging="true" FileName="Role">
                </ExportSettings>
                <MasterTableView ClientDataKeyNames="RoleCode" EditMode="Batch" CommandItemDisplay="Top" DataKeyNames="RoleCode"  > <%--BatchEditingSettings-HighlightDeletedRows="true"--%>
                    <BatchEditingSettings EditType="Cell" />
                     <CommandItemSettings ShowExportToCsvButton="true" ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" />
                    
                    <Columns>
                        <telerik:GridBoundColumn DataField="RoleCode" HeaderText="Role Code"  UniqueName="RoleCode" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ReadOnly="true"  >
                            <ColumnValidationSettings EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" Text="*This field is required" Display="Dynamic">
                            </RequiredFieldValidator>
                        </ColumnValidationSettings>
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="RoleDescription" HeaderText="Role Description"  UniqueName="RoleDescription" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ReadOnly="true" >
                            <ColumnValidationSettings EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="Red" Text="*This field is required" Display="Dynamic">
                            </RequiredFieldValidator>
                        </ColumnValidationSettings>
                        </telerik:GridBoundColumn>

                        <%--<telerik:GridTemplateColumn HeaderText="Category" DefaultInsertValue="a" HeaderStyle-Width="150px" UniqueName="DdlValue" DataField="DdlValue">
                        <ItemTemplate>
                            <%# Eval("DdlValue") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ID="CategoryIDDropDown" DataValueField="DdlValue"
                                DataTextField="DdlValue" DataSourceID="SqlDataSource2">
                            </telerik:RadDropDownList>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>--%>
                        
                        <telerik:GridClientDeleteColumn HeaderText="Delete" Visible="false">
                            <HeaderStyle Width="70px" />
                        </telerik:GridClientDeleteColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings>
                    <ClientEvents OnUserAction="UserAction" />
                </ClientSettings>
            </telerik:RadGrid>
        </div>
        <telerik:GridTextBoxColumnEditor ID="GridTextBoxEditor" runat="server" TextBoxStyle-Width="230px"></telerik:GridTextBoxColumnEditor>

   <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT [DdlValue] FROM [tbl_DdlValue]"></asp:SqlDataSource>

    </div>

            </section>
        </section>

</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">

</asp:Content>
   
