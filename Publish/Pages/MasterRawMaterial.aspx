﻿<%@ page title="" language="C#" masterpagefile="~/Pages/Admin.master" autoeventwireup="true" inherits="Pages_MasterRawMaterial, App_Web_10kffnjd" maintainscrollpositiononpostback="true" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script runat="server">
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">

  <%--  <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
    <style type="text/css">
        .rgRow, .rgAltRow
        {
            height: 40px;
        }
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script type="text/javascript">
        
        function UserAction(sender, args) {
            if (sender.get_batchEditingManager().hasChanges(sender.get_masterTableView()) &&
                !confirm("Any changes will be cleared. Are you sure you want to perform this action?")) {
                args.set_cancel(true);
            }
        }

        function keyPress(sender, args) {
            var text = sender.get_value() + args.get_keyCharacter();
            if (!text.match('^[0-9]+$'))
                args.set_cancel(true);
        }

    </script>

    
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>

    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="WebBlue" />

    <telerik:RadWindowManager ID="rmw1" runat="server">
    </telerik:RadWindowManager>

    <section id="main-content">
        <section class="wrapper">

            <br />
            <br />
                        <div style="padding-top: 20px; padding-left: 10px">
            </div>
            <div style="align-items: center; text-align: center;">
                <asp:Label ID="Label1" Font-Size="Large" Font-Bold="true" runat="server" CssClass="WebHR_Heading1"
                    Text="Manage Raw Material Master"></asp:Label>
            </div>

            <br />
            <br />
                      
                <div class="demo-container no-bg">
                    <%--<h3>RadGrid bound to RadClientDataSource</h3>--%>
                     <asp:Label ID="Label16" runat="server" Text="(Note : (*) fields are Mandatory)" ForeColor="Red"></asp:Label>
                                    <br />
                    <div id="grid">
                        <telerik:RadGrid RenderMode="Lightweight" ID="rgvRawMaterialMaster" runat="server" AllowPaging="true" AllowSorting="true" AllowFilteringByColumn="true" AutoGenerateColumns="false" OnNeedDataSource="rgvRawMaterialMaster_NeedDataSource" OnBatchEditCommand="rgvRawMaterialMaster_BatchEditCommand" OnItemDataBound="rgvRawMaterialMaster_ItemDataBound" OnItemCreated="rgvRawMaterialMaster_ItemCreated">
                              <ClientSettings AllowKeyboardNavigation="true">
                            <KeyboardNavigationSettings AllowActiveRowCycle="true"/>

                            <Selecting AllowRowSelect="true"/>
                        </ClientSettings>
                               <GroupingSettings CaseSensitive="false"></GroupingSettings>
                            <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" IgnorePaging="true" FileName="Project Master">
                            </ExportSettings>
                            <MasterTableView ClientDataKeyNames="Raw_Material_ID" EditMode="Batch" CommandItemDisplay="Top" DataKeyNames="Raw_Material_ID">
                                <%--BatchEditingSettings-HighlightDeletedRows="true"--%>
                                <BatchEditingSettings EditType="Cell" />
                                <CommandItemSettings ShowExportToCsvButton="true" ShowExportToExcelButton="true" />

                                <Columns>


                                    <telerik:GridBoundColumn DataField="Raw_Material_ID" HeaderText="Raw_Material_ID(*)" UniqueName="Raw_Material_ID" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor" Visible="false">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ForeColor="Red" ErrorMessage="*This field is required"></RequiredFieldValidator>
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="Raw_Material" HeaderText="Raw Material(*)" UniqueName="Raw_Material" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ForeColor="Red" ErrorMessage="*This field is required"></RequiredFieldValidator>
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="Raw_Material_Description" HeaderText="Description(*)" UniqueName="Raw_Material_Description" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ForeColor="Red" ErrorMessage="*This field is required"></RequiredFieldValidator>
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>
                                    


                                    <telerik:GridClientDeleteColumn HeaderText="Delete">
                                        <HeaderStyle Width="70px" />
                                    </telerik:GridClientDeleteColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <ClientEvents OnUserAction="UserAction" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                    <telerik:GridTextBoxColumnEditor ID="GridTextBoxEditor" runat="server" TextBoxStyle-Width="175px"></telerik:GridTextBoxColumnEditor>

                
                </div>
        </section>
    </section>

</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
</asp:Content>

