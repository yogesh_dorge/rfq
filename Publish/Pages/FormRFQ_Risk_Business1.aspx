﻿<%@ page language="C#" autoeventwireup="true" maintainscrollpositiononpostback="true" masterpagefile="~/Pages/Admin.master" inherits="FormRFQ_Risk_Business1, App_Web_3kw0abyc" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--  <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%-- <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Styles/semantic.css" rel="stylesheet" />
    <link href="../Styles/semantic.min.css" rel="stylesheet" />
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/style-responsive.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
     

    </center>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <br />

    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="WebBlue" />

    <section id="main-content">
        <section class="wrapper">
            <div class="divForm" style="padding: 1px; border: thin solid #000000; position: relative; width: 100%; height:100%;"
                align="center">
                <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
                    <Localization OK="Ok" Cancel="No" />
                </telerik:RadWindowManager>
                <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                </telerik:RadScriptManager>

                <div>
                    <asp:Label ID="Label1" runat="server" Text="Label" ForeColor="WhiteSmoke"></asp:Label>
                </div>

                <div class="ui last container">
                    <div class="ui five steps">
                        <div id="Activestep" runat="server">
                            <div class="content">
                                <div class="title" id="rfqtitle" runat="server">RFQ Generation</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lblRFQcreated" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="step" id="AssignUser_Title" runat="server">
                            <div class="content">
                                <div class="title">Assign Owner</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_AssignUser_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="OwnerAvability_Title" runat="server">
                            <div class="content">
                                <div class="title">Component Feasibility</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_OwnerAvability_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="Last_Step_Title" runat="server">
                            <div class="content">
                                <div class="title" runat="server">Quotation</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_Last_Step_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                         <div class="step" id="Approval" runat="server">
                            <div class="content">
                                <div id="Div2" class="title" runat="server">Approval</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_Approval" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <br />
                    <div>
                    <asp:Label ID="Label2" runat="server" Text="Label" ForeColor="WhiteSmoke"></asp:Label>

                         <asp:Label ID="Label3" runat="server" Text="Customer Name: "></asp:Label>
                      <asp:Label ID="lblCustomerName" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp  &nbsp  &nbsp
                      <asp:Label ID="Label5" runat="server" Text="Project: "></asp:Label>
                      <asp:Label ID="lblProduct" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp  &nbsp  &nbsp
                      <asp:Label ID="Label7" runat="server" Text="Product or Component: "></asp:Label>
                      <asp:Label ID="lblSubProduct" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp  &nbsp  &nbsp
                      <asp:Label ID="Label10" runat="server" Text="Remark: "></asp:Label>
                      <asp:Label ID="lblRemark" runat="server" Text="" Font-Bold="true"></asp:Label>

                </div>

                 <div>
                    <asp:Label ID="Label4" runat="server" Text="Label" ForeColor="WhiteSmoke"></asp:Label>
                     </div>

                <div style="text-align: center; float: left; width: 50%; height: 50%; border-right-color: skyblue; border-right: groove; font-family: 'Bookman Old Style';">

                    <asp:Label ID="Label9" runat="server" Font-Bold="true" Font-Size="X-Large" ForeColor="DarkBlue" Text="Business Analysis" CssClass="WebHR_Heading1"></asp:Label>
                    <br />
                    <br />
                    <telerik:RadTextBox ID="txtbusiness" runat="server" Height="200Px" Width="50%" TextMode="MultiLine"></telerik:RadTextBox><br />
                    <br />
                    <div style="text-align:right;">
                        <asp:Button ID="btn_Update" runat="server" CssClass="btn btn-success" OnClick="btn_Update_Click"
                           Text="Update" style="text-align:right" />
                    </div>
                    
                </div>


                <div style="text-align: center; float: right; width: 50%; height: 50%; border-left-color: skyblue; border-left: groove; font-family: 'Bookman Old Style'">
                    <asp:Label ID="Label18" runat="server" Font-Bold="true" Font-Size="X-Large" ForeColor="DarkBlue" Text="Risk Analysis" CssClass="WebHR_Heading1"></asp:Label>
                    <br />
                    <br />
                    <telerik:RadTextBox ID="txt_Risk" runat="server" Height="200Px" Width="50%" TextMode="MultiLine"></telerik:RadTextBox><br />
                    <br />
                     <div style="text-align:left;">
                     <asp:Button ID="btnSave" runat="server" CssClass="btn btn-success" OnClick="btnSave_Click1"
                        Text="Confirm" />
                         </div>

                        <telerik:RadWindow ID="RW_Approve" CssClass="RadWindow_Top" runat="server"  Height="180px" Skin="Sunset" Modal="true" VisibleStatusbar="false" Behaviors="Close">
                        <ContentTemplate>
                            <p style="text-align: center; font-family: Bookman Old Style; font-style: italic; font-size: medium">
                               Are you sure you want to Confirm?
                            </p>
                            <br />
                            <table width="80%" align="center" style="background-color: #FFF">

                                <tr id="Tr1" align="center" runat="server" style="background-color: #FFF">
                                    <td>
                                        <telerik:RadButton ID="btn_Yes_Confirm" runat="server" Text="Yes" OnClientClick="javascript:ShowOverlay();" RenderMode="Lightweight" Font-Bold="true" OnClick="btn_Yes_Confirm_Click" Width="30%" />
                                        <telerik:RadButton runat="server" ID="RadButton3" Text="No" RenderMode="Lightweight" Font-Bold="true" Width="30%">
                                        </telerik:RadButton>
                                    </td>
                                </tr>
                               
                            </table>
                        </ContentTemplate>
                    </telerik:RadWindow>

                </div>
                <br />
                <br />
                  <br />
                <div style="text-align: center;">
                  
                   <%-- &nbsp  &nbsp  &nbsp--%>
                       <%--<asp:Button ID="btn_Update" runat="server" CssClass="btn btn-success" OnClick="btn_Update_Click"
                           Text="Update" />--%>
                   <%-- <asp:Button ID="btnSave" runat="server" CssClass="btn btn-success" OnClick="btnSave_Click1"
                        Text="Confirm" />--%>

                    
                </div>

               

            </div>

              

        </section>
    </section>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
</asp:Content>
