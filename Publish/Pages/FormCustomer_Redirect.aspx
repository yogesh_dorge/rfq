﻿<%@ page language="C#" autoeventwireup="true" masterpagefile="~/Pages/Admin.master" inherits="Pages_FormCustomer_Redirect, App_Web_y2c0a35n" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">
    <%--<link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>

    <script type="text/javascript">


        function Testme() {
            var val = document.getElementById('<%=txtCustomer_Name.ClientID %>').value;
            if (!isNaN(val)) {
                alert("Please don't enter numeric values");
                ctl.focus();
                return (false);
            }




        }

    </script>



    <style type="text/css">
        .not-active
        {
            pointer-events: none;
            cursor: default;
            color: gray!important;
        }

        .active
        {
            color: blue!important;
        }
    </style>
    <br />
    <section id="main-content">
        <section class="wrapper">
            <div class="divForm" style="padding: 1px; border: thin solid #000000; position: relative; width: 100%;"
                align="center">
                <telerik:RadWindowManager ID="rmw1" runat="server">
                </telerik:RadWindowManager>
                <%--<telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
                    <Localization OK="Ok" Cancel="No" />
                </telerik:RadWindowManager>--%>
                <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                </telerik:RadScriptManager>
                <table id="tblMain" align="center" width="100%">
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="Label5" runat="server" Font-Bold="true" Font-Size="X-Large" Text="Manage Customer Master" CssClass="WebHR_Heading1"></asp:Label>
                        </td>
                    </tr>
                    <tr >
                        <td style="text-align: left; font-family: 'Bookman Old Style';" colspan="2">
                        <asp:Label ID="Label7" runat="server" Text="(Note : (*) fields are Mandatory)" ForeColor="Red"></asp:Label>
                            </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label9" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>

                        <td align="left" width="50%">
                            <asp:Label ID="Label10" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="50%">
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" Text="Customer Name:"></asp:Label>
                        </td>
                        &nbsp;
                        <td align="left" width="45%">
                            <asp:TextBox ID="txtCustomer_Name" runat="server" Font-Size="Small" Height="25px" Width="250" onKeyUp="javascript:Testme();" CssClass="WebHR_TextBox"></asp:TextBox>
                              <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <%--   <input type="text" id="txtCustomer_Name" runat="server" />--%>

                            <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="only characters allowed" ControlToValidate="txtCustomer_Name" ValidationExpression="^[A-Za-z]*$"></asp:RegularExpressionValidator>--%>

                            <asp:RequiredFieldValidator ID="rfvtxtCustomer_Name" runat="server" ErrorMessage="Required Field Customer Name" ControlToValidate="txtCustomer_Name"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>




                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label11" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label12" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label2" runat="server" Font-Size="Small" Text="Email Id:  "></asp:Label>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtEmailId" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"
                                TextMode="SingleLine"></asp:TextBox>
                             <asp:Label ID="Label19" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfv_txtEmailId" runat="server" ErrorMessage="Required Field Customer EmailID" ControlToValidate="txtEmailId"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>


                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label25" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label26" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label3" runat="server" Font-Size="Small" Text="Contact No:  ">
                            </asp:Label>
                        </td>
                        <td align="left" width="50%">
                          <%--  <asp:TextBox ID="txtContactNo" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>--%>
                             <telerik:RadNumericTextBox ID="txtContactNo" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox" runat="server">
                                            <NumberFormat DecimalDigits="0" />
                                        </telerik:RadNumericTextBox>

                              <asp:Label ID="Label20" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvtxtContactNo" runat="server" ErrorMessage="Required Field Customer Contact Number" ControlToValidate="txtContactNo"     ForeColor="Red"></asp:RequiredFieldValidator>
                          <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtContactNo" ErrorMessage="Enter only numbers" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>--%>


                        </td>
                    </tr>


                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label13" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label14" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label4" runat="server" Font-Size="Small" Text="Company Name:  "></asp:Label>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtCompanyName" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>
                             <asp:Label ID="Label23" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required Field Company Name"  ControlToValidate="txtCompanyName"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label15" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label16" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label6" runat="server" Font-Size="Small" Text="Company Address:  "></asp:Label>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtCompanyAddress" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>
                              <asp:Label ID="Label24" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required Field Company Address" ControlToValidate="txtCompanyAddress"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label17" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label18" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>


                    <%--<tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label7" runat="server" Font-Size="Small" Text="Email Server IP:  "></asp:Label>
                        </td>
                        <td align="left" width="40%">
                            <asp:TextBox ID="txtEmailServerIP" runat="server" Font-Size="Small" Height="25px" Width="250"  CssClass="WebHR_TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="txtEmailServerIP"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label19" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label20" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label8" runat="server" Font-Size="Small" Text="Enable Ssl:  "></asp:Label>
                        </td>
                        <td align="left" width="40%">
                            <asp:TextBox ID="txtEnableSsl" runat="server" Font-Size="Small" Height="25px" Width="250"  CssClass="WebHR_TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ControlToValidate="txtEnableSsl"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>--%>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label21" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label22" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr align="center">
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-success" OnClick="btnSave_Click1"
                                Text="Save" Width="100px" />
                        </td>
                    </tr>

                </table>
                <br />
            </div>
        </section>
    </section>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
</asp:Content>
