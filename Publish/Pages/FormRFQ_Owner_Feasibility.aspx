﻿<%@ page language="C#" autoeventwireup="true" maintainscrollpositiononpostback="true" masterpagefile="~/Pages/Admin.master" inherits="FormRFQ_Owner_Feasibility, App_Web_ok35y50u" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--  <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%-- <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Styles/semantic.css" rel="stylesheet" />
    <link href="../Styles/semantic.min.css" rel="stylesheet" />
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/style-responsive.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
     

    </center>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <br />

    <script type="text/javascript">
        function DisableButton() {
            document.getElementById("<%=btn_SendMail.ClientID %>").disabled = true;
        }
        window.onbeforeunload = DisableButton;
    </script>
    <script src="scripts.js" type="text/javascript"></script>

    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="WebBlue" />

    <section id="main-content">
        <section class="wrapper">
            <div class="divForm" style="padding: 1px; border: thin solid #000000; position: relative; width: 100%;"
                align="center">
                <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
                    <Localization OK="Ok" Cancel="No" />
                </telerik:RadWindowManager>
                <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                </telerik:RadScriptManager>

  <div class="ui last container">
                    <div class="ui five steps">
                        <div id="Activestep" runat="server">
                            <div class="content">
                                <div class="title" id="rfqtitle" runat="server">RFQ Generation</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lblRFQcreated" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="step" id="AssignUser_Title" runat="server">
                            <div class="content">
                                <div class="title">Assign Owner</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_AssignUser_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="OwnerAvability_Title" runat="server">
                            <div class="content">
                                <div class="title">Component Feasibility</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_OwnerAvability_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="Last_Step_Title" runat="server">
                            <div class="content">
                                <div id="Div1" class="title" runat="server">Quotation</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_Last_Step_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="Approvaed" runat="server">
                            <div class="content">
                                <div id="Div2" class="title" runat="server">Approval</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="Label10" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                <br />

                <telerik:RadGrid RenderMode="Lightweight" AutoGenerateColumns="false" ID="rgvRFQ" runat="server" Skin="Bootstrap" AllowPaging="false"
                    AllowMultiRowSelection="true" ClientSettings-Scrolling-AllowScroll="true"
                    ClientSettings-Resizing-AllowResizeToFit="true" Height="66px" OnNeedDataSource="rgvRFQ_NeedDataSource" HeaderStyle-HorizontalAlign="Center">
                    <%--   <GroupingSettings CaseSensitive="false"></GroupingSettings>--%>
                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="RFQ_ID">
                        <Columns>
                          
                            <telerik:GridBoundColumn HeaderText="RFQ No" ReadOnly="true" HeaderButtonType="TextButton" DataField="RFQ_ID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Customer Name" ReadOnly="true" HeaderButtonType="TextButton" DataField="Customer_Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="RFQ Type" ReadOnly="true" HeaderButtonType="TextButton" DataField="RFQ_Type"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Project" ReadOnly="true" HeaderButtonType="TextButton" DataField="Product_Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Product or Component" ReadOnly="true" HeaderButtonType="TextButton" DataField="Sub_Product_Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Generated" ReadOnly="true" DataFormatString="{0:dd-MMM-yyyy}" HeaderButtonType="TextButton" DataField="Generated_Datetime"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Committed By" ReadOnly="true" DataFormatString="{0:dd-MMM-yyyy}" HeaderButtonType="TextButton" DataField="CommittedDate"></telerik:GridBoundColumn>
                              <telerik:GridBoundColumn HeaderText="Cell Head Remark" ReadOnly="true" HeaderButtonType="TextButton" DataField="Cell_Head_Remark"></telerik:GridBoundColumn>
                           <telerik:GridBoundColumn HeaderText="Status" ReadOnly="true" HeaderButtonType="TextButton" DataField="Status"></telerik:GridBoundColumn>

                            
                        

                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>

                <br />
                <br />

                <div>
                    <asp:Label ID="lblRisk" runat="server" Text=""></asp:Label>
                    <br /><br />
                    <asp:Label ID="lblBusiness" runat="server" Text=""></asp:Label>
                </div>
                
                <br />
                <br />

                <div style="text-align: center;">
                     &nbsp  &nbsp  &nbsp
                    <asp:Button ID="btn_Go" runat="server" CssClass="btn btn-success" OnClick="btn_Go_Click"
                        Text="GO" Width="150px" />
                    &nbsp  &nbsp  &nbsp
                    <asp:Button ID="btn_NoGo" runat="server" CssClass="btn btn-danger" OnClick="btn_NoGo_Click"
                        Text="No GO" Width="150px" />
                    &nbsp  &nbsp  &nbsp
                    <asp:TextBox ID="txtNoGoReason" runat="server" Visible="false"></asp:TextBox>
                </div>
                <br />
                <br />
                <div style="text-align: center;">
                    <asp:Button ID="btn_QuatationForm" runat="server" CssClass="btn btn-success" OnClick="btn_QuatationForm_Click"
                        Text="Quotation Form" />
                </div><br />
                 <div style="text-align: center;">
                      <asp:Label ID="Label1" runat="server" Text="Status: "></asp:Label>
                     <asp:Label ID="lblStatus" runat="server" ForeColor="Red" Text=""></asp:Label>
                </div>


                <telerik:RadWindow ID="Rw_PrintOption" runat="server" Modal="true" ReloadOnShow="true"
                    OpenerElementID="btn_NoGo" Title="Reason" VisibleStatusbar="false" VisibleTitlebar="false">
                    <ContentTemplate>
                        <br />
                        <table width="100%" align="center" class="summaryTable" style="background-color: #82CAFA">
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>

                            <tr>
                                <td align="center" style="text-align: center">
                                    <asp:Label ID="Label2" runat="server" Font-Size="Small" Text="Reason:">
                                    </asp:Label>
                                    &nbsp

                                   <telerik:RadTextBox ID="txtReason" runat="server" Height="80Px" Width="70%" TextMode="MultiLine"></telerik:RadTextBox>

                                </td>
                            </tr>

                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>


                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>


                            <tr>
                                <td colspan="2" align="center">
                                   <%-- <telerik:RadButton ID="btn_SendMail" runat="server" OnClientClick="javascript:ShowOverlay();" Text="Send Mail" OnClick="btn_SendMail_Click" />--%>
                                    <asp:Button ID="btn_SendMail" runat="server" CssClass="btn btn-info" OnClientClick="javascript:ShowOverlay();" OnClick="btn_SendMail_Click"
            Text="Send Mail" />

                                </td>
                            </tr>

                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>

                        </table>
                    </ContentTemplate>
                </telerik:RadWindow>

                <br />
                <br />
                <br />

            </div>
        </section>
    </section>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
</asp:Content>
