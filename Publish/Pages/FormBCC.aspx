﻿<%@ page language="C#" autoeventwireup="true" masterpagefile="~/Pages/Admin.master" inherits="MasterMR, App_Web_10kffnjd" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%-- <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">
    <%--<link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <br />
    <section id="main-content">
        <section class="wrapper">
            <div class="divForm" style="padding: 1px; border: thin solid #000000; position: relative; width: 100%;"
                align="center">
                <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
                </telerik:RadWindowManager>
                <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                </telerik:RadScriptManager>


                <table id="tblMain" align="center" width="100%">
             <tr>
                <td align="center" colspan="4">
                    
                    <asp:Label ID="Label1" runat="server" Font-Bold="true" Font-Size="X-Large"  CssClass="WebHR_Heading1" 
                        Text="BCC EMAIL"></asp:Label>
   
                 </td>
            </tr>
             <tr>
                <td>
                    
                    &nbsp;</td>
            </tr>
          <tr>
                <td align="right" width="50%">
                    
                    BCC Email</td>
                <td>
                    
                    </td>
                <td align="left" width="50%">
                    <asp:TextBox ID="txtBCC" runat="server" Width="200px"></asp:TextBox>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ForeColor="Red"
                        ControlToValidate="txtBCC">
                    </asp:RequiredFieldValidator>
      <%--&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtCustName" runat="server" ErrorMessage="Special symbols not allowed" ValidationExpression="^[a-zA-Z0-9_ ]+$"  ForeColor="Red"></asp:RegularExpressionValidator> &nbsp;--%>
                              </td>
                <%--<td class="style2">
                    &nbsp;</td>--%>
            </tr>

                                 <tr>
                <td>
                    
                    &nbsp;</td>
            </tr>

            <tr>
                <%--<td align="right">
                    &nbsp;
                    </td>--%>
                <td align="right">
                    &nbsp;</td>
                <td align="right">
                    &nbsp;
                    <asp:Button ID="btnTest" runat="server" CssClass="Buttons_Blue" OnClick="btnTest_Click" Text="Save BCC EMAIL" 
                         />
                </td>
                <%--<td>
                    &nbsp;
                </td>--%>
            </tr>
            <tr>
                <td> &nbsp; &nbsp; &nbsp; &nbsp;
                    </td>
                <td class="style1">
                    <asp:Label ID="lblStatus" runat="server" Text=" "></asp:Label>
                    </td>
                <%--<td align="center">
                    
                 </td>
                <td>
                    </td>--%>
            </tr>
           <tr>
                <%--<td>
                    &nbsp;</td>--%>
                <td class="style1">
                    &nbsp;</td>
                <td colspan="2" align="right">
                    
                    <asp:LinkButton ID="Back" runat="server" CausesValidation="False" 
                        onclick="Back_Click" >Back</asp:LinkButton>
                 </td>
            </tr>

        </table>


       <%--         <table id="tblMain" align="center" width="100%">
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="Label5" runat="server" Font-Bold="true" Font-Size="X-Large" Text="MANAGE MR MASTER" CssClass="WebHR_Heading1"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label8" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>

                        <td align="right" width="50%">
                            <asp:Label ID="Label9" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td align="right" width="50%">
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" Text="MR Name:  "></asp:Label>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtMRName" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="*This field is required" ControlToValidate="txtMRName"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label2" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label7" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="50%">
                            <asp:Label ID="Label3" runat="server" Font-Size="Small" Text="MR Email ID:  ">
                            </asp:Label>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtMREmailID" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvServerName" runat="server" ErrorMessage="*This field is required" ControlToValidate="txtMREmailID"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="50%">
                            <asp:Label ID="Label10" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label11" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td align="right" width="50%">
                            <asp:Label ID="Label4" runat="server" Font-Size="Small" Text="Role:  "></asp:Label>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtRole" runat="server" CssClass="WebHR_TextBox" Height="25px" Width="250" Font-Size="Small" Text="MR" Enabled="false"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*This field is required" ControlToValidate="txtRole"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label12" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label13" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="50%">
                            <asp:Label ID="Label6" runat="server" Font-Size="Small" Text="MR Contact No:  "></asp:Label>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtContactNo" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*This field is required" ControlToValidate="txtContactNo"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtContactNo" ErrorMessage="Enter only 10 numbers" ValidationExpression="[0-9]{10}" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label14" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label15" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td width="50%" colspan="2" align="center">
                            <asp:Button ID="btnSave" runat="server" Font-Size="Medium" CssClass="Buttons_Blue" OnClick="btnSave_Click"
                                Text="SAVE" Width="74px" Style="height: 26px" />
                         
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label16" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label17" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%"></td>
                        <td align="right" width="50%">
                            <asp:LinkButton ID="Back" runat="server" CausesValidation="False" OnClick="Back_Click"
                                PostBackUrl="~/Pages/Dashboard.aspx">Back</asp:LinkButton>
                        </td>
                    </tr>
                </table>--%>
            </div>
        </section>
    </section>
</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
</asp:Content>
