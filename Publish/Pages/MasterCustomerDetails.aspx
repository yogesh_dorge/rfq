﻿<%@ page title="" language="C#" masterpagefile="~/Pages/Admin.master" autoeventwireup="true" inherits="Pages_MasterCustomerDetails, App_Web_3kw0abyc" maintainscrollpositiononpostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script runat="server">
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">
    link href="../Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
    <style type="text/css">
        .rgRow, .rgAltRow
        {
            height: 40px;
        }
    </style>
    <script type="text/javascript">

        function UserAction(sender, args) {
            if (sender.get_batchEditingManager().hasChanges(sender.get_masterTableView()) &&
                !confirm("Any changes will be cleared. Are you sure you want to perform this action?")) {
                args.set_cancel(true);
            }
        }

        $(function () {
            $('#txtCustomer_Name').keydown(function (e) {
                if (e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                        e.preventDefault();
                    }
                }
            });
        });
        $(function () {
            $('#txtCompanyName').keydown(function (e) {
                if (e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                        e.preventDefault();
                    }
                }
            });
        });

    </script>





    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>

    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="WebBlue" />

    <telerik:RadWindowManager ID="rmw1" runat="server">
    </telerik:RadWindowManager>


    <section id="main-content">
        <section class="wrapper">

            <br />
            <br />

            <div style="padding-top: 20px; padding-left: 10px">
            </div>
            <div style="align-items: center; text-align: center;">
                <asp:Label ID="lblUserHeadning" Font-Size="Large" Font-Bold="true" runat="server" CssClass="WebHR_Heading1"
                    Text="Manage Customer Master"></asp:Label>
            </div>

            <br />
            <br />
            <div class="demo-container no-bg">
                <%--<h3>RadGrid bound to RadClientDataSource</h3>--%>
                <div id="grid">
                    <asp:Label ID="Label16" runat="server" Text="(Note : (*) fields are Mandatory)" ForeColor="Red"></asp:Label>
                                    <br />
                    <telerik:RadGrid RenderMode="Lightweight" ID="rgvCustomerMaster" runat="server" AllowPaging="true" AllowSorting="true" AllowFilteringByColumn="true" AutoGenerateColumns="false" OnNeedDataSource="rgvCustomerMaster_NeedDataSource" OnBatchEditCommand="rgvCustomerMaster_BatchEditCommand" OnItemDataBound="rgvCustomerMaster_ItemDataBound" OnItemCreated="rgvCustomerMaster_ItemCreated">
                        <ClientSettings AllowKeyboardNavigation="true">
                            <KeyboardNavigationSettings AllowActiveRowCycle="true" />

                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                           <GroupingSettings CaseSensitive="false"></GroupingSettings>
                        <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" IgnorePaging="true" FileName="Check Sheet Master">
                        </ExportSettings>
                        <MasterTableView ClientDataKeyNames="Customer_ID" EditMode="Batch" CommandItemDisplay="Top" DataKeyNames="Customer_ID">
                            <%--BatchEditingSettings-HighlightDeletedRows="true"--%>
                            <BatchEditingSettings EditType="Cell" />
                            <CommandItemSettings ShowExportToCsvButton="true" ShowExportToExcelButton="true" />

                            <Columns>
                                <telerik:GridBoundColumn DataField="Customer_ID" HeaderText="Customer ID" Visible="false">
                                </telerik:GridBoundColumn>

                                <%--       <telerik:GridBoundColumn DataField="Customer_Name" HeaderText="Customer Name" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ForeColor="Red" ErrorMessage="*This field is required"></RequiredFieldValidator>
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>--%>


                                <telerik:GridTemplateColumn HeaderText="Customer Name(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" UniqueName="Customer_Name" DataField="Customer_Name">
                                    <ItemTemplate>
                                        <%# Eval("Customer_Name") %>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <input type="text" id="txtCustomer_Name" />

                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="EmailId" HeaderText="Email Id(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator ForeColor="Red" ErrorMessage="*This field is required"></RequiredFieldValidator>
                                    </ColumnValidationSettings>
                                </telerik:GridBoundColumn>


                                <%--<telerik:GridTemplateColumn HeaderText="Contact No" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" UniqueName="ContactNo" DataField="ContactNo">
                                        <ItemTemplate>
                                            <%# Eval("ContactNo") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <telerik:RadTextBox ID="txtContactNo" MaxLength="10" runat="server"></telerik:RadTextBox>
                                       
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtContactNo" runat="server" Text="*Required" ForeColor="Red" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1"  runat="server"  ControlToValidate="txtContactNo" ErrorMessage="Enter only numbers" ValidationExpression="[0-9]{10}" ForeColor="Red"></asp:RegularExpressionValidator>
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>--%>

                                <telerik:GridTemplateColumn HeaderText="Contact No(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" UniqueName="ContactNo" DataField="ContactNo">
                                    <ItemTemplate>
                                        <%# Eval("ContactNo") %>
                                    </ItemTemplate>
                                    <EditItemTemplate>

                                        <%--  <input type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="txtContactNo"/>--%>
                                        <telerik:RadNumericTextBox ID="txtContactNo" runat="server">
                                            <NumberFormat DecimalDigits="0" />
                                        </telerik:RadNumericTextBox>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>



                                <%--  <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company Name" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ForeColor="Red" ErrorMessage="*This field is required"></RequiredFieldValidator>
                                        </ColumnValidationSettings>
                                    </telerik:GridBoundColumn>--%>


                                <telerik:GridTemplateColumn HeaderText="Company Name(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" UniqueName="CompanyName" DataField="CompanyName">
                                    <ItemTemplate>
                                        <%# Eval("CompanyName") %>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <input type="text" id="txtCompanyName" />

                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>



                                <telerik:GridBoundColumn DataField="CompanyAddress" HeaderText="Address(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator ForeColor="Red" ErrorMessage="*This field is required"></RequiredFieldValidator>
                                    </ColumnValidationSettings>
                                </telerik:GridBoundColumn>


                                <telerik:GridClientDeleteColumn HeaderText="Delete">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridClientDeleteColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnUserAction="UserAction" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
                <telerik:GridTextBoxColumnEditor ID="GridTextBoxEditor" runat="server" TextBoxStyle-Width="100px"></telerik:GridTextBoxColumnEditor>

                <%--                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [AuditType] FROM [tblAuditType]"></asp:SqlDataSource>

                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [DeptCode] FROM [tblDepartmentMaster]"></asp:SqlDataSource>--%>
            </div>

        </section>
    </section>

</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
</asp:Content>


