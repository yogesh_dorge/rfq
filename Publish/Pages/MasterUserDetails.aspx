﻿<%@ page title="" language="C#" masterpagefile="~/Pages/Admin.master" autoeventwireup="true" inherits="Pages_MasterUserDetails, App_Web_xcmovcei" maintainscrollpositiononpostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script runat="server">
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">

    <%--  <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript" src="Scripts/jquery-1.4.1.min.js"></script>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
    <style type="text/css">
        .rgRow, .rgAltRow
        {
            height: 40px;
        }
    </style>
    <script type="text/javascript">



        $(function () {
            $('#txtUserName').keydown(function (e) {
                if (e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                        e.preventDefault();
                    }
                }
            });
        });

        $(function () {
            $('#txtContactNo').keydown(function (e) {
                if (e.shiftKey || e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;
                    if (!((key < 48) || (key >57))) {
                        e.preventDefault();
                    }
                }
            });
        });


        $('#txtUserName').on('input', function () {
            var input = $(this);
            var is_name = input.val();
            if (is_name) { input.removeClass("invalid").addClass("valid"); }
            else { input.removeClass("valid").addClass("invalid"); }
        });



        function UserAction(sender, args) {
            if (sender.get_batchEditingManager().hasChanges(sender.get_masterTableView()) &&
                !confirm("Any changes will be cleared. Are you sure you want to perform this action?")) {
                args.set_cancel(true);
            }
        }




    </script>


    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>

    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="WebBlue" />

    <telerik:RadWindowManager ID="rmw1" runat="server">
    </telerik:RadWindowManager>

    <section id="main-content">
        <section class="wrapper">

            <br />
            <br />
            <div style="padding-top: 20px; padding-left: 10px">
            </div>
            <div style="align-items: center; text-align: center;">
                <asp:Label ID="Label1" Font-Size="Large" Font-Bold="true" runat="server" CssClass="WebHR_Heading1"
                    Text="Manage User Master"></asp:Label>
            </div>

            <br />
            <br />

            <div class="demo-container no-bg">
                <%--<h3>RadGrid bound to RadClientDataSource</h3>--%>




                <div id="grid">
                      <asp:Label ID="Label16" runat="server" Text="(Note : (*) fields are Mandatory)" ForeColor="Red"></asp:Label>
                                    <br />
                    <telerik:RadGrid RenderMode="Lightweight" ID="rgvUserMaster" StandardTab="" runat="server" AllowPaging="true" AllowSorting="true" AllowFilteringByColumn="true" AutoGenerateColumns="false" OnNeedDataSource="rgvUserMaster_NeedDataSource"  ValidationSettings-EnableValidation="true" OnBatchEditCommand="rgvUserMaster_BatchEditCommand" OnItemDataBound="rgvUserMaster_ItemDataBound" ClientSettings-AllowKeyboardNavigation="true" OnItemCreated="rgvUserMaster_ItemCreated">
                        <%--ClientDataSourceID="RadClientDataSource1"--%>
                        <%--<ClientSettings>
                       <ClientEvents OnRowCreating="rgvUserMaster_RowCreating" />
                </ClientSettings>--%>
                        <%--   <ClientSettings AllowKeyboardNavigation="true"  >
                            <KeyboardNavigationSettings AllowActiveRowCycle="true" />

                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>--%>
                        <GroupingSettings CaseSensitive="false"></GroupingSettings>
                        <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" IgnorePaging="true" FileName="User Details">
                        </ExportSettings>
                        <MasterTableView ClientDataKeyNames="Userid" EditMode="Batch" CommandItemDisplay="Top" DataKeyNames="Userid">
                            <%--BatchEditingSettings-HighlightDeletedRows="true"--%>
                            <BatchEditingSettings EditType="Cell" />
                            <CommandItemSettings ShowExportToCsvButton="true" ShowExportToExcelButton="true" />

                            <Columns>

                                <%--          <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator ForeColor="Red" ErrorMessage="*This field is required"></RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="only characters allowed" ControlToValidate="TextBox1" ValidationExpression="^[A-Za-z]*$"></asp:RegularExpressionValidator>

                                    </ColumnValidationSettings>
                                </telerik:GridBoundColumn>--%>

                                
                            
                                          <telerik:GridBoundColumn DataField="Userid" HeaderText="User Id" Visible="false">
                              

                                </telerik:GridBoundColumn>
                                

                                <telerik:GridTemplateColumn HeaderText="User Name(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" UniqueName="UserName" DataField="UserName">
                                    <ItemTemplate>
                                        <%# Eval("UserName") %>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <input type="text" id="txtUserName" />
                                        <%--     <telerik:RadTextBox ID="txtUserName"  runat="server"></telerik:RadTextBox>--%>
                                        <%-- <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txtUserName" ForeColor="Red" ErrorMessage="*This field is required"></asp:RequiredFieldValidator>--%>
                                        <%-- <asp:RequiredFieldValidator ID="rdb_txtUserName" ControlToValidate="txtUserName" runat="server" Text="*Required" ForeColor="Red" />
                                  <%--      <asp:RegularExpressionValidator ID="reb_txtUserName" runat="server" ErrorMessage="only characters allowed" ControlToValidate="txtUserName" ForeColor="Red" ValidationExpression="^[A-Za-z]*$"></asp:RegularExpressionValidator>--%>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>




                                <telerik:GridBoundColumn DataField="UserEmail" HeaderText="Email ID(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator ForeColor="Red" ErrorMessage="*This field is required"></RequiredFieldValidator>
                                    </ColumnValidationSettings>

                                </telerik:GridBoundColumn>

                                <telerik:GridTemplateColumn HeaderText="Role Code(*)" DefaultInsertValue="User" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" UniqueName="Role_Code" DataField="Role_Code">
                                    <ItemTemplate>
                                        <%# Eval("Role_Code") %>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ID="DeptCodeIDDropDown" DataValueField="RoleCode"
                                            DataTextField="RoleCode" DataSourceID="SqlDataSource1">
                                        </telerik:RadDropDownList>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>

                                <%--    <telerik:GridTemplateColumn HeaderText="Plant" DefaultInsertValue="X1" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" UniqueName="Plant" DataField="Plant">
                                        <ItemTemplate>
                                            <%# Eval("Plant") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ID="PlantIDDropDown" DataValueField="Plant"
                                                DataTextField="Plant" DataSourceID="SqlDataSource3">
                                            </telerik:RadDropDownList>
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>--%>


                                <telerik:GridTemplateColumn HeaderText="Contact No(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" UniqueName="ContactNo" DataField="ContactNo">
                                    <ItemTemplate>
                                        <%# Eval("ContactNo") %>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <%--<telerik:RadTextBox ID="txtContactNo" MaxLength="10" runat="server"></telerik:RadTextBox>--%>

                                           <%-- <input type="number" id="txtContactNo"  />--%>
                                        <%--<input type="text" maxlength="10" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="txtContactNo"/>--%>

                                        <telerik:RadNumericTextBox ID="txtContactNo"  runat="server">
                                              <NumberFormat DecimalDigits="0" />
                                            </telerik:RadNumericTextBox>

                                  <%--      
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtContactNo" runat="server" Text="*Required" ForeColor="Red" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtContactNo" ErrorMessage="Enter only numbers" ValidationExpression="[0-9]{10}" ForeColor="Red"></asp:RegularExpressionValidator>--%>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridClientDeleteColumn HeaderText="Delete">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridClientDeleteColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnUserAction="UserAction" />
                        </ClientSettings>
                    </telerik:RadGrid>


               <%--     <telerik:AjaxSetting AjaxControlID="rgvUserMaster">
                        <updatedcontrols>
  <telerik:AjaxUpdatedControl ControlID="rgvUserMaster" />
 </updatedcontrols>
                    </telerik:AjaxSetting>--%>
                </div>
                <telerik:GridTextBoxColumnEditor ID="GridTextBoxEditor" runat="server" TextBoxStyle-Width="175px"></telerik:GridTextBoxColumnEditor>

                <%-- <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                        ProviderName="System.Data.SqlClient" SelectCommand="SELECT [DeptCode] as UserDept FROM [tblDepartmentMaster]"></asp:SqlDataSource>--%>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [RoleCode] FROM [tblRoleMaster]"></asp:SqlDataSource>
                <%-- <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                        ProviderName="System.Data.SqlClient" SelectCommand="SELECT [Plant] FROM [tbl_PlantDetails]"></asp:SqlDataSource>--%>
            </div>
        </section>
    </section>

</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
</asp:Content>

