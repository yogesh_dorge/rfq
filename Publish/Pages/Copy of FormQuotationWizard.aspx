﻿<%@ page language="C#" autoeventwireup="true" maintainscrollpositiononpostback="true" inherits="Pages_FormQuotationWizard, App_Web_tcx0gt55" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>



<script type="text/javascript">
    (function () {

        window.pageLoad = function () {
            var $ = $telerik.$;
            var cssSelectors = ["accountInfo", "personalInfo", "contactDetails", "confirmation", "Checksheet"];
            var breadCrumbButtons = $(".rwzBreadCrumb .rwzLI");

            for (var i = 0; i < cssSelectors.length; i++) {
                $(breadCrumbButtons[i]).addClass(cssSelectors[i]);
            }
        }

        window.OnClientLoad = function (sender, args) {
            for (var i = 1; i < sender.get_wizardSteps().get_count() ; i++) {
                sender.get_wizardSteps().getWizardStep(i).set_enabled(true);
            }

        }

        window.OnClientButtonClicking = function (sender, args) {
            if (!args.get_nextActiveStep().get_enabled()) {
                args.get_nextActiveStep().set_enabled(true);

            }


        }



        ////window.AcceptTermsCheckBoxValidation = function (source, args) {
        ////    var termsChecked = $telerik.$("input[id*='AcceptTermsCheckBox']")[0].checked;
        ////    args.IsValid = termsChecked;
        ////}

        //////window.UserNameLenghthValidation = function (source, args) {
        //////    var userNameConditions = $telerik.$(".conditions")[0];
        //////    var isValid = (args.Value.length >= 4 && args.Value.length <= 15);
        //////    args.IsValid = isValid;
        //////    $telerik.$(userNameConditions).toggleClass("redColor", !isValid);

        //////}

        //////window.PasswordLenghthValidation = function (source, args) {
        //////    var passwordConditions = $telerik.$(".conditions")[1];
        //////    var isValid = args.Value.length >= 6;
        //////    args.IsValid = isValid;
        //////    $telerik.$(passwordConditions).toggleClass("redColor", !isValid);
        //////}
    })();


</script>
<style type="text/css">
    .style1
    {
        height: 20px;
    }

    table, th, td
    {
        border: 1px groove black;
    }

        td:hover
        {
            background-color: #F8F8FF;
        }



    td
    {
        font-size: 14px !important;
    }

    .W3_Title
    {
        color: #666;
        font-weight: bold !important;
        font-size: 20px !important;
        text-decoration: none;
    }

    .W3_Header
    {
        color: #666;
        font-weight: bold !important;
        font-size: 30px !important;
        text-decoration: none;
        text-align: center !important;
    }

    .W3_Warning
    {
        color: #C7310C;
        font-weight: bold !important;
        font-size: 20px !important;
        text-decoration: none;
        text-align: center !important;
    }

    /*PRINT DIV*/
    @media print
    {
        .myDivToPrint
        {
            background-color: white;
            height: 100%;
            width: 100%;
            position: fixed;
            top: 0;
            left: 0;
            margin: 0;
            padding: 15px;
            font-size: 14px;
            line-height: 18px;
        }
    }

    #scrolly
    {
        width: 1000px;
        height: 190px;
        overflow: auto;
        overflow-y: hidden;
        margin: 0 auto;
        white-space: nowrap;
    }
</style>
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>

    <script src="assets/Scripts/script.js"></script>
    <link href="../Styles/styles.css" runat="server" rel="stylesheet" />


    <%--  <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%-- <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Styles/semantic.css" rel="stylesheet" />
    <link href="../Styles/semantic.min.css" rel="stylesheet" />
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/style-responsive.css" rel="stylesheet" />


    <style type="text/css">
    .demo-container {
        width:inherit;
        height:inherit;
        overflow:auto !important;
    }
  
</style>

</head>
<body style="background-color:powderblue;">

    <link href="../Styles/styles.css" runat="server" rel="stylesheet" />
    <form id="form1" runat="server">
        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
        <telerik:RadWindowManager ID="rmw1" runat="server">
        </telerik:RadWindowManager>
        <telerik:RadSkinManager ID="RadSkinManager1" runat="server" Skin="Outlook" Visible="false" ShowChooser="true" />



        <div class="demo-container">


            <div class="wizardHeader">

                <h1 style="color: #3399ff; align-content: center">
                    <asp:Label ID="Label10" Text="RFQ No:" runat="server" />
                    <asp:Label ID="lbl_RFQ_No" ForeColor="Chocolate" Text="" runat="server" />
                </h1>
            </div>
                      <div style="text-align-last: right;">
                <telerik:RadButton RenderMode="Lightweight" runat="server" ID="btn_PrintForm" Text="Quotation Print" OnClick="btn_PrintForm_Click"
                    CssClass="submit" />

                <telerik:RadButton RenderMode="Lightweight" runat="server" ID="btn_redirect" Text="Home" OnClick="btn_redirect_Click"
                    CssClass="submit" Visible="false" />

                          <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Pages/Dashboard.aspx" Font-Bold="true" Font-Size="X-Large" Font-Underline="true">Home</asp:HyperLink>

            </div>
            <br />
              <div class="ui last container">
                    <div class="ui five steps">
                        <div id="Activestep" runat="server">
                            <div class="content">
                                <div class="title" id="rfqtitle" runat="server">RFQ Generation</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lblRFQcreated" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="step" id="AssignUser_Title" runat="server">
                            <div class="content">
                                <div class="title">Assign Owner</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_AssignUser_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="OwnerAvability_Title" runat="server">
                            <div class="content">
                                <div class="title">Component Feasibility</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_OwnerAvability_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="Last_Step_Title" runat="server">
                            <div class="content">
                                <div id="Div1" class="title" runat="server">Quotation</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_Last_Step_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="Approvaed" runat="server">
                            <div class="content">
                                <div id="Div2" class="title" runat="server">Approval</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_approval_step_title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

           
            <br />

            <div style="text-align: center;">
                 <asp:Label ID="Label5" runat="server" Text="Customer Name: "></asp:Label>
                      <asp:Label ID="lblCustomerName" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp  &nbsp  &nbsp
                      <asp:Label ID="Label9" runat="server" Text="Project: "></asp:Label>
                      <asp:Label ID="lblProduct" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp  &nbsp  &nbsp
                      <asp:Label ID="Label13" runat="server" Text="Product or Component: "></asp:Label>
                      <asp:Label ID="lblSubProduct" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp  &nbsp  &nbsp
                      <asp:Label ID="Label14" runat="server" Text="Remark: "></asp:Label>
                      <asp:Label ID="lblRemark" runat="server" Text="" Font-Bold="true"></asp:Label>
            </div>
               

            <telerik:RadWizard RenderMode="Lightweight" ID="RadWizard1" runat="server"  Skin="Silk" OnClientLoad="OnClientLoad" OnClientButtonClicking="OnClientButtonClicking" OnNextButtonClick="RadWizard1_NextButtonClick" OnFinishButtonClick="RadWizard1_FinishButtonClick" OnNavigationBarButtonClick="RadWizard1_NavigationBarButtonClick" Height="95%" OnActiveStepChanged="RadWizard1_ActiveStepChanged">
                <WizardSteps>

                    <telerik:RadWizardStep ID="RadWizardStep1" Title="Header Part" runat="server" StepType="Start" ValidationGroup="accountInfo" CausesValidation="true" SpriteCssClass="accountInfo" >

                        <div class="inputWapper first">
                            <asp:Label ID="lbl_CustomerName" Text="Customer: " runat="server" AssociatedControlID="txt_CustomerName" />

                            <asp:Label ID="txt_CustomerName" Font-Bold="true" Text="" Width="320px" runat="server" />
                        </div>



                        <div class="inputWapper first">
                            <asp:Label ID="lbl_RfqType" Text="RFQ Type: " runat="server" AssociatedControlID="txt_Rfq_type" />

                            <asp:Label ID="txt_Rfq_type" Font-Bold="true" Text="" Width="320px" runat="server" />

                        </div>
                        <br />

                        <div class="inputWapper first">
                            <asp:Label ID="lbl_INCOTerm" Text="INCO Term: " runat="server" AssociatedControlID="cmbInco_term" />

                            <telerik:RadComboBox RenderMode="Lightweight" ID="cmbInco_term" Width="320px" Filter="StartsWith" AutoPostBack="true" runat="server" EmptyMessage="Select INCO Term"></telerik:RadComboBox>
                            <asp:RequiredFieldValidator ID="rfvcmbInco_term" runat="server" ControlToValidate="cmbInco_term" EnableClientScript="true" ValidationGroup="accountInfo" ErrorMessage="*" CssClass="validator" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>


                        <div class="inputWapper first">
                            <asp:Label ID="Label2" Text="Part No: " runat="server" AssociatedControlID="txt_Part_No" />
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txt_Part_No" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfv_txt_Part_No" runat="server" ControlToValidate="txt_Part_No" EnableClientScript="true" ValidationGroup="accountInfo" ErrorMessage="*" CssClass="validator" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <br />

                        <div class="inputWapper first">
                            <asp:Label ID="Label15" Text="Revision/Version: " runat="server" AssociatedControlID="txt_Revision_Version" />
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txt_Revision_Version" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfvtxt_Revision_Version" runat="server" ControlToValidate="txt_Revision_Version" EnableClientScript="true" ValidationGroup="accountInfo" ErrorMessage="*" CssClass="validator" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="inputWapper first">
                            <asp:Label ID="Label1" Text="Description: " runat="server" AssociatedControlID="txt_Description" />
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txt_Description" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfvtxt_Description" runat="server" ControlToValidate="txt_Description" EnableClientScript="true" ValidationGroup="accountInfo" ErrorMessage="*" CssClass="validator" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>

                        <br />

                        <div class="inputWapper first">
                            <asp:Label ID="Label3" Text="EAU: " runat="server" AssociatedControlID="txt_EAU" />
                            <%--   <telerik:RadTextBox RenderMode="Lightweight" ID="txt_EAU" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None"></telerik:RadTextBox>--%>

                            <telerik:RadNumericTextBox ID="txt_EAU" RenderMode="Lightweight" AutoPostBack="true" CausesValidation="false" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None" MinValue="0">
                                <NumberFormat DecimalDigits="0" />
                            </telerik:RadNumericTextBox>


                            <asp:RequiredFieldValidator ID="rfvtxt_EAU" runat="server" ControlToValidate="txt_EAU" EnableClientScript="true" ValidationGroup="accountInfo" ErrorMessage="*" CssClass="validator" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="inputWapper first">
                            <asp:Label ID="Label4" Text="Raw Material Specified: " runat="server" AssociatedControlID="txt_RawMaterialSpecified" />


                            <telerik:RadComboBox RenderMode="Lightweight" ID="txt_RawMaterialSpecified" Width="320px" Filter="StartsWith" AutoPostBack="true" runat="server" EmptyMessage="Select Raw Material"></telerik:RadComboBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_RawMaterialSpecified" EnableClientScript="true" ValidationGroup="accountInfo" ErrorMessage="*" CssClass="validator" ForeColor="Red"></asp:RequiredFieldValidator>

                            <%--<telerik:RadTextBox RenderMode="Lightweight" ID="txt_RawMaterialSpecified" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None"></telerik:RadTextBox>--%>
                            <asp:RequiredFieldValidator ID="rfv_txt_RawMaterialSpecified" runat="server" ControlToValidate="txt_RawMaterialSpecified" EnableClientScript="true" ValidationGroup="accountInfo" ErrorMessage="*" CssClass="validator" ForeColor="Red"></asp:RequiredFieldValidator>


                        </div>

                        <br />

                        <div class="inputWapper first">
                            <asp:Label ID="Label6" Text="Raw Material - Considered: " runat="server" AssociatedControlID="txt_RawMaterialConsidered" />

                            <asp:Label ID="txt_RawMaterialConsidered" Font-Bold="true" Text="" Width="320px" runat="server" />

                        </div>

                        <div class="inputWapper first">
                            <asp:Label ID="Label7" Text="RATE / KG: " runat="server" AssociatedControlID="txt_RATE_KG" />

                            <asp:Label ID="txt_RATE_KG" Font-Bold="true" Text="" Width="320px" runat="server" />
                        </div>
                        <br />
                        <div class="inputWapper first">
                            <asp:Label ID="Label8" Text="Input Weight (KGS): " runat="server" AssociatedControlID="txt_InputWeight_KGS" />
                            <%--    <telerik:RadTextBox RenderMode="Lightweight" ID="txt_InputWeight_KGS" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None"></telerik:RadTextBox>--%>
                            <telerik:RadNumericTextBox ID="txt_InputWeight_KGS" RenderMode="Lightweight" AutoPostBack="true" CausesValidation="false" OnTextChanged="txt_InputWeight_KGS_TextChanged" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None" MinValue="0">
                                <NumberFormat DecimalDigits="2" />
                            </telerik:RadNumericTextBox>



                            <asp:RequiredFieldValidator ID="rfvtxt_InputWeight_KGS" runat="server" ControlToValidate="txt_InputWeight_KGS" EnableClientScript="true" ValidationGroup="accountInfo" ErrorMessage="*" CssClass="validator" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="inputWapper first">
                            <asp:Label ID="Label11" Text="Forg Wt: " runat="server" AssociatedControlID="txt_ForgWt" />
                            <%--     <telerik:RadTextBox RenderMode="Lightweight" ID="txt_ForgWt" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None"></telerik:RadTextBox>--%>
                            <telerik:RadNumericTextBox ID="txt_ForgWt" RenderMode="Lightweight" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None" MinValue="0">
                                <NumberFormat DecimalDigits="2" />
                            </telerik:RadNumericTextBox>

                            <asp:RequiredFieldValidator ID="rfvtxt_ForgWt" runat="server" ControlToValidate="txt_ForgWt" EnableClientScript="true" ValidationGroup="accountInfo" ErrorMessage="*" CssClass="validator" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>

                        <br />

                        <div class="inputWapper first">
                            <asp:Label ID="Label12" Text="FINISH Weight (KGS): " runat="server" AssociatedControlID="txt_InputWeight_KGS" />
                            <%--  <telerik:RadTextBox RenderMode="Lightweight" ID="txt_FINISHWeight" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None"></telerik:RadTextBox>--%>

                            <telerik:RadNumericTextBox ID="txt_FINISHWeight" RenderMode="Lightweight" runat="server" Width="320px" ValidationGroup="accountInfo" Resize="None" MinValue="0">
                                <NumberFormat DecimalDigits="2" />
                            </telerik:RadNumericTextBox>

                            <asp:RequiredFieldValidator ID="rfvtxt_FINISHWeight" runat="server" ControlToValidate="txt_FINISHWeight" EnableClientScript="true" ValidationGroup="accountInfo" ErrorMessage="*" CssClass="validator" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <br />
                        <br />
                        <br />

                        <%-- <div style="text-align: center;">
                            <asp:Label ID="Label13" Text="RAW MATERIAL COST RS:" Font-Bold="true" runat="server" />
                            <asp:Label ID="txt_RAWMATERIALCOST_RS" Font-Bold="true" Text="56565" Width="320px" runat="server" />
                        </div>--%>

                        <table id="Table2" width="100%" runat="server" style="overflow-x: scroll; table-layout: fixed;">
                            <tr>

                                <td align="center" class="W3_Warning">RAW MATERIAL COST RS:
              <asp:Label ID="txt_RAWMATERIAL_COST_RS" Font-Bold="true" ForeColor="Blue" Text="" runat="server" />
                                </td>

                            </tr>
                        </table>



                    </telerik:RadWizardStep>

                    <telerik:RadWizardStep ID="RadWizardStep2" Title="Processing Details" runat="server" StepType="Step" ValidationGroup="personalInfo" SpriteCssClass="personalInfo">

                        <table id="Table1" width="100%" runat="server" style="overflow-x: scroll; table-layout: fixed;">
                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style'; color: blue">PROCESSING (Process cost giving details of process reqd)
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style'; color: blue">MHR
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style'; color: blue" colspan="2">Efficiency 
                       
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style'; color: blue">Cycle Time in minutes
                        
                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style'; color: blue">Processing Cost
                       
                                </td>


                            </tr>
                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">Forging
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">

                                    <telerik:RadNumericTextBox ID="txt_MHR1" OnTextChanged="txt_MHR1_TextChanged" AutoPostBack="true" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="0" />
                                    </telerik:RadNumericTextBox>
                                  <%--  <asp:RequiredFieldValidator ID="rfvtxt_MHR1" runat="server" ControlToValidate="txt_MHR1" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                      <asp:RequiredFieldValidator ID="rfvtxt_MHR1" runat="server" ControlToValidate="txt_MHR1"  ErrorMessage="*"  ForeColor="Red"  EnableClientScript="true" ValidationGroup="personalInfo" ></asp:RequiredFieldValidator>
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';" colspan="2">

                                    <%-- <asp:TextBox ID="txt_effeciency1" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_effeciency1" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxt_effeciency1" runat="server" ControlToValidate="txt_effeciency1" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--  <asp:TextBox ID="txt_cycletime1" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_cycletime1" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_cycletime1" runat="server" ControlToValidate="txt_cycletime1" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>


                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="lbl_processingcost1" Text="" Font-Bold="true" runat="server"></asp:Label>



                                </td>

                            </tr>

                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">Forging HT - Hardening & Tempering
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--   <asp:TextBox ID="txt_MHR2" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_MHR2" AutoPostBack="true" OnTextChanged="txt_MHR2_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="0" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_MHR2" runat="server" ControlToValidate="txt_MHR2" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';" colspan="2">

                                    <%--<asp:TextBox ID="txt_effeciency2" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_effeciency2" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_effeciency2" runat="server" ControlToValidate="txt_effeciency2" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--  <asp:TextBox ID="txt_cycletime2" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>
                                    <telerik:RadNumericTextBox ID="txt_cycletime2" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxt_cycletime2" runat="server" ControlToValidate="txt_cycletime2" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="lbl_processingcost2" Text="" Font-Bold="true" runat="server"></asp:Label>


                                </td>

                            </tr>

                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">Rough Machining
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%-- <asp:TextBox ID="txt_MHR3" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_MHR3" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="0" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_MHR3" runat="server" ControlToValidate="txt_MHR3" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';" colspan="2">

                                    <%--                                    <asp:TextBox ID="txt_effeciency3" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_effeciency3" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_effeciency3" runat="server" ControlToValidate="txt_effeciency3" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--  <asp:TextBox ID="txt_cycletime3" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>


                                    <telerik:RadNumericTextBox ID="txt_cycletime3" AutoPostBack="true" OnTextChanged="txt_cycletime3_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxt_cycletime3" runat="server" ControlToValidate="txt_cycletime3" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="lbl_processingcost3" Font-Bold="true" Text="" runat="server"></asp:Label>



                                </td>

                            </tr>
                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">CNC 1st
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--  <asp:TextBox ID="txt_MHR4" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_MHR4" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="0" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_MHR4" runat="server" ControlToValidate="txt_MHR4" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';" colspan="2">

                                    <%-- <asp:TextBox ID="txt_effeciency4" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_effeciency4" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>
                                    <asp:RequiredFieldValidator ID="txttxt_effeciency4" runat="server" ControlToValidate="txt_effeciency4" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--               <asp:TextBox ID="txt_cycletime4" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_cycletime4" AutoPostBack="true" OnTextChanged="txt_cycletime4_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_cycletime4" runat="server" ControlToValidate="txt_cycletime4" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="lbl_processingcost4" Font-Bold="true" Text="" runat="server"></asp:Label>



                                </td>

                            </tr>
                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">CNC 2nd
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--   <asp:TextBox ID="txt_MHR5" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>
                                    <telerik:RadNumericTextBox ID="txt_MHR5" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="0" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_MHR5" runat="server" ControlToValidate="txt_MHR5" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';" colspan="2">

                                    <%-- <asp:TextBox ID="txt_effeciency5" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_effeciency5" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_effeciency5" runat="server" ControlToValidate="txt_effeciency5" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--  <asp:TextBox ID="txt_cycletime5" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>
                                    <telerik:RadNumericTextBox ID="txt_cycletime5" AutoPostBack="true" OnTextChanged="txt_cycletime5_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxt_cycletime5" runat="server" ControlToValidate="txt_cycletime5" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="lbl_processingcost5" Font-Bold="true" Text="" runat="server"></asp:Label>



                                </td>

                            </tr>
                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">VMC 1st
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--<asp:TextBox ID="txt_MHR6" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_MHR6" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="0" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_MHR6" runat="server" ControlToValidate="txt_MHR6" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';" colspan="2">

                                    <%-- <asp:TextBox ID="txt_effeciency6" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_effeciency6" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_effeciency6" runat="server" ControlToValidate="txt_effeciency6" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--       <asp:TextBox ID="txt_cycletime6" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_cycletime6" AutoPostBack="true" OnTextChanged="txt_cycletime6_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_cycletime6" runat="server" ControlToValidate="txt_cycletime6" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="lbl_processingcost6" Font-Bold="true" Text="" runat="server"></asp:Label>


                                </td>

                            </tr>
                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">Part Marking
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--   <asp:TextBox ID="txt_MHR7" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <%--   <telerik:RadNumericTextBox ID="txt_MHR7" RenderMode="Lightweight"  CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None">
                                        <NumberFormat DecimalDigits="0" />
                                    </telerik:RadNumericTextBox>--%>

                                    <%--   <asp:RequiredFieldValidator ID="rfvtxt_MHR7" runat="server" ControlToValidate="txt_MHR7" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>--%>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';" colspan="2">

                                    <%--   <asp:TextBox ID="txt_effeciency7" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <%--   <telerik:RadNumericTextBox ID="txt_effeciency7" RenderMode="Lightweight"  CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None">
                                        <NumberFormat DecimalDigits="0" />
                                    </telerik:RadNumericTextBox>--%>

                                    <%-- <asp:RequiredFieldValidator ID="rfvtxt_effeciency7" runat="server" ControlToValidate="txt_effeciency7" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>--%>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%-- <asp:TextBox ID="txt_cycletime7" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <%--  <telerik:RadNumericTextBox ID="txt_cycletime7" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>--%>

                                    <%-- <asp:RequiredFieldValidator ID="rfvtxt_cycletime7" runat="server" ControlToValidate="txt_cycletime7" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>--%>

                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--  <asp:Label ID="lbl_processingcost7" Font-Bold="true" Text="" runat="server"></asp:Label>--%>

                                    <telerik:RadNumericTextBox ID="lbl_processingcost7" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvlbl_processingcost7" runat="server" ControlToValidate="lbl_processingcost7" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>



                                </td>

                            </tr>
                            <tr>

                                <td colspan="6"></td>
                            </tr>
                            <tr>
                                <td align="justify" style="text-align: center; color: blue" colspan="3">Inspection
               
                                </td>
                                <td align="justify" style="text-align: center;" colspan="3">
                                    <%-- <asp:TextBox ID="txt_Inspection" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_Inspection" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_Inspection" runat="server" ControlToValidate="txt_Inspection" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                            </tr>
                            <tr>
                                <td align="justify" style="text-align: center; color: blue;" colspan="3">Wash & clean
               
                                </td>
                                <td align="justify" style="text-align: center;" colspan="3">
                                    <%--                  <asp:TextBox ID="txt_WashAndclean" runat="server" Font-Bold="true" Height="100%" CssClass="WebHR_TextBox" Width="90%" Enabled="true"></asp:TextBox>--%>
                                    <telerik:RadNumericTextBox ID="txt_WashAndclean" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txt_WashAndclean" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                            </tr>
                            <tr>
                                <td align="justify" style="text-align: center; color: blue" colspan="3">Tool Cost
               
                                </td>
                                <td align="justify" style="text-align: center;" colspan="3">

                                    <telerik:RadNumericTextBox ID="txt_toolCost" AutoPostBack="true" OnTextChanged="txt_toolCost_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="personalInfo" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_toolCost" runat="server" ControlToValidate="txt_toolCost" EnableClientScript="true" ValidationGroup="personalInfo" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                            </tr>
                            <tr>
                                <td align="justify" style="text-align: center; color: blue" colspan="3">Total Processing Cost
               
                                </td>
                                <td align="justify" style="text-align: center;" colspan="3">
                                    <asp:Label ID="txt_total_Processing_Cost" Font-Bold="true" runat="server"></asp:Label>


                                </td>
                            </tr>
                            <tr>
                                <td align="justify" style="text-align: center; color: blue" colspan="3">R/M + Processing
               
                                </td>
                                <td align="justify" style="text-align: center;" colspan="3">
                                    <asp:Label ID="txt_Rm_Processing_cost" Font-Bold="true" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>


                    </telerik:RadWizardStep>

                    <telerik:RadWizardStep ID="RadWizardStep3" Title="Sub Total Ex work cost"
                        runat="server" StepType="Step" ValidationGroup="ContactDetails" SpriteCssClass="contactDetails">

                        <table id="Table3" width="100%" runat="server" style="overflow-x: scroll; table-layout: fixed;">

                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">ICC
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--<asp:TextBox ID="txt_Icc_per" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_Icc_per" AutoPostBack="true" OnTextChanged="txt_Icc_per_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="ContactDetails" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_Icc_per" runat="server" ControlToValidate="txt_Icc_per" EnableClientScript="true" ValidationGroup="ContactDetails" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>


                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="txt_Icc_Onrm1" Text="On RM" runat="server"></asp:Label>

                                </td>


                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="txt_ICC_Procc_Cost" Font-Bold="true" Text="" runat="server"></asp:Label>

                                </td>

                            </tr>

                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">OVER HEADS
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--       <asp:TextBox ID="txt_overhead_per" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>
                                    <telerik:RadNumericTextBox ID="txt_overhead_per" AutoPostBack="true" OnTextChanged="txt_overhead_per_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="ContactDetails" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_overhead_per" runat="server" ControlToValidate="txt_overhead_per" EnableClientScript="true" ValidationGroup="ContactDetails" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="txt_overhead_PerName" Text="On R/M +Processing" runat="server"></asp:Label>
                                </td>


                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="txt_OVERHEADS_proc_cost" Font-Bold="true" Text="" runat="server"></asp:Label>

                                </td>
                            </tr>

                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">REJECTON
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--  <asp:TextBox ID="txt_REJECTON_Per" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>
                                    <telerik:RadNumericTextBox ID="txt_REJECTON_Per" AutoPostBack="true" OnTextChanged="txt_REJECTON_Per_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="ContactDetails" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_REJECTON_Per" runat="server" ControlToValidate="txt_REJECTON_Per" EnableClientScript="true" ValidationGroup="ContactDetails" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">

                                    <asp:Label ID="txt_REJECTON_perName" Text="On R/M +Processing" runat="server"></asp:Label>
                                </td>


                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="txt_REJECTON_proc_cost" Font-Bold="true" Text="" runat="server"></asp:Label>

                                </td>

                            </tr>

                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">PROFIT MARGINE
                 
                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <%--   <asp:TextBox ID="txt_PROFITMargin_per" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_PROFITMargin_per" AutoPostBack="true" OnTextChanged="txt_PROFITMargin_per_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="ContactDetails" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                    <asp:RequiredFieldValidator ID="rfvtxt_PROFITMargin_per" runat="server" ControlToValidate="txt_PROFITMargin_per" EnableClientScript="true" ValidationGroup="ContactDetails" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">

                                    <asp:Label ID="txt_PROFITMARGINE_perName" Text="On R/M +Processing" runat="server"></asp:Label>
                                </td>


                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="txt_PROFITMARGINE_proc_cost" Font-Bold="true" Text="" runat="server"></asp:Label>

                                </td>

                            </tr>

                        </table>

                        <table id="Table4" width="100%" runat="server" style="overflow-x: scroll; table-layout: fixed;">
                            <tr>

                                <td align="center" class="W3_Warning">Sub Total Ex work cost:
              <asp:Label ID="txt_SubTotalExworkcost" Font-Bold="true" ForeColor="Blue" Text="" runat="server" />
                                </td>

                            </tr>
                        </table>

                    </telerik:RadWizardStep>


                    <telerik:RadWizardStep ID="RadWizardStep5" Title="Sub Total (Domestic / Export)" runat="server" ValidationGroup="Confirmation" SpriteCssClass="confirmation" StepType="Finish">


                        <table id="Table5" width="100%" runat="server" style="overflow-x: scroll; table-layout: fixed;">

                            <tr id="tr_ExpoertSea" runat="server">

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">Export Sea Worthy Packing
                 
                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">

                                    <%--  <asp:TextBox ID="txt_ExportSeaWorthyPacking_per" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_ExportSeaWorthyPacking_per" AutoPostBack="true" OnTextChanged="txt_ExportSeaWorthyPacking_per_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="confirmation" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                     <asp:RequiredFieldValidator ID="rfvtxt_ExportSeaWorthyPacking_per" runat="server" ControlToValidate="txt_ExportSeaWorthyPacking_per" EnableClientScript="true" ValidationGroup="confirmation" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>


                                </td>


                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="lbl_ExportSeaWorthyPacking_cost" Font-Bold="true" Text="" runat="server"></asp:Label>

                                </td>

                            </tr>
                            <tr id="tr_SGA" runat="server">

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">S G & A
                 
                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">

                                    <%--                                    <asp:TextBox ID="txt_SGA_per" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_SGA_per" AutoPostBack="true" OnTextChanged="txt_SGA_per_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="confirmation" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                     <asp:RequiredFieldValidator ID="rfvtxt_SGA_per" runat="server" ControlToValidate="txt_SGA_per" EnableClientScript="true" ValidationGroup="confirmation" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>


                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="txt_SG_And_A_Cost" Font-Bold="true" Text="" runat="server"></asp:Label>

                                </td>

                            </tr>
                            <tr id="tr_returnable" runat="server">

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">Returnable Packaging
                 
                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">

                                    <%--   <asp:TextBox ID="txt_returnable_Packing_per" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_returnable_Packing_per" AutoPostBack="true" OnTextChanged="txt_returnable_Packing_per_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="confirmation" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                                                        <asp:RequiredFieldValidator ID="rfvtxt_returnable_Packing_per" runat="server" ControlToValidate="txt_returnable_Packing_per" EnableClientScript="true" ValidationGroup="confirmation" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>


                                </td>


                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="txt_returnable_packing_cost" Font-Bold="true" Text="" runat="server"></asp:Label>

                                </td>

                            </tr>
                            <tr id="tr_transport" runat="server">

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">Transport
                 
                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">

                                    <%--  <asp:TextBox ID="txt_Transport_per" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>
                                    <telerik:RadNumericTextBox ID="txt_Transport_per" AutoPostBack="true" OnTextChanged="txt_Transport_per_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="confirmation" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>

                                      <asp:RequiredFieldValidator ID="rfvtxt_Transport_per" runat="server" ControlToValidate="txt_Transport_per" EnableClientScript="true" ValidationGroup="confirmation" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>


                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="txt_Transport_cost" Font-Bold="true" Text="" runat="server"></asp:Label>

                                </td>

                            </tr>
                        </table>

                        <table id="tbl_fca_cost" width="100%" runat="server" style="overflow-x: scroll; table-layout: fixed;">
                            <tr>

                                <td align="center" class="W3_Warning">Total FCA Cost (In INR):
              <asp:Label ID="txt_TotalFCACost" Font-Bold="true" ForeColor="Blue" Text="" runat="server" />
                                </td>

                            </tr>
                        </table>

                   <%-- </telerik:RadWizardStep>

                    <telerik:RadWizardStep StepType="Finish" Title="Export 2" ValidationGroup="Checksheet" SpriteCssClass="Checksheet">--%>

                        <table id="Table7" width="100%" runat="server" style="overflow-x: scroll; table-layout: fixed;">

                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">Freight Insurance & Clearance
                 
                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">

                                    <%--  <asp:TextBox ID="txt_FreightInsurance_Clearance_per1" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_FreightInsurance_Clearance_per1" AutoPostBack="true" OnTextChanged="txt_FreightInsurance_Clearance_per1_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="Checksheet" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>
                                      <asp:RequiredFieldValidator ID="rfvtxt_FreightInsurance_Clearance_per1" runat="server" ControlToValidate="txt_FreightInsurance_Clearance_per1" EnableClientScript="true" ValidationGroup="confirmation" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>


                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">

                                    <%-- <asp:TextBox ID="txt_FreightInsurance_Clearance_per2" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>

                                    <%--  <telerik:RadNumericTextBox ID="txt_FreightInsurance_Clearance_per2" AutoPostBack="true" OnTextChanged="txt_FreightInsurance_Clearance_per2_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="Checksheet" Resize="None">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>--%>


                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="lbl_FreightInsurance_Clearance_Cost" Text="" runat="server"></asp:Label>

                                </td>

                            </tr>


                        </table>

                        <table id="Table8" width="100%" runat="server" style="overflow-x: scroll; table-layout: fixed;">
                            <tr>

                                <td align="center" class="W3_Warning">Total DAP Cost (In INR):
              <asp:Label ID="txt_TotalDAPCostInINR" Font-Bold="true" ForeColor="Blue" Text="56565" runat="server" />
                                </td>

                            </tr>
                        </table>

                        <br />
                        <br />
                        <table id="Table9" width="100%" runat="server" style="overflow-x: scroll; table-layout: fixed;">
                            <tr>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">Currency Conversion Factor
                 
                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">

                                    <%--  <asp:TextBox ID="txt_CurrencyConversionFactor_per1" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>

                                    <telerik:RadNumericTextBox ID="txt_CurrencyConversionFactor_per1" AutoPostBack="true" OnTextChanged="txt_CurrencyConversionFactor_per1_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="Checksheet" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>
                                     <asp:RequiredFieldValidator ID="rfvtxt_CurrencyConversionFactor_per1" runat="server" ControlToValidate="txt_CurrencyConversionFactor_per1" EnableClientScript="true" ValidationGroup="confirmation" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>

                                </td>
                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">

                                    <%--   <asp:TextBox ID="txt_CurrencyConversionFactor_per2" runat="server" Height="100%" CssClass="WebHR_TextBox" Width="100%" Enabled="true"></asp:TextBox>--%>
                                    <telerik:RadNumericTextBox ID="txt_CurrencyConversionFactor_per2" AutoPostBack="true" OnTextChanged="txt_CurrencyConversionFactor_per2_TextChanged" RenderMode="Lightweight" CausesValidation="false" runat="server" Width="90%" ValidationGroup="Checksheet" Resize="None" MinValue="0">
                                        <NumberFormat DecimalDigits="2" />
                                    </telerik:RadNumericTextBox>
                                      <asp:RequiredFieldValidator ID="rfvtxt_CurrencyConversionFactor_per2" runat="server" ControlToValidate="txt_CurrencyConversionFactor_per2" EnableClientScript="true" ValidationGroup="confirmation" ErrorMessage="*"  ForeColor="Red"></asp:RequiredFieldValidator>


                                </td>

                                <td align="justify" style="text-align: center; font-family: 'Bookman Old Style';">
                                    <asp:Label ID="lbl_CurrencyConversionFactor_cost" Text="" runat="server"></asp:Label>

                                </td>

                            </tr>
                        </table>


                        <table id="Table10" width="100%" runat="server" style="overflow-x: scroll; table-layout: fixed;">
                            <tr>

                                <td align="center" class="W3_Warning">Total DAP Cost (€):
              <asp:Label ID="lbl_TotalDAPCost_2" Font-Bold="true" ForeColor="Blue" Text="56565" runat="server" />
                                </td>

                            </tr>
                        </table>
                    </telerik:RadWizardStep>


                    <%--   <telerik:RadWizardStep ID="RadWizardStep4" runat="server" Title="Export 2"  StepType="Complete" CssClass="complete">
                        <p style="font-size: 35px">You have created successfully Audit Plan!</p>
                        <telerik:RadButton RenderMode="Lightweight" ID="NewRegistrationButton" runat="server" OnClick="NewRegistrationButton_Click" Font-Bold="true" Font-Size="Medium" Width="100px" Height="50px" Text="GO HOME"></telerik:RadButton>
                    </telerik:RadWizardStep>--%>
                </WizardSteps>
            </telerik:RadWizard>





           

            <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                ProviderName="System.Data.SqlClient" SelectCommand="SELECT [Terms] FROM [tbl_InCo_terms]"></asp:SqlDataSource>--%>
            <%--  <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                ProviderName="System.Data.SqlClient" SelectCommand="SELECT [Shift] FROM [tblShiftMaster]"></asp:SqlDataSource>--%>
        </div>
    </form>
</body>
</html>
