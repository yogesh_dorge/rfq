﻿<%@ page title="" language="C#" masterpagefile="~/Pages/Admin.master" autoeventwireup="true" inherits="FormSMTPSettingsas, App_Web_3kw0abyc" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">
    <%--<link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>

    <style type="text/css">
        .not-active
        {
            pointer-events: none;
            cursor: default;
            color: gray!important;
        }

        .active
        {
            color: blue!important;
        }
    </style>
    <br />
    <section id="main-content">
        <section class="wrapper">
            <div class="divForm" style="padding: 1px; border: thin solid #000000; position: relative; width: 100%;"
                align="center">
                <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
                    <Localization OK="Ok" Cancel="No" />
                </telerik:RadWindowManager>
                <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                </telerik:RadScriptManager>
                <table id="tblMain" align="center" width="100%">
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="Label5" runat="server" Font-Bold="true" Font-Size="X-Large" Text="SMTP SETTING MASTER" CssClass="WebHR_Heading1"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label9" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>

                        <td align="left" width="50%">
                            <asp:Label ID="Label10" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="50%">
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" Text="Email ID:  "></asp:Label>
                        </td>
                        &nbsp;
                        <td align="left" width="45%">
                            <asp:TextBox ID="txtEmailId" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="*" ControlToValidate="txtEmailid"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>




                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label11" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label12" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="50%">
                            <asp:Label ID="Label2" runat="server" Font-Size="Small" Text="Password:  "></asp:Label>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtPassword" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"
                                TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>


                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label25" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label26" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="50%">
                            <asp:Label ID="Label3" runat="server" Font-Size="Small" Text="Server Name:  ">
                            </asp:Label>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtServerName" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvServerName" runat="server" ErrorMessage="*This field is required" ControlToValidate="txtServerName"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>


                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label13" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label14" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label4" runat="server" Font-Size="Small" Text="Port Number:  "></asp:Label>
                        </td>
                        <td align="left" width="40%">
                            <asp:TextBox ID="txtPortNo" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*This field is required" ControlToValidate="txtPortNo"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label15" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label16" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label6" runat="server" Font-Size="Small" Text="Email Using Gmail:  "></asp:Label>
                        </td>
                        <td align="left" width="40%">
                            <asp:TextBox ID="txtEmailUsingGmail" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*This field is required" ControlToValidate="txtEmailUsingGmail"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label17" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label18" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label7" runat="server" Font-Size="Small" Text="Email Server IP:  "></asp:Label>
                        </td>
                        <td align="left" width="40%">
                            <asp:TextBox ID="txtEmailServerIP" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*This field is required" ControlToValidate="txtEmailServerIP"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label19" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label20" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label8" runat="server" Font-Size="Small" Text="Enable Ssl:  "></asp:Label>
                        </td>
                        <td align="left" width="40%">
                            <asp:TextBox ID="txtEnableSsl" runat="server" Font-Size="Small" Height="25px" Width="250" CssClass="WebHR_TextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ControlToValidate="txtEnableSsl"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label21" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label22" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>



                    <tr>
                        <td width="50%" colspan="2" align="center">
                            <asp:Button ID="btnSave" runat="server" Font-Size="Small" CssClass="Buttons_Blue" OnClick="btnSave_Click"
                                Text="SAVE" Width="74px" Style="height: 26px" />
                            <asp:Button ID="btnTestEmail" runat="server" Font-Size="Small" CssClass="Buttons_Blue" OnClick="btnTestEmail_Click"
                                Text="Test Email" Width="80px" Style="height: 26px" />
                        </td>
                    </tr>



                    <tr>
                        <td align="right" width="40%">
                            <asp:Label ID="Label23" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                        <td align="right" width="50%">
                            <asp:Label ID="Label24" runat="server" Text="." ForeColor="White"></asp:Label>
                        </td>
                    </tr>

                    <tr align="center">
                        <td align="right">
                            <asp:TextBox ID="txtTestEmailTo" runat="server" CssClass="WebHR_TextBox" Visible="false"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="rfvtxtTestEmailTo" runat="server" ErrorMessage="*" ControlToValidate="txtTestEmailTo"
                            ForeColor="Red" Enabled="false"></asp:RequiredFieldValidator>--%>
                        
                        </td>
                        <td align="left">
                            <asp:Button ID="btnSendEmail" runat="server" CssClass="Buttons_Blue" OnClick="btnSendEmail_Click"
                                Text="Send Email" Width="80px" Style="height: 26px" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td width="50%"></td>
                        <td align="right" width="50%">
                            <asp:LinkButton ID="Back" runat="server" CausesValidation="False" OnClick="Back_Click"
                                PostBackUrl="~/Pages/Dashboard.aspx">Back</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </div>
        </section>
    </section>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
</asp:Content>
