﻿<%@ page language="C#" autoeventwireup="true" maintainscrollpositiononpostback="true" masterpagefile="~/Pages/Admin.master" inherits="FormRFQ_AssignOwner, App_Web_xcmovcei" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--  <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%-- <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Styles/semantic.css" rel="stylesheet" />
    <link href="../Styles/semantic.min.css" rel="stylesheet" />
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/style-responsive.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>    

    </center>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <br />

    <script type="text/javascript">
        function DisableButton() {
            document.getElementById("<%=btnSave.ClientID %>").disabled = true;

        }
        window.onbeforeunload = DisableButton;

    </script>

    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="WebBlue" />

    <section id="main-content">
        <section class="wrapper">
            <div class="divForm" style="padding: 1px; border: thin solid #000000; position: relative; width: 100%;"
                align="center">
                <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
                    <Localization OK="Ok" Cancel="No" />
                </telerik:RadWindowManager>
                <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                </telerik:RadScriptManager>


                <div class="ui last container">
                    <div class="ui five steps">
                        <div id="Activestep" runat="server">
                            <div class="content">
                                <div class="title" id="rfqtitle" runat="server">RFQ Generation</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lblRFQcreated" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="step" id="AssignUser_Title" runat="server">
                            <div class="content">
                                <div class="title">Assign Owner</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_AssignUser_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="OwnerAvability_Title" runat="server">
                            <div class="content">
                                <div class="title">Component Feasibility</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_OwnerAvability_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="Last_Step_Title" runat="server">
                            <div class="content">
                                <div id="Div1" class="title" runat="server">Quotation</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_Last_Step_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="Approvaed" runat="server">
                            <div class="content">
                                <div id="Div2" class="title" runat="server">Approval</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="Label10" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <br />

                <telerik:RadGrid RenderMode="Lightweight" AutoGenerateColumns="false" ID="rgvRFQ" runat="server" Skin="Bootstrap" AllowPaging="false"
                    AllowMultiRowSelection="true" ClientSettings-Scrolling-AllowScroll="true"
                    ClientSettings-Resizing-AllowResizeToFit="true" Height="66px" OnNeedDataSource="rgvRFQ_NeedDataSource" HeaderStyle-HorizontalAlign="Center">
                    <%--   <GroupingSettings CaseSensitive="false"></GroupingSettings>--%>
                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="RFQ_ID">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="RFQ No" ReadOnly="true" HeaderButtonType="TextButton" DataField="RFQ_ID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Customer Name" ReadOnly="true" HeaderButtonType="TextButton" DataField="Customer_Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="RFQ Type" ReadOnly="true" HeaderButtonType="TextButton" DataField="RFQ_Type"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Project" ReadOnly="true" HeaderButtonType="TextButton" DataField="Product_Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Product or Component" ReadOnly="true" HeaderButtonType="TextButton" DataField="Sub_Product_Name"></telerik:GridBoundColumn>

                            <telerik:GridDateTimeColumn HeaderText="Generated Date" DataFormatString="{0:dd-MMM-yyyy}" EnableTimeIndependentFiltering="true" HeaderButtonType="TextButton" DataField="Generated_Datetime"></telerik:GridDateTimeColumn>

                            <telerik:GridDateTimeColumn HeaderText="Committed Date" DataFormatString="{0:dd-MMM-yyyy}" EnableTimeIndependentFiltering="true" HeaderButtonType="TextButton" DataField="CommittedDate"></telerik:GridDateTimeColumn>

                            <telerik:GridBoundColumn HeaderText="Remark" ReadOnly="true" HeaderButtonType="TextButton" DataField="Remark"></telerik:GridBoundColumn>


                            <telerik:GridBoundColumn HeaderText="Status" ReadOnly="true" HeaderButtonType="TextButton" DataField="Status"></telerik:GridBoundColumn>

                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>

                <br />

                <div style="text-align: center; font-family: 'Bookman Old Style';">
                    <asp:Label ID="Label5" runat="server" Font-Bold="true" Font-Size="X-Large" Text="ASSIGN RFQ OWNER" CssClass="WebHR_Heading1"></asp:Label>
                </div>
                <br />
                <div style="text-align: left; font-family: 'Bookman Old Style';">
                    <asp:Label ID="Label16" runat="server" Text="(Note : (*) fields are Mandatory)" ForeColor="Red"></asp:Label>
                </div>
                <br />

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <fieldset style="border: none;">

                            <div style="text-align: center; font-family: 'Bookman Old Style';">
                                &nbsp  &nbsp  &nbsp  &nbsp  &nbsp   
                    <asp:Label ID="Label1" runat="server" Font-Size="Small" Text="RFQ Owner:  "></asp:Label>

                                <telerik:RadComboBox RenderMode="Lightweight" Font-Size="Small" CausesValidation="true" ID="cmbOwner" OnSelectedIndexChanged="cmbOwner_SelectedIndexChanged" Filter="StartsWith" AutoPostBack="false" runat="server" Width="255"
                                    EmptyMessage="Select Owner Name">
                                </telerik:RadComboBox>
                                <asp:Label ID="Asterisk_cmbOwner" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </div>


                            <asp:RequiredFieldValidator ID="rfvcmbOwner" SetFocusOnError="true" EnableClientScript="false" runat="server" ErrorMessage="Required Field Owner Name" ControlToValidate="cmbOwner"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                            <br />

                            <div style="font-family: 'Bookman Old Style';">
                                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp   &nbsp 
                     <asp:Label ID="Label7" runat="server" Font-Size="Small" Text="Remark:">
                     </asp:Label>

                                <asp:TextBox ID="txtRemark" runat="server" Font-Size="Small" CausesValidation="true" CssClass="WebHR_TextBox" Height="50px" Width="250"></asp:TextBox>
                                <asp:Label ID="Asterisk_txtRemark" runat="server" Text="*" ForeColor="Red"></asp:Label>


                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" EnableClientScript="false" runat="server" ErrorMessage="Reqired Field Remark"
                                ForeColor="Red" SetFocusOnError="true" ControlToValidate="txtRemark"></asp:RequiredFieldValidator>

                            <br />
                        </fieldset>
                    </ContentTemplate>
                    <Triggers>
                    </Triggers>
                </asp:UpdatePanel>
                <br />
                <div style="text-align: center;">
                    &nbsp  &nbsp  &nbsp
                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-success" OnClientClick="javascript:ShowOverlay();" CausesValidation="true" OnClick="btnSave_Click1"
                        Text="SAVE" />

                </div>

                <br />

                <br />

            </div>
        </section>
    </section>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
</asp:Content>
