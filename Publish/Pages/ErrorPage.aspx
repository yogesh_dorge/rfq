﻿<%@ page title="" language="C#" autoeventwireup="true" inherits="Pages_ErrorPage, App_Web_tcx0gt55" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        </telerik:RadScriptManager>
        <telerik:RadWindowManager ID="rmw1" runat="server">
        </telerik:RadWindowManager>
        
    
    <div>
        <asp:Panel ID="Panel1" runat="server" Width="79%">
            <table style="width: 58%;" frame="box">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;
                    </td>
                    <td>
                        <br />
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Not an Authenticated User"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="Button1" runat="server" CssClass="Buttons_Blue" Text="OK" Width="91px"
                            PostBackUrl="~/Pages/Dashboard.aspx" OnClick="Button1_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
        </form>
</body>
</html>
