﻿<%@ page title="" language="C#" masterpagefile="~/Pages/Admin.master" autoeventwireup="true" inherits="Pages_MasterSub_ProductMaster, App_Web_3kw0abyc" maintainscrollpositiononpostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script runat="server">
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">

    <%--  <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
    <style type="text/css">
        .rgRow, .rgAltRow
        {
            height: 40px;
        }
    </style>
    <script type="text/javascript">


        function UserAction(sender, args) {
            if (sender.get_batchEditingManager().hasChanges(sender.get_masterTableView()) &&
                !confirm("Any changes will be cleared. Are you sure you want to perform this action?")) {
                args.set_cancel(true);
            }
        }

    </script>


    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>

    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="WebBlue" />

    <telerik:RadWindowManager ID="rmw1" runat="server">
    </telerik:RadWindowManager>

    <section id="main-content">
        <section class="wrapper">

            <br />
            <br />
            <div style="padding-top: 20px; padding-left: 10px">
            </div>
            <div style="align-items: center; text-align: center;">
                <asp:Label ID="Label1" Font-Size="Large" Font-Bold="true" runat="server" CssClass="WebHR_Heading1"
                    Text="Manage Product or Component Master"></asp:Label>
            </div>

            <br />
            <br />

            <div class="demo-container no-bg">
                <%--<h3>RadGrid bound to RadClientDataSource</h3>--%>
                <div id="grid">
                      <asp:Label ID="Label16" runat="server" Text="(Note : (*) fields are Mandatory)" ForeColor="Red"></asp:Label>
                            <br />
                    <telerik:RadGrid RenderMode="Lightweight" ID="rgvSubProductMaster" runat="server" AllowPaging="true" AllowSorting="true" AllowFilteringByColumn="true" AutoGenerateColumns="false" OnNeedDataSource="rgvSubProductMaster_NeedDataSource" OnBatchEditCommand="rgvSubProductMaster_BatchEditCommand">
                          <ClientSettings AllowKeyboardNavigation="true">
                            <KeyboardNavigationSettings AllowActiveRowCycle="true"/>

                            <Selecting AllowRowSelect="true"/>
                        </ClientSettings>
                        <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" IgnorePaging="true" FileName="Product or Component Details">
                        </ExportSettings>
                           <GroupingSettings CaseSensitive="false"></GroupingSettings>
                        <MasterTableView ClientDataKeyNames="Sub_Product_Code" EditMode="Batch" CommandItemDisplay="Top" DataKeyNames="Sub_Product_Code">
                            <%--BatchEditingSettings-HighlightDeletedRows="true"--%>
                            <BatchEditingSettings EditType="Cell" />
                            <CommandItemSettings ShowExportToCsvButton="true" ShowExportToExcelButton="true" />

                            <Columns>

                                <telerik:GridBoundColumn DataField="Sub_Product_Code" HeaderText="Product or Component Code(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator ForeColor="Red" ErrorMessage="*This field is required"></RequiredFieldValidator>
                                    </ColumnValidationSettings>
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="Sub_Product_Name" HeaderText="Product or Component Name(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator ForeColor="Red" ErrorMessage="*This field is required"></RequiredFieldValidator>
                                    </ColumnValidationSettings>
                                </telerik:GridBoundColumn>

                                <telerik:GridTemplateColumn HeaderText="Project Code(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" UniqueName="Product_Code" DataField="Product_Code">
                                    <ItemTemplate>
                                        <%# Eval("Product_Code") %>
                                      
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                   <%--     <telerik:RadDropDownList RenderMode="Lightweight" runat="server" ID="Product_CodeDropDown" DataValueField="Product_Code"
                                            DataTextField="Product_Name" DataSourceID="SqlDataSource1">
                                        </telerik:RadDropDownList>--%>

                                        <telerik:RadComboBox RenderMode="Lightweight" Font-Size="Small" ID="cmbProductCode" OnSelectedIndexChanged="cmbProductCode_SelectedIndexChanged" Filter="StartsWith" DataSourceID="SqlDataSource1" DataValueField="Product_Code"
                                            DataTextField="Product_Code" AutoPostBack="false" runat="server"
                                            EmptyMessage="Select Project Code">
                                        </telerik:RadComboBox>

                                        <asp:RequiredFieldValidator ID="rfvcmbProductCode" SetFocusOnError="true" EnableClientScript="false" runat="server" ErrorMessage="*" ControlToValidate="cmbProductCode"
                                            ForeColor="Red"></asp:RequiredFieldValidator>

                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>

                                 <telerik:GridBoundColumn DataField="Product_Name" ReadOnly="true" HeaderText="Project Name(*)" HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ColumnEditorID="GridTextBoxEditor">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                     
                                    </ColumnValidationSettings>
                                </telerik:GridBoundColumn>




                                <telerik:GridClientDeleteColumn HeaderText="Delete">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridClientDeleteColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnUserAction="UserAction" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </div>
                <telerik:GridTextBoxColumnEditor ID="GridTextBoxEditor" runat="server" TextBoxStyle-Width="175px"></telerik:GridTextBoxColumnEditor>

                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [Product_Code],[Product_Name] FROM [tbl_ProductMaster]"></asp:SqlDataSource>


            </div>
        </section>
    </section>

</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
</asp:Content>

