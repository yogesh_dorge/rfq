﻿<%@ page language="C#" autoeventwireup="true" inherits="ProductWiseRfqReport, App_Web_ysfbq0bl" masterpagefile="~/Pages/Admin.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

      <%--<telerik:RadDatePicker runat="server" ID="RadDatePickerTo" DateInput-DateFormat="dd/MMM/yyyy"  DateInput-DisplayDateFormat="dd/MMM/yyyy" DateInput-ReadOnly="true">
        </telerik:RadDatePicker>--%>
   

    <script type="text/javascript">
        function pageLoad() {
            var grid = $find("<%= rgvRecords.ClientID %>");
            var columns = grid.get_masterTableView().get_columns();
            for (var i = 0; i < columns.length; i++) {
                columns[i].resizeToFit();
            }
        }
    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

     <section id="main-content">
        <section class="wrapper">
      <center>
        <div style="padding: 10px;">
            <asp:Label ID="Label1" runat="server" Text="PROJECT WISE REPORT" Font-Bold="true" Font-Size ="XX-Large"></asp:Label>
            <br />
        </div>

        <div id="headingOne" role="tab" class="panel-heading">

             
        <asp:Label ID="Label2" runat="server" Text="From " Font-Bold="true"></asp:Label>
        <%--<telerik:RadDatePicker runat="server" ID="RadDatePickerFrom" DateInput-DateFormat="dd/MMM/yyyy"  DateInput-DisplayDateFormat="dd/MMM/yyyy" DateInput-ReadOnly="true">
        </telerik:RadDatePicker>--%>
            <asp:TextBox ID="txtFromDate" runat="server" CssClass="WebHR_TextBox" Height="20px" ></asp:TextBox>
                        <cc1:CalendarExtender ID="txtFromDate_CalendarExtender" runat="server" TargetControlID="txtFromDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>

        
                        <asp:RequiredFieldValidator ID="CodeValidator0" runat="server" ErrorMessage="Enter Date"
                            ForeColor="Red" ControlToValidate="txtFromDate"></asp:RequiredFieldValidator>
         
        &nbsp
        <asp:Label ID="Label3" runat="server" Text="To " Font-Bold="true"></asp:Label>
        <%--<telerik:RadDatePicker runat="server" ID="RadDatePickerTo" DateInput-DateFormat="dd/MMM/yyyy"  DateInput-DisplayDateFormat="dd/MMM/yyyy" DateInput-ReadOnly="true">
        </telerik:RadDatePicker>--%>
            <asp:TextBox ID="txtToDate" runat="server" CssClass="WebHR_TextBox" Height="20px" ></asp:TextBox>
                        <cc1:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" TargetControlID="txtToDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="CodeValidator1" runat="server" ErrorMessage="Enter Date"
                            ForeColor="Red" ControlToValidate="txtToDate"></asp:RequiredFieldValidator>

        &nbsp  



             <%--<asp:Label ID="lblDept" runat="server" Text="Department " Font-Bold="true"></asp:Label>
              <telerik:RadComboBox RenderMode="Lightweight" ID="cmbDepartment"  Filter="StartsWith" AutoPostBack="false" runat="server" 
                                EmptyMessage="Select Department" DataSourceID="SqlDataSource3" DataTextField="DeptCode" DataValueField="DeptCode">
                            </telerik:RadComboBox>
                         <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                ProviderName="System.Data.SqlClient" SelectCommand="SELECT [DeptCode] FROM [tblDepartmentMaster]"></asp:SqlDataSource>
                          <asp:RequiredFieldValidator ID="rfvcmbDepartment" runat="server" ErrorMessage="*" ControlToValidate="cmbDepartment"
                            ForeColor="Red"></asp:RequiredFieldValidator>--%>
            &nbsp
        <%--<telerik:RadButton ID="btnFilterApply" runat="server" Text="Apply" OnClick="btnFilterApply_Click">
        </telerik:RadButton>--%>
            <asp:Button ID="btnFilterApply" runat="server" Text="Apply" OnClick="btnFilterApply_Click"></asp:Button>
    </div>

        <div class="divForm" style="padding: 1px; border: thin solid #000000; position: relative;">
            <telerik:RadScriptManager runat="server" ID="scriptManager1">
            </telerik:RadScriptManager>

    
             <telerik:RadGrid RenderMode="Lightweight" AutoGenerateColumns="False"  ID="rgvRecords"
                 PageSize="<%$ appSettings:ReportPageSize %>" AllowFilteringByColumn="True"  Skin="WebBlue"  OnDetailTableDataBind="rgvRecords_DetailTableDataBind" OnNeedDataSource="rgvRecords_NeedDataSource"   OnItemCreated="rgvRecords_ItemCreated" runat="server" OnDataBinding="rgvRecords_DataBinding" GroupPanelPosition="Top" GroupingSettings-CaseSensitive="false" >
                  <ClientSettings>
       <Scrolling AllowScroll="True" UseStaticHeaders="true" />
              <%--        <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />  --%>
   </ClientSettings>

                <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" IgnorePaging="true" FileName="Product Wise Report">
<Excel Format="ExcelML"></Excel>
                </ExportSettings>
                
                <MasterTableView GroupLoadMode="Client" TableLayout="Auto" DataKeyNames="RFQ_ID" CommandItemDisplay="Top" AlternatingItemStyle-HorizontalAlign="Left"  >
                    <CommandItemSettings ShowExportToExcelButton="true"  ShowExportToCsvButton="true" ShowAddNewRecordButton="false"  />
       
                    <Columns>
                       <%-- <telerik:GridButtonColumn Text="Select" ItemStyle-ForeColor="RoyalBlue"  ItemStyle-Font-Underline="true"  CommandName="Select_ID"  ButtonType="LinkButton">--%>

                                                                        
      

                <telerik:GridBoundColumn SortExpression="RFQ_ID"  HeaderText="RFQ ID" Visible="true" HeaderButtonType="TextButton"
                                    DataField="RFQ_ID" >
                                </telerik:GridBoundColumn>

                          <telerik:GridBoundColumn SortExpression="Customer_Name"  HeaderText="Customer Name" HeaderButtonType="TextButton"
                                    DataField="Customer_Name">
                                </telerik:GridBoundColumn>


                         <telerik:GridBoundColumn SortExpression="Customer_EmailId"  HeaderText="Customer Email Id" HeaderButtonType="TextButton"
                                    DataField="Customer_EmailId">
                                </telerik:GridBoundColumn>

                  
                         <telerik:GridDateTimeColumn SortExpression="Generated_Datetime"  HeaderText="Generated Date" HeaderButtonType="TextButton" EnableTimeIndependentFiltering="true"
                                    DataField="Generated_Datetime" DataFormatString="{0:dd-MMM-yyyy}">
                                </telerik:GridDateTimeColumn>



                                <telerik:GridDateTimeColumn SortExpression="CommittedDate"  HeaderText="Committed Date" HeaderButtonType="TextButton"
                                    DataField="CommittedDate" DataFormatString="{0:dd-MMM-yyyy}">
                                </telerik:GridDateTimeColumn>

                  <%--       <telerik:GridBoundColumn SortExpression="Employee_Name"  HeaderText="Employee Name" HeaderButtonType="TextButton"
                                    DataField="Employee_Name">
                                </telerik:GridBoundColumn>
                      --%>

                        <telerik:GridBoundColumn SortExpression="Status"  HeaderText="Status" HeaderButtonType="TextButton"
                                    DataField="Status">
                                </telerik:GridBoundColumn>

             <%--   <telerik:GridBoundColumn SortExpression="At_Stage"  HeaderText="At Stage" HeaderButtonType="TextButton"
                                 DataField="At_Stage">
                             </telerik:GridBoundColumn>
                  
                     --%>     

                                  <telerik:GridBoundColumn SortExpression="Remark"  HeaderText="Remark" HeaderButtonType="TextButton"
                                    DataField="Remark">
                                </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn SortExpression="Risk_Analysis_Text"  HeaderText="Risk Analysis Text" HeaderButtonType="TextButton"
                                    DataField="Risk_Analysis_Text">
                                </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn SortExpression="Business_Analysis_Text"  HeaderText="Business Analysis Text" HeaderButtonType="TextButton"
                                    DataField="Business_Analysis_Text" >
                            </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn SortExpression="Product_Code"  HeaderText="Project Code" HeaderButtonType="TextButton"
                                    DataField="Product_Code">
                                </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Product_Name"  HeaderText="Project Name" HeaderButtonType="TextButton"
                                    DataField="Product_Name" >
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Sub_Product_Code"  HeaderText="Product Or Component Name" HeaderButtonType="TextButton"
                                    DataField="Sub_Product_Code">
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Sub_Product_Name"  HeaderText="Product Or Component Name" HeaderButtonType="TextButton"
                                    DataField="Sub_Product_Name" >
                            </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn SortExpression="RFQ_Type"  HeaderText="RFQ Type" HeaderButtonType="TextButton"
                                    DataField="RFQ_Type">
                                </telerik:GridBoundColumn>


                              </Columns>

<AlternatingItemStyle HorizontalAlign="Left"></AlternatingItemStyle>
                </MasterTableView>
                  <ClientSettings AllowGroupExpandCollapse="True" ReorderColumnsOnClient="True" AllowDragToGroup="True"
                    AllowColumnsReorder="True">
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="true" />

<FilterItemStyle HorizontalAlign="Left"></FilterItemStyle>

<FilterMenu RenderMode="Lightweight"></FilterMenu>

<HeaderContextMenu RenderMode="Lightweight"></HeaderContextMenu>
            </telerik:RadGrid>
             <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
                        </telerik:RadWindowManager>
        </div>
    </center>
            </section>
         </section>

 </asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
 </asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
 </asp:Content>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
  
</asp:Content>--%>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
  
</asp:Content>--%>
