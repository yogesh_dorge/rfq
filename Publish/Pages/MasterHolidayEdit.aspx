﻿<%@ page title="" language="C#" masterpagefile="~/Site.master" autoeventwireup="true" inherits="Pages_MasterHolidayEdit, App_Web_p2aj4fm0" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }

        .filterHeight {
        min-height:100px;
        z-index:6999;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <center>
        <div id="FormDiv1" class="divForm" style="padding: 1px; border: thin solid #000000;
            position: relative; top: 0px; right: -2px; left: 2px; height: 100%; width: 100%;" align="left">
            <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
            </telerik:RadScriptManager>
            <telerik:RadWindowManager ID="rmw1" runat="server">
            </telerik:RadWindowManager>
            <br />
            <table class="style1">
                <tr>
                    <td align="center">
                        <asp:Label ID="lblUserHeadning" runat="server" CssClass="WebHR_Heading1" Text="MANAGE HOLIDAY'S"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Image ID="Image7" runat="server" Height="16px" ImageUrl="~/Images/add-icon.gif" />
                        <asp:LinkButton ID="linkAddDept" runat="server" Font-Bold="True" OnClick="LinkButton1_Click"
                            Font-Size="Small">Add Holiday</asp:LinkButton>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False"
                            AllowPaging="True" Skin="Outlook" PageSize="<%$appSettings:PageSize%>"  OnDeleteCommand="RadGrid1_DeleteCommand" 
OnItemCreated="RadGrid1_ItemCreated" 
OnNeedDataSource="RadGrid1_NeedDataSource" 
OnUpdateCommand="RadGrid1_UpdateCommand"
                            AutoGenerateEditColumn="True">
                            <ClientSettings>
        <Scrolling AllowScroll="True" UseStaticHeaders="true" />
                                                        <Selecting AllowRowSelect="True" />
                            </ClientSettings>

                                    <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" IgnorePaging="true" FileName="Holiday">
                </ExportSettings>

                            <MasterTableView DataKeyNames="Holiday_ID" CommandItemDisplay="Top" >
                                <CommandItemSettings ExportToPdfText="Export to PDF" ShowExportToCsvButton="true" ShowExportToExcelButton="true" ShowAddNewRecordButton="false"/>
                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                               
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Holiday_ID" EmptyDataText="NA" FilterControlAltText="Filter Holiday_ID column"
                                        HeaderText="Holiday ID" ReadOnly="True" UniqueName="Holiday_ID" Visible="false">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Holiday_Date" EmptyDataText="NA" FilterControlAltText="Filter Holiday_Date column"
                                        HeaderText="Holiday Date" UniqueName="Holiday_Date" HeaderStyle-Width="150px" DataFormatString="{0:dd-MMM-yyyy}" >
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Holiday_Date_RTC" EmptyDataText="NA" FilterControlAltText="Filter Holiday_Date_RTC column"
                                        HeaderText="Holiday_Date_RTC" UniqueName="Holiday_Date_RTC" Visible="false">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Holiday_Details" EmptyDataText="NA" FilterControlAltText="Filter Holiday_Details column"
                                        HeaderText="Holiday Details" UniqueName="Holiday_Details">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="" UniqueName="RoleDelete" AllowFiltering="false" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="DeleteLink" runat="server" CommandName="Delete" Text="Delete"
                                                OnClientClick="javascript:if(!confirm('Are you sure to delete?')){return false;}">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <PagerStyle AlwaysVisible="True" />
                           
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </div>
    </center>
    <%--</body>
</html>--%>
</asp:Content>
