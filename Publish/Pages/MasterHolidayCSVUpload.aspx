﻿<%@ page title="" language="C#" masterpagefile="~/Site.master" autoeventwireup="true" inherits="Pages_MasterHolidayCSVUpload, App_Web_050thnq4" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    
    <div id="FormDiv1" class="divForm" style="padding: 1px; border: thin solid #000000; position: relative; top: 0px; right: -2px; left: 2px; height: 100%; width: 100%;"
        align="left">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        </telerik:RadScriptManager>
        <telerik:RadWindowManager ID="rmw1" runat="server">
        </telerik:RadWindowManager>
        <br />
        <table style="width: 100%" borderstyle="Solid">
            <tr>
                <td>
                    <asp:Label ID="lblUserHeadning" runat="server" CssClass="WebHR_Heading1"
                        Text="CSV Holiday Uploading"></asp:Label>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td align="center">

                    <asp:Label Text="Project " runat="server" Font-Size="Large" Visible="false"></asp:Label>
                    <telerik:RadComboBox ID="cmbProject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cmbProject_SelectedIndexChanged" Visible="false"></telerik:RadComboBox>

                </td>

            </tr>
        </table>
        <br />
        <table style="width: 100%;">
            <tr>
                <td align="center">
                    <asp:Panel ID="pnlUpload" runat="server" Width="50%" BorderColor="Black">
                        <asp:FileUpload ID="FileUploadControl" runat="server"  />&nbsp;&nbsp;
                        <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click"
                            Text="Upload" /><br /><br />
                        <asp:Label ID="StatusLabel" runat="server" Font-Bold="True"
                            ForeColor="#3333FF" Font-Size="Large"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="RadGrid1" runat="server" AutoGenerateColumns="False"
                        Width="100%" OnRowCommand="RadGrid1_RowCommand"
                        OnSelectedIndexChanged="RadGrid1_SelectedIndexChanged" BorderStyle="Solid"
                        BorderColor="#333333" BorderWidth="1px" Font-Names="Verdana"
                        Font-Overline="False" Font-Size="12px" GridLines="None" ShowFooter="True"
                        OnPreRender="RadGrid1_PreRender">
                        <AlternatingRowStyle BackColor="#EBEBEB" Font-Names="Verdana" Font-Size="16px"
                            Height="30px" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <Columns>
                            <asp:BoundField DataField="ProjectName" HeaderText="Project Name">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TemplateName" HeaderText="Template Name">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <%--<asp:HyperLinkField DataNavigateUrlFields="TemplatePath" Text="Download File"
                                DataNavigateUrlFormatString="{0}" HeaderText="Path">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:HyperLinkField>--%>
                            <asp:BoundField DataField="TemplateUplodedDate" HeaderText="Template Uploded Date">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TemplateVersion" HeaderText="Template Version">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle BackColor="#EBEBEB" BorderStyle="Solid" BorderWidth="2px"
                            Font-Names="Verdana" Height="30px" BorderColor="#333333" />
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Back</asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>

