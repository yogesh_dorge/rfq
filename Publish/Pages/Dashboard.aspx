﻿<%@ page title="Dashboard" language="C#" masterpagefile="~/Pages/Admin.master" autoeventwireup="true" inherits="Pages_Dashboard, App_Web_ysfbq0bl" maintainscrollpositiononpostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Charting" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <!--Empty-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--Empty-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">

    <script type="text/javascript">
        window.history.forward();
        function noBack() { window.history.forward(); }

        function ShowTickets(custId, status) {

        }
    </script>
    <style type="text/css">
        .not-active
        {
            pointer-events: none;
            cursor: default;
            color: gray!important;
        }

        .active
        {
            color: blue!important;
        }
    </style>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
    <telerik:RadWindowManager ID="rmw1" runat="server" Skin="Outlook">
    </telerik:RadWindowManager>
    <section id="main-content">
        <section class="wrapper">
            <div style="padding-top: 20px; padding-left: 10px">
            </div>

            <div style="padding-top: 20px">

                <!--Half Row-->

                <div class="col-md-6 col-sm-6 mb">
                    <div class="white-panel pn" style="height: 150px;">
                        <div class="white-header">
                            <%--<h5>Documents for your approval...</h5>--%>
                            <h5>RFQ Quotation Documents for your approval</h5>
                        </div>

                        <div align="center">
                            <asp:ListView ID="ListView2" runat="server" EnableTheming="True">
                                <LayoutTemplate>
                                    <table id="Table1" runat="server" class="TableCSS">
                                        <tr id="Tr1" runat="server" class="TableHeader">
                                            <td id="Td1" runat="server">--
                                            </td>
                                            <td id="Td2" runat="server">--
                                            </td>
                                        </tr>
                                        <tr id="ItemPlaceholder" runat="server">
                                        </tr>
                                        <tr id="Tr2" runat="server">
                                            <td id="Td3" runat="server" colspan="2">
                                                <asp:DataPager ID="DataPager1" runat="server" Visible="False">
                                                    <Fields>
                                                        <asp:NextPreviousPagerField ButtonType="Link" />
                                                        <asp:NumericPagerField />
                                                        <asp:NextPreviousPagerField ButtonType="Link" />
                                                    </Fields>
                                                </asp:DataPager>
                                            </td>
                                        </tr>
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr class="TableData">
                                        <td align="left">
                                            <div style="border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: #F1F1F1">
                                                A new <b><a href='<%# Eval("Link")%>'>RFQ No Approval</a></b> has been requested.<p>
                                                    By <em><a style="color: #0000FF">
                                                        <%# Eval("RFQ_ID")%>&nbsp;</a></em>for Customer &nbsp;<em class="gry" style="color: darkblue;">
                                                          <%# Eval("Customer_Name")%>  </em>
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                            <br />
                            <asp:Label ID="lblApprovalMessage" runat="server" Text="No Documents pending for approval..."
                                Visible="False"></asp:Label>
                        </div>

                        <%--<br />
                        <asp:Label Text="TOTAL CONSUMED HOURS: " runat="server" ID="lblConsumedHours" Font-Size="X-Large" Font-Bold="true">
                        </asp:Label>
                        <asp:Label runat="server" ID="lblConsumedHoursVal" Font-Size="X-Large" Font-Bold="true"></asp:Label>
                        <br />--%>
                        <div>
                            <asp:Label runat="server" ID="LblStatus" Font-Bold="true">
                            </asp:Label>
                        </div>
                        <br />
                    </div>
                    <asp:Label runat="server" ID="lblNote1" Font-Size="Smaller" Font-Bold="true" ForeColor="Red" align="Left"></asp:Label>
                    <br />
                    <asp:Label runat="server" ID="lblNote2" Font-Size="Smaller" Font-Bold="true" ForeColor="Red" align="Left"></asp:Label>
                </div>
            </div>
            <!--Half Row-->

            <div class="col-md-6 col-sm-6 mb">
                <div class="white-panel pn" style="height: 150px;">
                    <div class="white-header">
                        <%-- <h5>Rejected CAR Log</h5>--%>
                        <h5>No-Go for the RFQ</h5>

                    </div>
                    <telerik:RadGrid ID="No_Go_Grid" CssClass="margin_box" HeaderStyle-ForeColor="RoyalBlue" HeaderStyle-Font-Bold="true"  HorizontalAlign="Center" AutoGenerateColumns="false" runat="server" Font-Size="Small" OnNeedDataSource="No_Go_Grid_NeedDataSource" GroupingEnabled="false" Skin="Simple" ClientSettings-Resizing-AllowResizeToFit="true"  ClientSettings-Scrolling-AllowScroll="false" OnItemCommand="No_Go_Grid_ItemCommand" OnItemDataBound="No_Go_Grid_ItemDataBound">
                        <ClientSettings>
                            <Selecting CellSelectionMode="None" />
                        </ClientSettings>
                        <MasterTableView>
                            <%--  <CommandItemSettings ExportToPdfText="Export to PDF" />--%>
                           <%-- <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                <HeaderStyle Width="10px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                <HeaderStyle Width="10px" />
                            </ExpandCollapseColumn>--%>
                            <Columns>

                                 
                                 <telerik:GridHyperLinkColumn DataTextFormatString="{0}"
                                DataNavigateUrlFields="RFQ_ID" ItemStyle-ForeColor="RoyalBlue"  UniqueName="RFQ_ID"
                                HeaderText="RFQ Number" DataTextField="RFQ_ID" ItemStyle-Font-Underline="true" HeaderStyle-HorizontalAlign="Center"/>

                                <%--<telerik:GridBoundColumn HeaderText="RFQ ID" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" DataField="RFQ_ID" ></telerik:GridBoundColumn>--%>
                                <telerik:GridBoundColumn HeaderText="Rejected By" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" DataField="STAGE3_Owner"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Status" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" DataField="Stage4_Owner_GO_NoGo"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Reason" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Stage4_NotGo_Reason"></telerik:GridBoundColumn>

                                <%--                                <telerik:GridTemplateColumn UniqueName="AuditNo" HeaderStyle-Font-Size="Medium" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left" SortExpression="Count" HeaderText="AuditNo" DataField="AuditNo">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hypCount" runat="server" CssClass="active" Font-Underline="true"></asp:HyperLink>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>--%>
                            </Columns>

                      <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                </EditColumn>
                            </EditFormSettings>
                        </MasterTableView>
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                    
                    <div style="text-align: right; padding-right: 15px;">
                        <%--<asp:Label Text="TOTAL COUNT: " runat="server" ID="lblTicketTotalCount" Font-Size="X-Large" Font-Bold="true">
                        </asp:Label>
                        <asp:Label runat="server" ID="lblTicketTotalCountVal" Font-Size="X-Large" Font-Bold="true"></asp:Label>--%>
                        <asp:LinkButton ID="Link_RejectCAR_Log" runat="server" Font-Bold="True" ForeColor="SteelBlue" OnClick="Link_RejectCAR_Log_Click">View All</asp:LinkButton>
                    </div>
                </div>
            </div>

            <!--Full Row-->
            <div class="col-md-12 col-sm-12 mb">
                <div class="white-panel pn" style="height: 350px;">
                    <div class="white-header">
                        <h5><b>Created RFQ</b></h5>
                    </div>

                    <telerik:RadGrid ID="RadGrid1" runat="server" CssClass="margin_box" HeaderStyle-ForeColor="RoyalBlue" HeaderStyle-Font-Bold="true" HorizontalAlign="Center" AutoGenerateColumns="false" OnNeedDataSource="RadGrid1_NeedDataSource" ClientSettings-Scrolling-AllowScroll="false" ClientSettings-Resizing-AllowResizeToFit="true" OnItemDataBound="RadGrid1_ItemDataBound" Skin="Simple" OnItemCommand="RadGrid1_ItemCommand">
                        <ClientSettings>
                            <Selecting CellSelectionMode="None" />
                        </ClientSettings>
                        <MasterTableView>
                            <%--  <CommandItemSettings ExportToPdfText="Export to PDF" />--%>
                            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                <HeaderStyle Width="10px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                <HeaderStyle Width="10px" />
                            </ExpandCollapseColumn>
                            <Columns>

                                <%--<telerik:GridHyperLinkColumn DataTextFormatString="{0}"
                                    DataNavigateUrlFields="RFQ_ID" HeaderStyle-ForeColor="RoyalBlue" ItemStyle-Font-Underline="true" ItemStyle-ForeColor="RoyalBlue" DataNavigateUrlFormatString="" UniqueName="RFQ_ID"
                                    HeaderText="RFQ ID" DataTextField="RFQ_ID" />--%>

                                 <telerik:GridHyperLinkColumn DataTextFormatString="{0}"
                                DataNavigateUrlFields="RFQ_ID" ItemStyle-ForeColor="RoyalBlue"  UniqueName="RFQ_ID"
                                HeaderText="RFQ Number" DataTextField="RFQ_ID" ItemStyle-Font-Underline="true" HeaderStyle-HorizontalAlign="Center"/>

                              
                              
                                <telerik:GridBoundColumn HeaderText="Customer Name" HeaderButtonType="TextButton" HeaderStyle-HorizontalAlign="Center"
                                    DataField="Customer_Name">
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Generated Date" HeaderButtonType="TextButton" HeaderStyle-HorizontalAlign="Center"
                                    DataField="Generated_Datetime">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Committed Date" HeaderButtonType="TextButton" HeaderStyle-HorizontalAlign="Center"
                                    DataField="CommittedDate">
                                </telerik:GridBoundColumn>
                               <telerik:GridBoundColumn HeaderText="Project" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Product_Name"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Sub-Project" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Sub_Product_Name"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Status" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" DataField="Status"></telerik:GridBoundColumn>
                                <%--<telerik:GridBoundColumn HeaderText="Transaction By" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" DataField="STAGE3_Owner"></telerik:GridBoundColumn>--%>
                                <telerik:GridBoundColumn SortExpression="Remark" HeaderText="Remark" HeaderButtonType="TextButton" HeaderStyle-HorizontalAlign="Center"
                                    DataField="Remark">
                                </telerik:GridBoundColumn>
                              
                            </Columns>
                            <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                </EditColumn>
                            </EditFormSettings>
                        </MasterTableView>
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>

                    <div style="text-align: right; padding-right: 15px;">
                        <%--  <asp:Label Text="VIEW ALL " runat="server" ID="lblTotalHours" Font-Size="X-Large" Font-Bold="true">
                        </asp:Label>
                        <asp:Label runat="server" ID="lblTotalHoursVal" Font-Size="X-Large" Font-Bold="true"></asp:Label>--%>
                        <asp:LinkButton ID="Link_RFQDetails" runat="server" Font-Bold="True" ForeColor="SteelBlue" OnClick="Link_RFQDetails_Click">View All</asp:LinkButton>

                    </div>
                </div>
            </div>

            <!--Full Row-->

            <div class="col-md-12 col-sm-12 mb">
                <div class="white-panel pn" style="height: 350px;">
                    <div class="white-header">
                        <h5><b>Completed RFQ</b></h5>
                    </div>

                    <telerik:RadGrid ID="rgvapproval" runat="server" CssClass="margin_box" HeaderStyle-ForeColor="RoyalBlue" HeaderStyle-Font-Bold="true" HorizontalAlign="Center" Width="100%" AutoGenerateColumns="false" OnNeedDataSource="rgvapproval_NeedDataSource" OnItemDataBound="rgvapproval_ItemDataBound" ClientSettings-Scrolling-AllowScroll="false" ClientSettings-Resizing-AllowResizeToFit="true" Skin="Simple" OnItemCommand="rgvapproval_ItemCommand">
                        <ClientSettings>
                            <Selecting CellSelectionMode="None" />
                        </ClientSettings>
                        <MasterTableView>
                            <%--  <CommandItemSettings ExportToPdfText="Export to PDF" />--%>
                            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                <HeaderStyle Width="10px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                <HeaderStyle Width="10px" />
                            </ExpandCollapseColumn>
                            <Columns>
                               
                                  
                                 <telerik:GridHyperLinkColumn DataTextFormatString="{0}"
                                DataNavigateUrlFields="RFQ_ID" ItemStyle-ForeColor="RoyalBlue"  UniqueName="RFQ_ID"
                                HeaderText="RFQ Number" DataTextField="RFQ_ID" ItemStyle-Font-Underline="true" HeaderStyle-HorizontalAlign="Center"/>

                                <%--<telerik:GridBoundColumn HeaderText="RFQ ID"  HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" DataField="RFQ_ID" ></telerik:GridBoundColumn>--%>
                                <telerik:GridBoundColumn HeaderText="RFQ Type" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="RFQ_Type"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Customer_Name"></telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Generated Date" HeaderButtonType="TextButton" HeaderStyle-HorizontalAlign="Center" DataField="Generated_Datetime"> </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Committed Date" HeaderButtonType="TextButton" HeaderStyle-HorizontalAlign="Center" DataField="CommittedDate"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Project" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Product_Name"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Sub-Project" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Sub_Product_Name"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Status" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" DataField="Status"></telerik:GridBoundColumn>
                                <%--<telerik:GridBoundColumn HeaderText="Transaction By" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" DataField="STAGE3_Owner"></telerik:GridBoundColumn>--%>
                            </Columns>
                            <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                </EditColumn>
                            </EditFormSettings>
                        </MasterTableView>
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>


                    <div style="text-align: right; padding-right: 15px;">
                        <asp:LinkButton ID="linkNCCarDetails" runat="server" Font-Bold="True" ForeColor="SteelBlue" OnClick="linkNCCarDetails_Click">View All</asp:LinkButton>
                    </div>
                </div>
            </div>


               <!--Full Row-->

            <div class="col-md-12 col-sm-12 mb">
                <div class="white-panel pn" style="height: 350px;">
                    <div class="white-header">
                        <h5><b>Pending Risk And Business Analysis</b></h5>
                    </div>

                    <telerik:RadGrid ID="radriskBusiness" runat="server" CssClass="margin_box" HeaderStyle-ForeColor="RoyalBlue" HeaderStyle-Font-Bold="true" HorizontalAlign="Center" Width="100%" AutoGenerateColumns="false" OnNeedDataSource="radriskBusiness_NeedDataSource" OnItemDataBound="radriskBusiness_ItemDataBound" ClientSettings-Scrolling-AllowScroll="false" ClientSettings-Resizing-AllowResizeToFit="true" Skin="Simple" OnItemCommand="radriskBusiness_ItemCommand">
                        <ClientSettings>
                            <Selecting CellSelectionMode="None" />
                        </ClientSettings>
                        <MasterTableView>
                            <%--  <CommandItemSettings ExportToPdfText="Export to PDF" />--%>
                            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                <HeaderStyle Width="10px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                <HeaderStyle Width="10px" />
                            </ExpandCollapseColumn>
                            <Columns>
                               
                             <%--     
                                 <telerik:GridHyperLinkColumn DataTextFormatString="{0}"
                                DataNavigateUrlFields="RFQ_ID" ItemStyle-ForeColor="RoyalBlue"  UniqueName="RFQ_ID"
                                HeaderText="RFQ Number" DataTextField="RFQ_ID" ItemStyle-Font-Underline="true" HeaderStyle-HorizontalAlign="Center"/>--%>
                                                              
                                <telerik:GridBoundColumn HeaderText="Customer ID" HeaderStyle-HorizontalAlign="Center" Visible="false" ItemStyle-HorizontalAlign="Center" DataField="Customer_ID"></telerik:GridBoundColumn>

                               <%--  <telerik:GridHyperLinkColumn DataTextFormatString="{0}"
                                DataNavigateUrlFields="Customer_Name" ItemStyle-ForeColor="RoyalBlue"  UniqueName="Customer_Name"
                                HeaderText="Customer Name" DataTextField="Customer_Name" ItemStyle-Font-Underline="true" HeaderStyle-HorizontalAlign="Center"/>--%>
                                
                                   <telerik:GridHyperLinkColumn 
                                    DataNavigateUrlFields="Customer_ID,Customer_Name" HeaderStyle-ForeColor="RoyalBlue" ItemStyle-Font-Underline="true" ItemStyle-ForeColor="RoyalBlue" DataNavigateUrlFormatString="../Pages/FormRFQ_Risk_Business1.aspx?Customer_ID={0}&Customer_Name={1}"   UniqueName="Customer_Name"
                                    HeaderText="Customer_Name" DataTextField="Customer_Name" />

<%--                                <telerik:GridBoundColumn HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Customer_Name"></telerik:GridBoundColumn>--%>

                              
                                <telerik:GridBoundColumn HeaderText="Email Id" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="EmailId"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Contact No" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="ContactNo"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Company Name" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" DataField="CompanyName"></telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn HeaderText="Company Address" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" DataField="CompanyAddress"></telerik:GridBoundColumn>
                               
                            </Columns>
                            <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                </EditColumn>
                            </EditFormSettings>
                        </MasterTableView>
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>


                    <div style="text-align: right; padding-right: 15px;">
                        <asp:LinkButton ID="btn_link_risk_Businees" runat="server" Font-Bold="True" ForeColor="SteelBlue" OnClick="btn_link_risk_Businees_Click">View All</asp:LinkButton>
                    </div>
                </div>
            </div>

            <br />
            <br />
        </section>
    </section>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <!--Empty-->
</asp:Content>
