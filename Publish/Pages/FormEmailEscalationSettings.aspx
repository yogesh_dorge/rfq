﻿<%@ page language="C#" autoeventwireup="true" masterpagefile="~/Site.master" inherits="Pages_FormSMTPSettings, App_Web_p2aj4fm0" validaterequest="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <center>
        
        <div style="padding: 1px; border: thin solid #000000; position: relative; width: 50%;"
            align="center" class="WebHR_box">
            <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
            </telerik:RadWindowManager>
            <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
            </telerik:RadScriptManager>
            <table id="tblMain" align="center" width="100%">
                <tr>
                    <td align="center" colspan="2">
                        <asp:Label ID="Label5" runat="server" Text="EMAIL ESCALATION SETTINGS" 
                            CssClass="WebHR_Heading1"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" width="40%">
                        &nbsp;</td>
                    <td align="left" width="60%">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label6" runat="server" Text="Before" Visible="false"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                        <asp:Label ID="Label7" runat="server" Text="After" Visible="false"></asp:Label>

                    &nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label1" runat="server" Text="Task Owner"></asp:Label>
                    </td>
                    <td align="left">
<%--                        <asp:DropDownList ID="txtUserDue" runat="server" CssClass="WebHR_TextBox">
                            <asp:ListItem Value="0">NONE</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                        </asp:DropDownList>--%>
                    &nbsp;&nbsp;
                        
                        <asp:DropDownList ID="txtTaskOwner" runat="server" CssClass="WebHR_TextBox">
                            <asp:ListItem Value="0">NONE</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                        </asp:DropDownList>
                        
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label2" runat="server" Text="Level 1"></asp:Label>
                    </td>
                    <td align="left">
<%--                        <asp:DropDownList ID="txtDeptDue" runat="server" CssClass="WebHR_TextBox">
                            <asp:ListItem Value="0">NONE</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                        </asp:DropDownList>--%>
                    &nbsp;&nbsp;
                        
                        <asp:DropDownList ID="txtLevel1" runat="server" CssClass="WebHR_TextBox">
                            <asp:ListItem Value="0">NONE</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                        </asp:DropDownList>
                        
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label3" runat="server" Text="Level 2"></asp:Label>
                    </td>
                    <td align="left">
                        <%--<asp:DropDownList ID="txtPMDue" runat="server" CssClass="WebHR_TextBox">
                            <asp:ListItem Value="0">NONE</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                        </asp:DropDownList>--%>
                    &nbsp;&nbsp;
                        
                        <asp:DropDownList ID="txtLevel2" runat="server" CssClass="WebHR_TextBox">
                            <asp:ListItem Value="0">NONE</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                        </asp:DropDownList>
                        
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label4" runat="server" Text="Level 3"></asp:Label>
                    </td>
                    <td align="left">
<%--                        <asp:DropDownList ID="txtDeptDue" runat="server" CssClass="WebHR_TextBox">
                            <asp:ListItem Value="0">NONE</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                        </asp:DropDownList>--%>
                    &nbsp;&nbsp;
                        
                        <asp:DropDownList ID="txtLevel3" runat="server" CssClass="WebHR_TextBox">
                            <asp:ListItem Value="0">NONE</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                        </asp:DropDownList>
                        
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label9" runat="server" Text="DT" Visible="false"></asp:Label>
                    </td>
                    <td align="left">
<%--                        <asp:DropDownList ID="txtDeptDue" runat="server" CssClass="WebHR_TextBox">
                            <asp:ListItem Value="0">NONE</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                        </asp:DropDownList>--%>
                    &nbsp;&nbsp;
                        
                        <asp:DropDownList ID="txtDTOverDue" runat="server" CssClass="WebHR_TextBox" Visible="false">
                            <asp:ListItem Value="0">NONE</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                        </asp:DropDownList>
                        
                    </td>
                </tr>
                
                <tr>
                    <td align="right">
                        <asp:Label ID="Label8" runat="server" Text="Daily Email Time" Visible="False"></asp:Label>
                    </td>
                    <td align="left" width="60%">
                        <telerik:RadTimePicker ID="RadTimePicker1" Runat="server" Visible="False">
                        </telerik:RadTimePicker>
                    </td>
                </tr>
                
                <tr>
                    <td align="right">
                        &nbsp;</td>
                    <td align="left" width="60%">
                        <asp:Label ID="lblEg" runat="server" Text="(e.g. 09:00 AM)" Visible="False"></asp:Label>
                    </td>
                </tr>
                
                <tr>
                    <td width="50%" colspan="2" align="center">
                        <asp:Button ID="btnSave" runat="server" CssClass="Buttons_Blue" OnClick="btnSave_Click"
                            Text="SAVE" Width="74px" Style="height: 26px" />
                    </td>
                </tr>
                <tr>
                    <td width="50%" align="right" colspan="2" class="style1">
                        <asp:LinkButton ID="Back" runat="server" CausesValidation="False" OnClick="Back_Click">Back</asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
    </center>
</asp:Content>
