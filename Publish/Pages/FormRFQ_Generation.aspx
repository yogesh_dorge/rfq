﻿<%@ page language="C#" autoeventwireup="true" masterpagefile="~/Pages/Admin.master" maintainscrollpositiononpostback="true" inherits="FormRFQ_Generation, App_Web_3kw0abyc" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--  <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%-- <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Styles/semantic.css" rel="stylesheet" />
    <link href="../Styles/semantic.min.css" rel="stylesheet" />
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/style-responsive.css" rel="stylesheet" />

    <script>
        function RestrictChar() {
            // get the keycode when the user pressed any key in application 
            var exp = String.fromCharCode(window.event.keyCode)
            //Below line will have the special characters that is not allowed you can add if you want more for some characters you need to add escape sequence
            var r = new RegExp("[-_,/'().0-9a-zA-Z \r]", "g");
            if (exp.match(r) == null) {
                window.event.keyCode = 0
                return false;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
       

    </center>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <br />
    <script type="text/javascript">
        function DisableButton() {
            document.getElementById("<%=btn_SendMail.ClientID %>").disabled = true;
        }
        window.onbeforeunload = DisableButton;
    </script>
    <script src="scripts.js" type="text/javascript"></script>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="WebBlue" />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <section id="main-content">
        <section class="wrapper">
            <div class="divForm" style="padding: 1px; border: thin solid #000000; position: relative; width: 100%;"
                align="center">
                <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
                    <Localization OK="Ok" Cancel="No" />
                </telerik:RadWindowManager>
                <%-- <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                </telerik:RadScriptManager>--%>


                <div class="ui last container">
                    <div class="ui five steps">
                        <div id="Activestep" runat="server">
                            <div class="content">
                                <div class="title" id="rfqtitle" runat="server">RFQ Generation</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lblRFQcreated" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="step" id="AssignUser_Title" runat="server">
                            <div class="content">
                                <div class="title">Assign Owner</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_AssignUser_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="OwnerAvability_Title" runat="server">
                            <div class="content">
                                <div class="title">Owner Feasibility</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_OwnerAvability_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="Last_Step_Title" runat="server">
                            <div class="content">
                                <div class="title" runat="server">Quotation</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="lbl_Last_Step_Title" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="step" id="Approvaed" runat="server">
                            <div class="content">
                                <div id="Div2" class="title" runat="server">Approval</div>
                                <div class="description">
                                    <asp:Label runat="server" ID="Label10" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <br />


                <div style="text-align: center; font-family: 'Bookman Old Style';">
                    <asp:Label ID="Label5" runat="server" Font-Bold="true" Font-Size="X-Large" Text="RFQ Generation" CssClass="WebHR_Heading1"></asp:Label>
                </div>
                <br />

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <fieldset style="border: none;">
                            <div style="text-align: left; font-family: 'Bookman Old Style';">
                                <asp:Label ID="Label16" runat="server" Text="(Note : (*) fields are Mandatory)" ForeColor="Red"></asp:Label>
                            </div>
                            <br />




                            <div style="text-align: center; font-family: 'Bookman Old Style';">
                                &nbsp  &nbsp  &nbsp  &nbsp  &nbsp      &nbsp  &nbsp  &nbsp  &nbsp  &nbsp   &nbsp  &nbsp  &nbsp  &nbsp  &nbsp
                    <asp:Label ID="Label1" runat="server" Font-Size="Small" Text="Customer Name:  "></asp:Label>

                                <%--                    <telerik:RadComboBox RenderMode="Lightweight" Font-Size="Small" ID="cmbCustomerName" OnSelectedIndexChanged="cmbCustomerName_SelectedIndexChanged" Filter="StartsWith" AutoPostBack="false" runat="server" Width="255"
                        EmptyMessage="Select Name">
                    </telerik:RadComboBox>--%>


                                <telerik:RadComboBox RenderMode="Lightweight" runat="server" AutoPostBack="True" DropDownAutoWidth="Enabled" ID="cmbCustomerName" Skin="Outlook" Height="190px" Width="250px"
                                    MarkFirstMatch="true" EnableLoadOnDemand="true" 
                                    HighlightTemplatedItems="true" EmptyMessage="Select Customer Name" OnTextChanged="cmbCustomerName_TextChanged" OnSelectedIndexChanged="cmbCustomerName_SelectedIndexChanged" CausesValidation="true" OnClientItemsRequested="UpdateItemCountField"
                                    OnDataBound="cmbCustomerName_DataBound" OnItemDataBound="cmbCustomerName_ItemDataBound"
                                    OnItemsRequested="cmbCustomerName_ItemsRequested">

                                    <HeaderTemplate>


                                        <table runat="server" width="400">
                                            <tr>
                                                <%--<td style="width: 50px">Customer ID</td>--%>
                                                <td style="width: 250px">Customer Name</td>
                                                <td style="width: 150px">Email ID</td>

                                            </tr>
                                        </table>

                                    </HeaderTemplate>
                                    <ItemTemplate>


                                        <table runat="server" width="400">
                                            <tr>
                                                <%--  <td style="width: 50px"><%# DataBinder.Eval(Container.DataItem, "Customer_ID") %></td>--%>
                                                <td style="width: 250px"><%# DataBinder.Eval(Container.DataItem, "Customer_Name") %></td>
                                                <td style="width: 150px"><%# DataBinder.Eval(Container.DataItem, "EmailId") %></td>

                                            </tr>
                                        </table>


                                    </ItemTemplate>
                                    <FooterTemplate>
                                        A total of
                <asp:Literal runat="server" ID="RadComboItemsCount" />
                                        items
                                    </FooterTemplate>
                                </telerik:RadComboBox>

                                <asp:SqlDataSource ID="SessionDataSource1" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                                    SelectCommand="SELECT * FROM [tbl_CustomerMaster]"></asp:SqlDataSource>

                                <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                    <script type="text/javascript">
                                        function UpdateItemCountField(sender, args) {
                                            //Set the footer text.
                                            sender.get_dropDownElement().lastChild.innerHTML = "A total of " + sender.get_items().get_count() + " items";
                                        }
                                        function OnClientSelectedIndexChanged(sender, args) {
                                            __doPostBack('cmbCustomerName', '');
                                        }
                                    </script>
                                </telerik:RadScriptBlock>
                                <asp:Label ID="Asterisk_CustomerName" runat="server" Text="*" ForeColor="Red"></asp:Label>



                                <asp:Button ID="btnNewCustomer" runat="server" CssClass="btn btn-info" OnClick="btnNewCustomer_Click"
                                    Text="+" />
                            </div>
                            <asp:RequiredFieldValidator ID="rfvcmbCustomerName" SetFocusOnError="true" EnableClientScript="false" runat="server" ErrorMessage="Required Field Customer Name" ControlToValidate="cmbCustomerName"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                            <br />

                            <div style="text-align: center; font-family: 'Bookman Old Style';">
                                &nbsp  &nbsp  &nbsp  &nbsp  &nbsp    &nbsp  &nbsp   &nbsp  &nbsp  &nbsp  &nbsp  &nbsp   &nbsp  &nbsp  &nbsp   
                    <asp:Label ID="Label9" runat="server" Font-Size="Small" Text="RFQ Type:  "></asp:Label>

                                <telerik:RadComboBox RenderMode="Lightweight" Font-Size="Small" ID="cmbRFQ_Type" Filter="StartsWith" AutoPostBack="false" runat="server" Width="255"
                                    EmptyMessage="Select Type">
                                </telerik:RadComboBox>
                                <asp:Label ID="Asterisk_RFQ_Type" runat="server" Text="*" ForeColor="Red"></asp:Label>


                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="true" EnableClientScript="false" runat="server" ErrorMessage="Required Field RFQ Type" ControlToValidate="cmbRFQ_Type"
                                ForeColor="Red"></asp:RequiredFieldValidator>

                            <br />
                            <div style="text-align: center; font-family: 'Bookman Old Style';">
                                &nbsp  &nbsp   &nbsp  &nbsp  &nbsp  &nbsp  &nbsp   &nbsp  &nbsp  &nbsp  
                     <asp:Label ID="Label3" runat="server" Font-Size="Small" Text="Committed Date:">
                     </asp:Label>
                                <asp:TextBox ID="txtcommitted_Date" runat="server" Font-Size="Small" CssClass="WebHR_TextBox" Height="25px" Width="250"></asp:TextBox>
                                <cc1:CalendarExtender ID="txtcommitted_Date_CalendarExtender" runat="server" TargetControlID="txtcommitted_Date"
                                    DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                                </cc1:CalendarExtender>
                                <asp:Label ID="Asterisk_txtcommitted_Date" runat="server" Text="*" ForeColor="Red"></asp:Label>


                            </div>
                            <asp:RequiredFieldValidator ID="CodeValidator0" runat="server" ErrorMessage="Required Field Commited Date"
                                ForeColor="Red" EnableClientScript="false" SetFocusOnError="true" ControlToValidate="txtcommitted_Date"></asp:RequiredFieldValidator>
                            <br />
                            <div style="font-family: 'Bookman Old Style'; text-align: center;">
                                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp   &nbsp     &nbsp  &nbsp  &nbsp  &nbsp      &nbsp  &nbsp  &nbsp  &nbsp   
                     <asp:Label ID="Label7" runat="server" Font-Size="Small" Text="Remark:">
                     </asp:Label>

                                <asp:TextBox ID="txtRemark" runat="server" Font-Size="Small" CssClass="WebHR_TextBox" Height="35px" Width="250" OnTextChanged="txtRemark_TextChanged"></asp:TextBox>
                                <asp:Label ID="Asterisk_txtRemark" runat="server" Text="*" ForeColor="Red"></asp:Label>


                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required Field Remark"
                                ForeColor="Red" EnableClientScript="false" SetFocusOnError="true" ControlToValidate="txtRemark"></asp:RequiredFieldValidator>

                            <br />

                            <div style="text-align: center; font-family: 'Bookman Old Style';">
                                &nbsp  &nbsp  &nbsp  &nbsp      &nbsp  &nbsp  &nbsp  &nbsp       &nbsp  &nbsp  &nbsp  &nbsp   
                    <asp:Label ID="Label6" runat="server" Font-Size="Small" Text="Project Name:  "></asp:Label>





                                <telerik:RadComboBox RenderMode="Lightweight" Font-Size="Small" ID="cmbProduct_Name" OnSelectedIndexChanged="cmbProduct_Name_SelectedIndexChanged" Filter="StartsWith" AutoPostBack="true" runat="server" Width="255"
                                    EmptyMessage="Select Project Name">
                                </telerik:RadComboBox>
                                <asp:Label ID="Asterisk_cmbProduct_Name" runat="server" Text="*" ForeColor="Red"></asp:Label>


                            </div>
                            <asp:RequiredFieldValidator ID="rcfcmbProduct_Name" SetFocusOnError="true" EnableClientScript="false" runat="server" ErrorMessage="Required Field Project Name" ControlToValidate="cmbProduct_Name"
                                ForeColor="Red"></asp:RequiredFieldValidator>
                            <br />
                            <div style="text-align: center; font-family: 'Bookman Old Style';">

                                <asp:Label ID="Label8" runat="server" Font-Size="Small" Text="Product or Component Name:  "></asp:Label>

                                <telerik:RadComboBox RenderMode="Lightweight" Font-Size="Small" ID="cmbSubProductName" OnSelectedIndexChanged="cmbSubProductName_SelectedIndexChanged" Filter="StartsWith" AutoPostBack="true" runat="server" Width="255"
                                    EmptyMessage="Select Product or Component Name">
                                </telerik:RadComboBox>
                                <asp:Label ID="Asterisk_cmbSubProductName" runat="server" Text="*" ForeColor="Red"></asp:Label>


                            </div>
                            <asp:RequiredFieldValidator ID="rcfcmbSubProductName" SetFocusOnError="true" EnableClientScript="false" runat="server" ErrorMessage="Required Field Product or Component Name" ControlToValidate="cmbSubProductName"
                                ForeColor="Red"></asp:RequiredFieldValidator>

                            <br />
                            <br />
                            <div style="text-align: center; font-family: 'Bookman Old Style';">
                                &nbsp  &nbsp  &nbsp  &nbsp   
                    <asp:CheckBox ID="chk_RirkAnalysis" Font-Bold="true" Text="Risk Analysis" runat="server" />
                                &nbsp
                    <asp:CheckBox ID="chk_BusinessAnalysis" Font-Bold="true" Text="Business Analysis" runat="server" />
                            </div>
                            <br />
                            <div>
                                &nbsp  &nbsp  &nbsp  &nbsp   
                    <asp:Label ID="Label4" runat="server" Text="Risk Analysis: "></asp:Label>
                                <asp:Label ID="lblRisk" runat="server" Text=""></asp:Label>
                                <br />
                                <br />
                                &nbsp  &nbsp  &nbsp  &nbsp   
                    <asp:Label ID="Label11" runat="server" Text="Business Analysis: "></asp:Label>
                                <asp:Label ID="lblBusiness" runat="server" Text=""></asp:Label>
                                <br />

                            </div>
                            <br />
                        </fieldset>
                    </ContentTemplate>
                    <Triggers>
                        
                    </Triggers>
                </asp:UpdatePanel>
                <div style="text-align: center; font-family: 'Bookman Old Style';">
                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-success" OnClientClick="javascript:ShowOverlay();" OnClick="btnSave_Click"
                        Text="SAVE" />

                </div>

                <telerik:RadWindow ID="Rw_PrintOption" runat="server" Modal="true" ReloadOnShow="true"
                    OpenerElementID="btnSave" Title="Mail Send" VisibleStatusbar="false" VisibleTitlebar="false" Width="400px" Behaviors="Close">
                    <ContentTemplate>
                        <br />
                        <table width="100%" align="center" class="summaryTable" style="background-color: #82CAFA">
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>

                            <tr>
                                <td align="center" style="text-align: center">
                                    <asp:Label ID="Label2" runat="server" Font-Size="Small" Text="Cells Head:">
                                    </asp:Label>
                                    &nbsp

                           <%--         <telerik:RadComboBox RenderMode="Lightweight" ID="cmb_CellHead_Email" DataValueField="UserEmail" DataTextField="UserEmail" DataSourceID="SqlDataSource2" EnableCheckAllItemsCheckBox="true" CheckBoxes="true" Filter="StartsWith" AutoPostBack="false" runat="server"
                                        EmptyMessage="Select TO Recipients ">
                                    </telerik:RadComboBox>--%>



                                    <telerik:RadComboBox RenderMode="Lightweight" ID="cmb_CellHead_Email" DataValueField="UserEmail" DataTextField="UserEmail" DataSourceID="SqlDataSource2" EnableCheckAllItemsCheckBox="true" Filter="StartsWith" AutoPostBack="false" runat="server">
                                    </telerik:RadComboBox>

                                    <asp:Label ID="lblcmb_CellHead_Email" runat="server" Text="*" ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
                                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                                        ProviderName="System.Data.SqlClient" SelectCommand="SELECT  UserEmail  FROM [tbl_UserMaster] where Role_Code='RFQ Cell Head'"></asp:SqlDataSource>

                                </td>
                            </tr>

                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>

                            <%--  <tr>
                    <td align="center" colspan="2" style="text-align: center">&nbsp  &nbsp  &nbsp &nbsp 
                                    <asp:Label ID="Label4" runat="server" Font-Size="Small" Text="CC:">
                                    </asp:Label>
                        &nbsp
                                     &nbsp
                                     &nbsp
                                          <telerik:RadComboBox RenderMode="Lightweight" ID="cmb_BCC_Email" EnableCheckAllItemsCheckBox="true" CheckBoxes="true" Filter="StartsWith" AutoPostBack="false" DataValueField="UserEmail" DataTextField="UserEmail" DataSourceID="SqlDataSource1" runat="server"
                                              EmptyMessage="Select CC Recipients" Width="250px">
                                          </telerik:RadComboBox>
                        
                        <asp:Label ID="lblcmb_BCC_Email" runat="server" Text="*" ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                            ProviderName="System.Data.SqlClient" SelectCommand="SELECT  UserEmail  FROM [tbl_UserMaster]"></asp:SqlDataSource>


                    </td>
                </tr>--%>

                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>


                            <tr>
                                <td colspan="2" align="center">
                                    <%-- <telerik:RadButton ID="btn_SendMail" runat="server" Text="Send Mail"  OnClientClick="javascript:ShowOverlay();" CausesValidation="false"  OnClick="btn_SendMail_Click" />--%>

                                    <asp:Button ID="btn_SendMail" runat="server" CssClass="btn btn-info" OnClientClick="javascript:ShowOverlay();" OnClick="btn_SendMail_Click"
                                        Text="Send Mail" />
                                    <asp:Button ID="btn_Close" runat="server" CssClass="btn btn-info" OnClientClick="javascript:ShowOverlay();" OnClick="btn_Close_Click"
                                        Text="Close" />
                                </td>
                            </tr>

                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;
                                </td>
                            </tr>

                        </table>
                    </ContentTemplate>
                </telerik:RadWindow>
                <br />
                <telerik:RadGrid RenderMode="Lightweight" AutoGenerateColumns="false" MasterTableView-Visible="true" ID="rgvSchedule" runat="server" AllowPaging="false"
                    AllowMultiRowSelection="true" ClientSettings-Scrolling-AllowScroll="false"
                    ClientSettings-Resizing-AllowResizeToFit="true" AllowFilteringByColumn="true" OnNeedDataSource="rgvSchedule_NeedDataSource" HeaderStyle-HorizontalAlign="Center" OnDeleteCommand="rgvSchedule_DeleteCommand">
                    <ClientSettings>
                        <Scrolling UseStaticHeaders="true" />
                    </ClientSettings>
                    <GroupingSettings CaseSensitive="false"></GroupingSettings>
                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="RFQ_ID">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="RFQ ID" HeaderButtonType="TextButton" DataField="RFQ_ID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Customer Name" HeaderButtonType="TextButton" DataField="Customer_Name"></telerik:GridBoundColumn>

                            <telerik:GridDateTimeColumn HeaderText="Generated Date" EnableTimeIndependentFiltering="true" DataFormatString="{0:dd-MMM-yyyy}" HeaderButtonType="TextButton" DataField="Generated_Datetime"></telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn HeaderText="Committed Date" EnableTimeIndependentFiltering="true" DataFormatString="{0:dd-MMM-yyyy}" HeaderButtonType="TextButton" DataField="CommittedDate"></telerik:GridDateTimeColumn>

                            <telerik:GridBoundColumn HeaderText="Project" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Product_Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Sub-Project" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Sub_Product_Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Status" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataField="Status"></telerik:GridBoundColumn>
                            <%--<telerik:GridBoundColumn HeaderText="Transaction By" HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" DataField="STAGE3_Owner"></telerik:GridBoundColumn>--%>
                            <telerik:GridBoundColumn SortExpression="Remark" HeaderText="Remark" HeaderButtonType="TextButton" HeaderStyle-HorizontalAlign="Center"
                                DataField="Remark">
                            </telerik:GridBoundColumn>

                            <telerik:GridButtonColumn CommandName="Delete" Visible="false" Text="Delete" ConfirmText=" Are you sure delete this record?" UniqueName="DeleteColumn" />
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
        </section>
    </section>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
</asp:Content>
