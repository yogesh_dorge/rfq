﻿<%@ page title="" language="C#" masterpagefile="~/Site.master" autoeventwireup="true" inherits="Pages_RoleToProject, App_Web_tcx0gt55" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
        
    <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <center>
<div id="FormDiv" class="divForm" style="padding: 1px; border: thin solid #000000; position: relative;width: 60%;" align="center">
    <table width="100%">
        <tr>
            <td>
                &nbsp;</td>
            <td style="text-align: right">
                &nbsp;</td>
            <td style="text-align: right">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="center" colspan="2">
                    <asp:Label ID="lblUserHeadning" runat="server" CssClass="WebHR_Heading1" 
                        Text="ASSIGN ROLES"></asp:Label>
                </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" style="text-align: left">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
  
    
   <telerik:RadGrid ID="RadGrid1" runat="server"  AutoGenerateColumns="False" CellSpacing="0" GridLines="None" 
    onneeddatasource="RadGrid1_NeedDataSource" AllowMultiRowSelection="True" 
                    onprerender="RadGrid1_PreRender" onitemdatabound="RadGrid1_ItemDataBound" 
                    onselectedindexchanged="RadGrid1_SelectedIndexChanged" Skin="Sitefinity">
                <ClientSettings>
                <Selecting CellSelectionMode="SingleCell" UseClientSelectColumnOnly="True" />
<Selecting CellSelectionMode="None" AllowRowSelect="True"></Selecting>
            </ClientSettings>
          <GroupingSettings CaseSensitive="false"></GroupingSettings>
            <MasterTableView DataKeyNames="RoleCode">
                <CommandItemSettings ExportToPdfText="Export to PDF" />
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridClientSelectColumn FilterControlAltText="Filter SelectRole column" 
                        UniqueName="SelectRole">
                    </telerik:GridClientSelectColumn>
                    <telerik:GridBoundColumn DataField="ProjId" 
                        FilterControlAltText="Filter ProjId column" UniqueName="ProjId" Visible="False">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="RoleCode" EmptyDataText="NA" 
                        FilterControlAltText="Filter PlantOrMine column" HeaderText="Role Code" 
                        ReadOnly="True" UniqueName="RoleCode">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="RoleDesc" EmptyDataText="NA" 
                        FilterControlAltText="Filter Shift1 column" HeaderText="Role Description" 
                        UniqueName="RoleDescription">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </telerik:GridBoundColumn>
                  
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
</telerik:RadGrid>

            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
  
    
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="center">
  
    
                <asp:Button ID="btnNext" runat="server" Height="27px" onclick="btnNext_Click" 
                    Text="NEXT" Width="114px" CssClass="Buttons_Blue" />

            &nbsp;&nbsp;&nbsp;
            
            </td>
            <td align="left">
  
    
                <asp:Button ID="btnSkip" runat="server" Height="27px" onclick="btnSkip_Click" 
                    Text="SKIP &gt;&gt;" Width="114px" CssClass="Buttons_Blue" Visible="false" />

            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
    
    <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
    </telerik:RadWindowManager>
    
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
  
    
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

    </div></center>
</asp:Content>

