﻿<%@ page language="C#" autoeventwireup="true" inherits="CAR_CompletedReport, App_Web_if0ja4bt" masterpagefile="~/Pages/Admin.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

      <%--<link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
   

    <script type="text/javascript">
        function pageLoad() {
            var grid = $find("<%= rgvRecords.ClientID %>");
            var columns = grid.get_masterTableView().get_columns();
            for (var i = 0; i < columns.length; i++) {
                columns[i].resizeToFit();
            }
        }
    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

     <section id="main-content">
        <section class="wrapper">
      <center>
        <div style="padding: 10px;">
            <asp:Label ID="Label1" runat="server" Text="CAR Completed Report" Font-Bold="true" Font-Size ="XX-Large"></asp:Label>
            <br />
        </div>

        <div id="headingOne" role="tab" class="panel-heading">

             
        <asp:Label ID="Label2" runat="server" Text="From " Font-Bold="true"></asp:Label>
        <%--<telerik:RadDatePicker runat="server" ID="RadDatePickerFrom" DateInput-DateFormat="dd/MMM/yyyy"  DateInput-DisplayDateFormat="dd/MMM/yyyy" DateInput-ReadOnly="true">
        </telerik:RadDatePicker>--%>
            <asp:TextBox ID="txtFromDate" runat="server" CssClass="WebHR_TextBox" Height="20px" ></asp:TextBox>
                        <cc1:CalendarExtender ID="txtFromDate_CalendarExtender" runat="server" TargetControlID="txtFromDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="CodeValidator0" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="txtFromDate"></asp:RequiredFieldValidator>
        &nbsp
        <asp:Label ID="Label3" runat="server" Text="To " Font-Bold="true"></asp:Label>
        <%--<telerik:RadDatePicker runat="server" ID="RadDatePickerTo" DateInput-DateFormat="dd/MMM/yyyy"  DateInput-DisplayDateFormat="dd/MMM/yyyy" DateInput-ReadOnly="true">
        </telerik:RadDatePicker>--%>
            <asp:TextBox ID="txtToDate" runat="server" CssClass="WebHR_TextBox" Height="20px" ></asp:TextBox>
                        <cc1:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" TargetControlID="txtToDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="CodeValidator1" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="txtToDate"></asp:RequiredFieldValidator>

        &nbsp

             <%--<asp:Label ID="lblDept" runat="server" Text="Department " Font-Bold="true"></asp:Label>
              <telerik:RadComboBox RenderMode="Lightweight" ID="cmbDepartment"  Filter="StartsWith" AutoPostBack="false" runat="server" 
                                EmptyMessage="Select Department" DataSourceID="SqlDataSource3" DataTextField="DeptCode" DataValueField="DeptCode">
                            </telerik:RadComboBox>
                         <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                ProviderName="System.Data.SqlClient" SelectCommand="SELECT [DeptCode] FROM [tblDepartmentMaster]"></asp:SqlDataSource>
                          <asp:RequiredFieldValidator ID="rfvcmbDepartment" runat="server" ErrorMessage="*" ControlToValidate="cmbDepartment"
                            ForeColor="Red"></asp:RequiredFieldValidator>--%>
            &nbsp
        <%--<telerik:RadButton ID="btnFilterApply" runat="server" Text="Apply" OnClick="btnFilterApply_Click">
        </telerik:RadButton>--%>
            <asp:Button ID="btnFilterApply" runat="server" Text="Apply" OnClick="btnFilterApply_Click"></asp:Button>
    </div>

        <div class="divForm" style="padding: 1px; border: thin solid #000000; position: relative;">
            <telerik:RadScriptManager runat="server" ID="scriptManager1">
            </telerik:RadScriptManager>
    
             <telerik:RadGrid RenderMode="Lightweight" AutoGenerateColumns="false"  ID="rgvRecords" 
             AllowPaging="True"
                 PageSize="<%$appSettings:ReportPageSize%>" AllowFilteringByColumn="true" FilterItemStyle-HorizontalAlign="Left" GroupingSettings-CaseSensitive="false" Skin="WebBlue" CellSpacing="0" GridLines="None"  OnDetailTableDataBind="rgvRecords_DetailTableDataBind" OnNeedDataSource="rgvRecords_NeedDataSource"   OnItemCreated="rgvRecords_ItemCreated" runat="server" OnDataBinding="rgvRecords_DataBinding"  >
                  <ClientSettings>
       <Scrolling AllowScroll="True" UseStaticHeaders="true" />
                      <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />  
   </ClientSettings>

                <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" IgnorePaging="true" FileName="CAR Completed Report">
                </ExportSettings>
                
                <MasterTableView GroupLoadMode="Client" TableLayout="Auto" DataKeyNames="NC_No" CommandItemDisplay="Top" AlternatingItemStyle-HorizontalAlign="Left"  >
                    <CommandItemSettings ShowExportToExcelButton="true"  ShowExportToCsvButton="true" ShowAddNewRecordButton="false"  />
       
                    <Columns>
                       <%-- <telerik:GridButtonColumn Text="Select" ItemStyle-ForeColor="RoyalBlue"  ItemStyle-Font-Underline="true"  CommandName="Select_ID"  ButtonType="LinkButton">--%>

                                                                        
      

                <telerik:GridBoundColumn SortExpression="Audit_AuditNo"  HeaderText="Audit No" Visible="true" HeaderButtonType="TextButton"
                                    DataField="Audit_AuditNo" >
                                </telerik:GridBoundColumn>

                          <telerik:GridBoundColumn SortExpression="NC_No"  HeaderText="NC No" HeaderButtonType="TextButton"
                                    DataField="NC_No">
                                </telerik:GridBoundColumn>


                         <telerik:GridBoundColumn SortExpression="Department"  HeaderText="Department" HeaderButtonType="TextButton"
                                    DataField="Department">
                                </telerik:GridBoundColumn>

                         <telerik:GridBoundColumn SortExpression="Plant"  HeaderText="Plant" HeaderButtonType="TextButton"
                                    DataField="Plant">
                                </telerik:GridBoundColumn>


                                <telerik:GridBoundColumn SortExpression="Audit_Type"  HeaderText="Audit Type" HeaderButtonType="TextButton"
                                    DataField="Audit_Type">
                                </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Audit_Scope"  HeaderText="Audit Scope" HeaderButtonType="TextButton"
                                    DataField="Audit_Scope">
                                </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Frequency"  HeaderText="Frequency" HeaderButtonType="TextButton"
                                    DataField="Frequency">
                                </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="CARState"  HeaderText="CAR State" HeaderButtonType="TextButton"
                                    DataField="CARState">
                                </telerik:GridBoundColumn>


                                  <telerik:GridBoundColumn SortExpression="AuditeeName"  HeaderText="Auditee Name" HeaderButtonType="TextButton"
                                    DataField="AuditeeName">
                                </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="AuditeeStatus"  HeaderText="Auditee Status" HeaderButtonType="TextButton"
                                    DataField="AuditeeStatus">
                                </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="SubmissionDate"  HeaderText="Auditee Submission Date" HeaderButtonType="TextButton"
                                    DataField="SubmissionDate" DataFormatString="{0:dd-MMM-yyyy}">
                            </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn SortExpression="HODName"  HeaderText="HOD Name" HeaderButtonType="TextButton"
                                    DataField="HODName">
                                </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="HODStatus"  HeaderText="HOD Status" HeaderButtonType="TextButton"
                                    DataField="HODStatus">
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="HODRemark"  HeaderText="HOD Remark" HeaderButtonType="TextButton"
                                    DataField="HODRemark">
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="HODApprovalDate"  HeaderText="HOD Approval Date" HeaderButtonType="TextButton"
                                    DataField="HODApprovalDate" DataFormatString="{0:dd-MMM-yyyy}">
                            </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn SortExpression="AuditorName"  HeaderText="Auditor Name" HeaderButtonType="TextButton"
                                    DataField="AuditorName">
                                </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="AuditorStatus"  HeaderText="Auditor Status" HeaderButtonType="TextButton"
                                    DataField="AuditorStatus">
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="AuditorRemark"  HeaderText="Auditor Remark" HeaderButtonType="TextButton"
                                    DataField="AuditorRemark">
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="AuditorApprovalDate"  HeaderText="Auditor Approval Date" HeaderButtonType="TextButton"
                                    DataField="AuditorApprovalDate" DataFormatString="{0:dd-MMM-yyyy}">
                            </telerik:GridBoundColumn>
                        
                    <telerik:GridBoundColumn SortExpression="MRName"  HeaderText="MR Name" HeaderButtonType="TextButton"
                                    DataField="MRName">
                                </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="MRStatus"  HeaderText="MR Status" HeaderButtonType="TextButton"
                                    DataField="MRStatus">
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="MRRemark"  HeaderText="MR Remark" HeaderButtonType="TextButton"
                                    DataField="MRRemark">
                            </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="MRApprovalDate"  HeaderText="MR Approval Date" HeaderButtonType="TextButton"
                                    DataField="MRApprovalDate" DataFormatString="{0:dd-MMM-yyyy}">
                            </telerik:GridBoundColumn>



                              </Columns>
                </MasterTableView>
                  <ClientSettings AllowGroupExpandCollapse="True" ReorderColumnsOnClient="True" AllowDragToGroup="True"
                    AllowColumnsReorder="True">
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="true" />
            </telerik:RadGrid>
             <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
                        </telerik:RadWindowManager>
        </div>
    </center>
            </section>
         </section>

 </asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
 </asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
 </asp:Content>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
  
</asp:Content>--%>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
  
</asp:Content>--%>
