﻿<%--<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MasterDept.aspx.cs" Inherits="Pages_MasterDept" %>--%>
<%@ page language="C#" autoeventwireup="true" masterpagefile="~/Site.master" inherits="Pages_MasterHolidayAdd, App_Web_tcx0gt55" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit"
         TagPrefix="ajaxToolkit" %>

<asp:content id="Content1" contentplaceholderid="HeadContent" runat="Server">
    <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            height: 20px;
        }
    </style>
</asp:content>
<asp:content id="Content2" contentplaceholderid="MainContent" runat="Server">
     <center>
 <div id="FormDiv" class ="divForm" style="padding: 1px; border: thin solid #000000; position: relative; width: 300px;" align="center">
        

   
        <table style="width: 99%;">
             <tr>
                <td align="center" colspan="4">
                    
                    <asp:Label ID="lblUserHeadning" runat="server" CssClass="WebHR_Heading1" 
                        Text="ADD HOLIDAY"></asp:Label>
                 </td>
            </tr>
             <tr>
                <td colspan="4">
                    
                    &nbsp;</td>
            </tr>
            
            <tr style="visibility:hidden">
                <td align="right">
                   Holiday ID
                </td>
                <td>
                    &nbsp;</td>
                <td align="left">
                    <asp:TextBox ID="txtHolidayID" runat="server"  
                        CssClass="WebHR_TextBox" Visible="false" ></asp:TextBox>
                </td>
                <td class= "style2" >
                     
                </td>
            </tr>
            
             <tr>
                <td align="right">
                    
                    Choose Date</td>
                <td>
                    
                    &nbsp;</td>
                <td align="left">
    <asp:TextBox ID="txtHolidayDate" runat="server" CssClass="WebHR_TextBox" Height="16px" ></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="txtHolidayDate_CalendarExtender" runat="server" TargetControlID="txtHolidayDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </ajaxToolkit:CalendarExtender>
                        <asp:RequiredFieldValidator ID="CodeValidator0" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="txtHolidayDate"></asp:RequiredFieldValidator>

                </td>
                <td class="style2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                   Holiday Details
                </td>
                <td>
                    &nbsp;</td>
                <td align="left">
                    <asp:TextBox ID="txtHolidayDetails" runat="server"  
                        CssClass="WebHR_TextBox" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ForeColor="Red"
                        ControlToValidate="txtHolidayDetails">
                    </asp:RequiredFieldValidator>
                </td>
                <td class= "style2" >
                     
                </td>
            </tr>
            <tr>
                <td align="right">
                    &nbsp;
                    </td>
                <td align="right">
                    &nbsp;</td>
                <td align="left">
                    &nbsp;
                    <asp:Button ID="btnSave" runat="server" CssClass="Buttons_Blue" OnClick="btnSave_Click1" Text="SAVE" 
                        Width="74px" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td class="style1">
                    &nbsp;</td>
                <td colspan="2" align="right">
                    
                    <asp:LinkButton ID="Back" runat="server" CausesValidation="False" 
                        onclick="Back_Click">Back</asp:LinkButton>
                 </td>
            </tr>
        </table>
    </div>
    
     <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
    </telerik:RadWindowManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
     
    </center>
   </asp:content>



