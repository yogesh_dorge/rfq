﻿<%@ page language="C#" autoeventwireup="true" inherits="Pages_FormTestEmail, App_Web_p2aj4fm0" %>


<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit"
         TagPrefix="ajaxToolkit" %>
<link href="../Styles/Site.css" rel="stylesheet" type="text/css" />

<html>
<head></head> 
<title></title>
<body>
<form id="Form1"  runat="server">
<center>
    
<div id="FormDiv" class ="divForm" style="padding: 1px; border: thin solid #000000; position: relative; width: 300px;" align="center">
        
      <table style="width: 99%;">
             <tr>
                <td align="center" colspan="4">
                    
                    <asp:Label ID="Label1" runat="server" CssClass="WebHR_Heading1" 
                        Text="TEST EMAIL"></asp:Label>
   
                 </td>
            </tr>
             <tr>
                <td colspan="4">
                    
                    &nbsp;</td>
            </tr>

          <tr>
                <td align="right">
                    
                    Email To</td>
                <td>
                    
                    &nbsp;</td>
                <td align="left">
                    <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ForeColor="Red"
                        ControlToValidate="txtTo">
                    </asp:RequiredFieldValidator>
      <%--&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtCustName" runat="server" ErrorMessage="Special symbols not allowed" ValidationExpression="^[a-zA-Z0-9_ ]+$"  ForeColor="Red"></asp:RegularExpressionValidator> &nbsp;--%>
                              </td>
                <td class="style2">
                    &nbsp;</td>
            </tr>

          <tr>
                <td align="right">
                    
                    Email CC</td>
                <td>
                    
                    &nbsp;</td>
                <td align="left">
                    <asp:TextBox ID="txtCC" runat="server"></asp:TextBox>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ForeColor="Red"
                        ControlToValidate="txtCC">
                    </asp:RequiredFieldValidator>
      <%--&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtCustName" runat="server" ErrorMessage="Special symbols not allowed" ValidationExpression="^[a-zA-Z0-9_ ]+$"  ForeColor="Red"></asp:RegularExpressionValidator> &nbsp;--%>
                              </td>
                <td class="style2">
                    &nbsp;</td>
            </tr>

             <tr>
                <td align="right">
                    
                    Email Subject</td>
                <td>
                    
                    &nbsp;</td>
                <td align="left">
                    <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ForeColor="Red"
                        ControlToValidate="txtSubject">
                    </asp:RequiredFieldValidator>
      <%--&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtCustName" runat="server" ErrorMessage="Special symbols not allowed" ValidationExpression="^[a-zA-Z0-9_ ]+$"  ForeColor="Red"></asp:RegularExpressionValidator> &nbsp;--%>
                              </td>
                <td class="style2">
                    &nbsp;</td>
            </tr>


             <tr>
                <td align="right">
                    
                    Email Body</td>
                <td>
                    
                    &nbsp;</td>
                <td align="left">
                    <asp:TextBox ID="txtBody" runat="server"></asp:TextBox>
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ForeColor="Red"
                        ControlToValidate="txtBody">
                    </asp:RequiredFieldValidator>
      <%--&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtCustCode" runat="server" ErrorMessage="Special symbols not allowed" ValidationExpression="^[a-zA-Z0-9]+$"  ForeColor="Red"></asp:RegularExpressionValidator> &nbsp;--%>
                              </td>
                <td class="style2">
                    &nbsp;</td>
            </tr>




            <tr>
                <td align="right">
                    &nbsp;
                    </td>
                <td align="right">
                    &nbsp;</td>
                <td align="left">
                    &nbsp;
                    <asp:Button ID="btnTest" runat="server" CssClass="Buttons_Blue" OnClick="btnTest_Click" Text="TEST EMAIL" 
                         />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td> &nbsp; &nbsp; &nbsp; &nbsp;
                    </td>
                <td class="style1">
                    <asp:Label ID="lblStatus" runat="server" Text=" "></asp:Label>
                    </td>
                <td align="center">
                    <%--<asp:LinkButton ID="Back" runat="server" CausesValidation="False" 
                        onclick="Back_Click">Back</asp:LinkButton>--%>
                    
                 </td>
                <td>
                    </td>
            </tr>
           <tr>
                    <td colspan="3" align="right">
                        <asp:LinkButton ID="Back" runat="server" CausesValidation="False" OnClick="Back_Click">Back</asp:LinkButton>
                    </td>
                </tr>
        </table>

    </div>
     <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
    </telerik:RadWindowManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    
 </center> 
 </form>
    </body>
</html> 


