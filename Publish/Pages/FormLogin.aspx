﻿<%@ page language="C#" autoeventwireup="true" inherits="FormLogin, App_Web_p2aj4fm0" maintainscrollpositiononpostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="Dashboard" />
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina" />

    <title>LOGIN</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <%--<script>
        function InternetCheck() {
            if (navigator.onLine) {
                //alert('online');
            }
            else {
                //window.stop();
                alert('Please Check your Internet Connection');

            }
        }
    </script>--%>
</head>
<body>
    <%--<form id="form1" runat="server">--%>
    <%--<telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
    </telerik:RadWindowManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>--%>

    <div id="login-page">
        <div class="container">
            <h1 class="form-login-heading" style="color: rosybrown">RFQ Monitoring Process</h1>
            <form class="form-login" id="form1" runat="server">
                <div class="login-wrap" align="center">
                    <img src="../Images/index.jpg" />
                </div>
                <h2 class="form-login-heading" style="color: lightsteelblue">sign in now</h2>
                <br />
                <div>
                    <%--<div style="text-align: center; font-family: 'Bookman Old Style';">--%>
                    <%--  <asp:TextBox ID="txtUsername" runat="server" OnTextChanged="txtUsername_TextChanged" ToolTip="User Name" placeholder="User Name" AutoPostBack="true" Width="97%" Height="35px"></asp:TextBox>--%>

                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <fieldset>

                                <asp:TextBox ID="txtUsername" runat="server" OnTextChanged="txtUsername_TextChanged" ToolTip="User Name" placeholder="User Name" AutoPostBack="true" Width="97%" Height="35px"></asp:TextBox>


                                <asp:Label ID="s0" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                <br />
                                <div style="color: red;">
                                    @sanjeevgroup.com
                    <div>
                        <asp:RequiredFieldValidator ID="rfvUsrName" runat="server" ControlToValidate="txtUserName"
                            ErrorMessage="(*) User Email ID is required" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                                </div>
                                <%--</div>--%>
                                <br />
                                <%--  <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password" ToolTip="Password" placeholder="Password"></asp:TextBox>--%>
                                <asp:TextBox ID="txtPassword" runat="server" Width="97%" Height="35px" TextMode="Password" ToolTip="Password" placeholder="Password"></asp:TextBox>

                                <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                <div>
                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                        Display="Dynamic" ErrorMessage="(*) Password is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>

                                <br />
                                <%-- <asp:DropDownList runat="server" ID="cmbRolecode" Filter="StartsWith" DataSourceID="SqlDataSource8" DataTextField="RoleCode" CssClass="form-control" TabIndex="3" AutoPostBack="false" Enabled="false" EmptyMessage="Select Role"></asp:DropDownList>--%>



                                <asp:TextBox ID="cmbRolecode" runat="server" Enabled="false" CssClass="form-control" ToolTip="Login" placeholder="Role"></asp:TextBox>




                                <%--  <asp:DropDownList runat="server" ID="cmbRolecode" Filter="StartsWith"  CssClass="form-control" TabIndex="3" AutoPostBack="false" Enabled="false" EmptyMessage="Select Role"></asp:DropDownList>--%>

                                <div>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cmbRolecode"
                                        Display="Dynamic" ErrorMessage="(*) Role is required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </fieldset>
                        </ContentTemplate>
                        <Triggers>
                           <%-- <asp:AsyncPostBackTrigger ControlID="cmbRolecode" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:ConnStringBEHRCCR %>"
                        ProviderName="System.Data.SqlClient" SelectCommand="SELECT [RoleCode] FROM [tblRoleMaster]"></asp:SqlDataSource>

                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>

                    <br />
                    <asp:Button ID="btn_Login" runat="server" ForeColor="WhiteSmoke" Text="SIGN IN" OnClick="btn_Login_Click" CssClass="btn btn-theme btn-block" />
                    <br />
                    <asp:Label ID="Label16" runat="server" Text="(Note : (*) fields are mandatory)" ForeColor="Red"></asp:Label>
                    <hr />
                    <div id="hide_login">
                        <asp:Label ID="lblLogin_Failed" runat="server" Text="Unauthorised credentials" class="alert alert-danger" Visible="false"></asp:Label>
                        <div class="login-social-link centered">
                            <p>
                                BUILD ID :
                                <asp:Label ID="lblBuildId" ForeColor="Red" runat="server" Text="." Visible="True"></asp:Label>
                            </p>

                        </div>
                    </div>

                    <!-- Modal -->
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Forgot Password ?</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Enter your e-mail address below to reset your password.</p>
                                    <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                    <button class="btn btn-theme" type="button">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal -->
                    <br>
                </div>

            </form>

        </div>
    </div>
    <div style="text-align: right; color: white; font-family: 'Bookman Old Style'">
        &copy;&nbsp;2017 - <b>SANJEEV</b> - Developed by <a target="_blank" href="http://www.vtelebyte.com"><b>Vtelebyte Software Pvt.Ltd Pune</b></a>
    </div>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("../Images/sanjeev_background.png", { speed: 300 });
    </script>

    <%--</form>--%>
</body>
</html>
