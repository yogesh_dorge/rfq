﻿<%@ page title="" language="C#" masterpagefile="~/Pages/Admin.master" autoeventwireup="true" inherits="Pages_MasterSub_ProductMaster, App_Web_ok35y50u" maintainscrollpositiononpostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script runat="server">
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="assets/Scripts/script.js"></script>
    <%--<link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <style type="text/css">
        .style1
        {
            height: 20px;
        }

        table, th, td
        {
            border: 1px double black;
        }

            td:hover
            {
                background-color: #f5f5f5;
            }



        td
        {
            font-size: 14px !important;
        }

        .W3_Title
        {
            color: #666;
            font-weight: bold !important;
            font-size: 20px !important;
            text-decoration: none;
        }

        .W3_Header
        {
            color: #666;
            font-weight: bold !important;
            font-size: 30px !important;
            text-decoration: none;
            text-align: center !important;
        }

        .W3_Warning
        {
            color: #C7310C;
            font-weight: bold !important;
            font-size: 20px !important;
            text-decoration: none;
            text-align: center !important;
        }

        /*PRINT DIV*/
        @media print
        {
            .myDivToPrint
            {
                background-color: white;
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                margin: 0;
                padding: 15px;
                font-size: 14px;
                line-height: 18px;
            }
        }

        #scrolly
        {
            width: 1000px;
            height: 190px;
            overflow: auto;
            overflow-y: hidden;
            margin: 0 auto;
            white-space: nowrap;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function printDiv() {
            var PrintDiv = document.getElementById('<%=FormDiv.ClientID %>');
            PrintDiv.border = 0;
            var PrintDivWindow = window.open('', 'PrintDivData', 'left=100,top=100,width=1000,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
            PrintDivWindow.document.write(PrintDiv.outerHTML);
            PrintDivWindow.document.close();
            PrintDivWindow.focus();
            PrintDivWindow.print();
            PrintDivWindow.close();


        }

        //Enable Disable Textbox for EDIT D2Remark
        function EnableDisableTextBoxD2Remark(chkPassport) {
            if (chkPassport.checked == true) {
                document.getElementById("MainContent_txtD2Remark").disabled = false;
            }
            else {
                document.getElementById("MainContent_txtD2Remark").disabled = true;
            }
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">
    <section id="main-content">
        <section class="wrapper">
            <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
            </telerik:RadStyleSheetManager>
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            </telerik:RadAjaxManager>
            <center>
        <div style="padding:10px; text-align:end;"><input type="button" value="Print" onclick="javascript: printDiv()" /></div>

        <div style="text-align:left; font-size:large"><a href="CARListAuditSpecific.aspx" runat="server" id="BackID"><b><u></u></b></a></div>
 <div id="FormDiv" runat="server" class ="divForm myDivToPrint" style="padding: 1px; border: thin solid #000000; position: relative; " align="center"  >
        
      <table id="Table1" width="100%" runat="server" style="overflow-x:scroll;  table-layout:fixed;"  >
         <tr>
             <td colspan="10" align="center" class="W3_Header">Quotation Details</td>
             
         </tr>
         <tr >
            
             <td colspan="10">
                  
             </td>
         </tr>
              
            <tr>
             <td colspan="10" class="W3_Warning">Header Part :
               <%--  <asp:Label ID="lblResolutionDate" runat="server"></asp:Label>--%>
             </td>
         </tr>
          <tr>
             
             <td align="justify" style="text-align:right;" colspan="5">Customer Name:
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                 <asp:Label ID="lblCustomerName" Text="Sanjeev Auto Pvt.Ltd" CssClass="WebHR_TextBox" runat="server"></asp:Label>                 
             </td>                        
         </tr>
          <tr>
                 <td align="justify" style="text-align:right;" colspan="5">RFQ Type:
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                 <asp:Label ID="lbl_RFQ_Type" Text="Domestic" CssClass="WebHR_TextBox" runat="server"></asp:Label>                 
             </td>      
          </tr>
           <tr>
                 <td align="justify" style="text-align:right;" colspan="5">INCO Term:
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 

                    <telerik:RadComboBox RenderMode="Lightweight" Font-Size="Small" ID="cmbInco_term" CssClass="WebHR_TextBox" Height="90%"  Width="100%" Filter="StartsWith" AutoPostBack="true" runat="server" EmptyMessage="Select INCO Term">
                    </telerik:RadComboBox>
                 
                    <asp:RequiredFieldValidator ID="rcfcmbInco_term" SetFocusOnError="true" EnableClientScript="false" runat="server" ErrorMessage="*" ControlToValidate="cmbInco_term"
                        ForeColor="Red"></asp:RequiredFieldValidator>                                   
                           
             </td>      
          </tr>
            <tr>
                 <td align="justify" style="text-align:right;" colspan="5">Part No:
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
           
                      <asp:TextBox ID="txtPart_no" runat="server" CssClass="WebHR_TextBox" Text="PartNO" Height="100%"  Width="100%" Enabled="true"></asp:TextBox>
                       
             </td>      
          </tr>
           <tr>
                 <td align="justify" style="text-align:right;" colspan="5">Revision/Version:
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
           
                      <asp:TextBox ID="lbl_Revision_Version" runat="server" Text="Revision" Height="100%"  Width="100%" Enabled="true"></asp:TextBox>
                       
             </td>      
          </tr>
           <tr>
                 <td align="justify" style="text-align:right;" colspan="5">Description:
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
           
                      <asp:TextBox ID="lbl_Description" runat="server" TextMode="MultiLine" Text="Description" Height="100%"  Width="100%" Enabled="true"></asp:TextBox>
                       
             </td>      
          </tr>
            <tr>
                 <td align="justify" style="text-align:right;" colspan="5">EAU:
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
           
                      <asp:TextBox ID="txtEAU" runat="server" CssClass="WebHR_TextBox" Height="100%"  Text="EAU"  Width="100%" Enabled="true"></asp:TextBox>
                       
             </td>      
          </tr>
           <tr>
                 <td align="justify" style="text-align:right;" colspan="5">Raw Material - Specified:
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
           
                      <asp:TextBox ID="txtRawMaterial_Specified" runat="server" CssClass="WebHR_TextBox" Text="EAU" Height="100%"  Width="100%" Enabled="true"></asp:TextBox>
                       
             </td>      
          </tr>

            <tr>
                 <td align="justify" style="text-align:right;" colspan="5">Raw Material - Considered:
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                 <asp:Label ID="lbl_RawMaterial_Considered" Text="" runat="server"></asp:Label>                 
             </td>      
          </tr>
           <tr>
                 <td align="justify" style="text-align:right;" colspan="5">RATE / KG:
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                 <asp:Label ID="lbl_RATE_KG" Text="" runat="server"></asp:Label>                 
             </td>      
          </tr>
           <tr>
                 <td align="justify" style="text-align:right;" colspan="5">Input Weight (KGS):
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                 <asp:TextBox ID="txt_InputWeight" runat="server" CssClass="WebHR_TextBox" Height="100%"  Width="100%" Enabled="true"></asp:TextBox>                
             </td>      
          </tr>
           <tr>
                 <td align="justify" style="text-align:right;" colspan="5">Forg Weight:
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                 <asp:TextBox ID="txtforg_weight" runat="server" CssClass="WebHR_TextBox" Height="100%"  Width="100%" Enabled="true"></asp:TextBox>                
             </td>      
          </tr>
               <tr>
                 <td align="justify" style="text-align:right;" colspan="5">FINISH Weight (KGS):
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                 <asp:TextBox ID="txt_Finish_weight" runat="server" CssClass="WebHR_TextBox" Height="100%"  Width="100%" Enabled="true"></asp:TextBox>                
             </td>      
          </tr>
              <tr>
                 <td align="justify" style="text-align:right;" colspan="5">RAW MATERIAL COST (RS.):
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                 <asp:TextBox ID="txt" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>                
             </td>      
          </tr>

          <tr>
               <%--<td class="W3_Title" rowspan="2">D4</td>--%>
             <td colspan="10" class="W3_Warning">Processing Part :
              
             </td>
         </tr>
          <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style'; color:blue"  colspan="2"> PROCESSING (Process cost giving details of process reqd)
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';color:blue"  colspan="2"> MHR
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';color:blue"  colspan="2">
                Efficiency 
                       
                        </td>
              <td align="justify" style="text-align:center; font-family:'Bookman Old Style';color:blue" colspan="2">
               Cycle Time in minutes
                        
                        </td>

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';color:blue" colspan="2">
                  Processing Cost
                       
                        </td>
          

          </tr>
             <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> Forging
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> 
                  <asp:TextBox ID="txt_MHR1" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="2">
             
                        <asp:TextBox ID="txt_effeciency1" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        </td>
              <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
              <asp:TextBox ID="txt_cycletime1" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        
                        </td>

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_processingcost1" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>

            <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> Forging HT - Hardening & Tempering
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> 
                  <asp:TextBox ID="txt_MHR2" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="2">
             
                        <asp:TextBox ID="txt_effeciency2" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        </td>
              <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
              <asp:TextBox ID="txt_cycletime2" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        
                        </td>

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_processingcost2" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>

           <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> Rough Machining
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> 
                  <asp:TextBox ID="txt_MHR3" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="2">
             
                        <asp:TextBox ID="txt_effeciency3" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        </td>
              <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
              <asp:TextBox ID="txt_cycletime3" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        
                        </td>

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_processingcost3" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
             <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> CNC 1st
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> 
                  <asp:TextBox ID="txt_MHR4" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="2">
             
                        <asp:TextBox ID="txt_effeciency4" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        </td>
              <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
              <asp:TextBox ID="txt_cycletime4" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        
                        </td>

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_processingcost4" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
             <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> I
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> 
                  <asp:TextBox ID="txt_MHR5" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="2">
             
                        <asp:TextBox ID="txt_effeciency5" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        </td>
              <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
              <asp:TextBox ID="txt_cycletime5" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        
                        </td>

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_processingcost5" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
           <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> VMC 1st
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> 
                  <asp:TextBox ID="txt_MHR6" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="2">
             
                        <asp:TextBox ID="txt_effeciency6" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        </td>
              <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
              <asp:TextBox ID="txt_cycletime6" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        
                        </td>

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_processingcost6" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
            <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> Part Marking
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> 
                  <asp:TextBox ID="txt_MHR7" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="2">
             
                        <asp:TextBox ID="txt_effeciency7" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        </td>
              <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
              <asp:TextBox ID="txt_cycletime7" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
                        
                        </td>

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_processingcost7" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
          <tr >
            
             <td colspan="10">
                  
             </td>
         </tr>
           <tr>
                 <td align="justify" style="text-align:center; color:blue" colspan="5">Inspection
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                     <asp:TextBox ID="txt_Inspection" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>                    
             </td>      
          </tr>
           <tr>
                 <td align="justify" style="text-align:center; color:blue;" colspan="5">Wash & clean
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                     <asp:TextBox ID="txt_WashAndclean" runat="server" Font-Bold="true" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>                    
             </td>      
          </tr>
          <tr>
                 <td align="justify" style="text-align:center; color:blue" colspan="5">Tool Cost
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                     <asp:TextBox ID="txt_toolCost" runat="server" Font-Bold="true"  Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>                    
             </td>      
          </tr>

          <tr>
                 <td align="justify" style="text-align:center; color:blue" colspan="5">Total Processing Cost
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                     <asp:Label ID="Label1"   Font-Bold="true" runat="server"></asp:Label>                   
             </td>      
          </tr>
          <tr>
                 <td align="justify" style="text-align:center; color:blue" colspan="5">R/M + Processing
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                     <asp:Label ID="lbl_Rm_Processing"   Font-Bold="true" runat="server"></asp:Label>                   
             </td>      
          </tr>

          <tr >
            
             <td colspan="10">
                  
             </td>
         </tr>

          <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> ICC
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> 
                  <asp:TextBox ID="txt_Icc_per" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="4">
             
                        <asp:Label ID="lblOnrm1" Text="On RM" runat="server"></asp:Label>        
                        </td>
           

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_ICC_Procc_Cost" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>

            <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> OVER HEADS
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> 
                  <asp:TextBox ID="txt_overhead_per" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="4">
             
                        <asp:Label ID="lbl_overhead_PerName" Text="On R/M +Processing" runat="server"></asp:Label>        
                        </td>
           

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_OVERHEADS_proc_cost" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>

          <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> REJECTON
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> 
                  <asp:TextBox ID="txt_REJECTON_Per" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="4">
             
                        <asp:Label ID="lbl_REJECTON_perName" Text="On R/M +Processing" runat="server"></asp:Label>        
                        </td>
           

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_REJECTON_proc_cost" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>

          <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">PROFIT MARGINE
                 
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2"> 
                  <asp:TextBox ID="txt_Project_Margin" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>        
             </td>
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="4">
             
                        <asp:Label ID="lbl_PROFITMARGINE_perName" Text="On R/M +Processing" runat="server"></asp:Label>        
                        </td>
           

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_PROFITMARGINE_proc_cost" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
            <tr>
                 <td align="justify" style="text-align:center; color:blue" colspan="5">Sub Total Ex work cost
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                     <asp:Label ID="lbl_subTotal_ex_workcodt"   Font-Bold="true" runat="server"></asp:Label>                   
             </td>      
          </tr>
           <tr>
               <%--<td class="W3_Title" rowspan="2">D4</td>--%>
             <td colspan="10" class="W3_Warning">Sub Total (Domestic / Export) :
              
             </td>
         </tr>
           <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="4">Export Sea Worthy Packing
                 
             </td>
           
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="4">
             
                   <asp:TextBox ID="txt_ExportSeaWorthyPacking_per" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>      
                       
                        </td>
           

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_ExportSeaWorthyPacking_cost" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
          <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="4">S G & A
                 
             </td>
           
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="4">
             
                   <asp:TextBox ID="txt_SGA_per" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>      
                       
                        </td>
           

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_SG_And_A_Cost" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
          <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="4">Returnable Packaging
                 
             </td>
           
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="4">
             
                   <asp:TextBox ID="txt_returnable_Packing_per" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>      
                       
                        </td>
           

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_returnable_packing_cost" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
           <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="4">Transport
                 
             </td>
           
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="4">
             
                   <asp:TextBox ID="txt_Transport_per" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>      
                       
                        </td>
           

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_Transport_cost" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
            <tr>
                 <td align="justify" style="text-align:center; color:blue" colspan="5">Total FCA Cost (In INR)
               
             </td>
             <td align="justify" style="text-align:center;" colspan="5"> 
                     <asp:Label ID="lbl_Total_FCACost"   Font-Bold="true" runat="server"></asp:Label>                   
             </td>      
          </tr>
             <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="3">Freight Insurance & Clearance
                 
             </td>
           
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="3">
             
                   <asp:TextBox ID="txt_FreightInsurance_Clearance_per1" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>      
                       
                        </td>
            <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="2">
             
                   <asp:TextBox ID="txt_FreightInsurance_Clearance_per2" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>      
                       
                        </td>

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_FreightInsurance_Clearance_Cost" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
             <tr>
                
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="3">Currency Conversion Factor
                 
             </td>
           
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="3">
             
                   <asp:TextBox ID="txt_CurrencyConversionFactor_per1" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>      
                       
                        </td>
            <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="2">
             
                   <asp:TextBox ID="txt_CurrencyConversionFactor_per2" runat="server" Height="100%" CssClass="WebHR_TextBox"  Width="100%" Enabled="true"></asp:TextBox>      
                       
                        </td>

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_CurrencyConversionFactor_cost" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>

            <tr>
                
             <td align="justify" style="text-align:center; color:blue" colspan="3">Total DAP Cost(€)
                 
             </td>
           
             <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="3">
             
               <asp:Label ID="lbl_total_DAp_per1" Text="" runat="server"></asp:Label>          
                       
                        </td>
            <td align="justify" style="text-align:center; font-family:'Bookman Old Style';"  colspan="2">
             
                    <asp:Label ID="lbl_total_DAp_per2" Text="" runat="server"></asp:Label>        
                       
                        </td>

               <td align="justify" style="text-align:center; font-family:'Bookman Old Style';" colspan="2">
            <asp:Label ID="lbl_total_DAp_cost" Text="" runat="server"></asp:Label>          
                       
                        </td>          

          </tr>
          </table>





      </div>
    
     <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
    </telerik:RadWindowManager>
        <telerik:RadSkinManager ID="RadSkinManager1" Runat="server" ShowChooser="false">
        </telerik:RadSkinManager>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
     
    </center>
        </section>
    </section>


</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
</asp:Content>

