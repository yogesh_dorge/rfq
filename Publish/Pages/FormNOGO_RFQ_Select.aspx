﻿<%@ page title="" language="C#" masterpagefile="~/Pages/Admin.master" autoeventwireup="true" inherits="Pages_FormNOGO_RFQ_Select, App_Web_if0ja4bt" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<link href="../Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>

</center>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder2">
    <br />
    <section id="main-content">
        <section class="wrapper">
            <div id="FormDiv1" class="divForm" style="padding: 1px; border: thin solid #000000; position: relative; top: 0px; right: -2px; left: 2px; height: 100%; width: 100%;"
                align="left">
                <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                </telerik:RadScriptManager>
                <telerik:RadWindowManager ID="rmw1" runat="server">
                </telerik:RadWindowManager>
                <br />

                <div style="align-items: center; text-align: center;">
                    <asp:Label ID="lblUserHeadning" Font-Size="Large" Font-Bold="true" runat="server" CssClass="WebHR_Heading1"
                        Text="SELECT RFQ NUMBER"></asp:Label>
                </div>

                <br />

                <div style="text-align: left; font-size: large"><a href="Dashboard.aspx" runat="server" id="BackID"><b><u>Back</u></b></a></div>
                <telerik:RadGrid RenderMode="Lightweight" AutoGenerateColumns="false" ID="RadGrid2"
                    AllowFilteringByColumn="True" Skin="WebBlue" OnItemCommand="RadGrid2_ItemCommand" OnItemDataBound="RadGrid2_ItemDataBound" OnNeedDataSource="RadGrid2_NeedDataSource" runat="server" HeaderStyle-HorizontalAlign="Center">
                    <ClientSettings>
                        <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                    </ClientSettings>
                    <MasterTableView AutoGenerateColumns="false">
                        <Columns>
                            <%-- <telerik:GridButtonColumn Text="Select" ItemStyle-ForeColor="RoyalBlue"  ItemStyle-Font-Underline="true"  CommandName="Select_ID"  ButtonType="LinkButton">--%>


                                <telerik:GridHyperLinkColumn DataTextFormatString="{0}"
                                DataNavigateUrlFields="RFQ_ID" ItemStyle-ForeColor="RoyalBlue"  UniqueName="RFQ_ID"
                                HeaderText="RFQ Number" DataTextField="RFQ_ID" ItemStyle-Font-Underline="true" />

                                      

                           <%-- <telerik:GridBoundColumn HeaderText="RFQ ID" HeaderButtonType="TextButton"
                                DataField="RFQ_ID">
                            </telerik:GridBoundColumn>--%>
                            <telerik:GridBoundColumn  HeaderText="Rejected By" HeaderButtonType="TextButton"
                                DataField="STAGE3_Owner">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn  HeaderText="Status" HeaderButtonType="TextButton"
                                DataField="Stage4_Owner_GO_NoGo">
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn HeaderText="Reason" HeaderButtonType="TextButton"
                                DataField="Stage4_NotGo_Reason">
                            </telerik:GridBoundColumn>

                           

                            
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>





            </div>
        </section>
    </section>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder3">
</asp:Content>

