﻿<%@ page language="C#" autoeventwireup="true" inherits="FormActivityReport, App_Web_p2aj4fm0" masterpagefile="~/Site.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <center>
        <div style="padding: 10px;">
            <asp:Label ID="Label1" runat="server" Text="User Login Log Report" Font-Bold="true" Font-Size ="XX-Large"></asp:Label>
        </div>

        <div id="headingOne" role="tab" class="panel-heading">
        <asp:Label ID="Label2" runat="server" Text="From " Font-Bold="true"></asp:Label>
        <%--<telerik:RadDatePicker runat="server" ID="RadDatePickerFrom" DateInput-DateFormat="dd/MMM/yyyy"  DateInput-DisplayDateFormat="dd/MMM/yyyy" DateInput-ReadOnly="true">
        </telerik:RadDatePicker>--%>
            <asp:TextBox ID="txtFromDate" runat="server" CssClass="WebHR_TextBox" Height="16px" ></asp:TextBox>
                        <cc1:CalendarExtender ID="txtFromDate_CalendarExtender" runat="server" TargetControlID="txtFromDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="CodeValidator0" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="txtFromDate"></asp:RequiredFieldValidator>
        &nbsp
        <asp:Label ID="Label3" runat="server" Text="To " Font-Bold="true"></asp:Label>
        <%--<telerik:RadDatePicker runat="server" ID="RadDatePickerTo" DateInput-DateFormat="dd/MMM/yyyy"  DateInput-DisplayDateFormat="dd/MMM/yyyy" DateInput-ReadOnly="true">
        </telerik:RadDatePicker>--%>
            <asp:TextBox ID="txtToDate" runat="server" CssClass="WebHR_TextBox" Height="16px" ></asp:TextBox>
                        <cc1:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" TargetControlID="txtToDate"
                            DaysModeTitleFormat="dd/MMM/yyyy" Format="dd/MMM/yyyy" TodaysDateFormat="dd/MMM/yyyy">
                        </cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="CodeValidator1" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="txtToDate"></asp:RequiredFieldValidator>

        &nbsp
        <%--<telerik:RadButton ID="btnFilterApply" runat="server" Text="Apply" OnClick="btnFilterApply_Click">
        </telerik:RadButton>--%>
            <asp:Button ID="btnFilterApply" runat="server" Text="Apply" OnClick="btnFilterApply_Click"></asp:Button>
    </div>

        <div class="divForm" style="padding: 1px; border: thin solid #000000; position: relative;">
            <telerik:RadScriptManager runat="server" ID="scriptManager1">
            </telerik:RadScriptManager>
            <telerik:RadGrid runat="server" ID="rgvRecords" AutoGenerateColumns="False" AllowPaging="True"
                PageSize="<%$appSettings:ReportPageSize%>" AllowFilteringByColumn="true" FilterItemStyle-HorizontalAlign="Left" GroupingSettings-CaseSensitive="false" Skin="Sitefinity" CellSpacing="0" GridLines="None" 
                AllowMultiRowSelection="True" OnNeedDataSource="rgvRecords_NeedDataSource"
                OnDetailTableDataBind="rgvRecords_DetailTableDataBind" 
                ShowGroupPanel="True" OnItemCreated="rgvRecords_ItemCreated">

                <ClientSettings>
       <Scrolling AllowScroll="True" UseStaticHeaders="true" />
   </ClientSettings>
                <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" IgnorePaging="true" FileName="User Login Log Report">
                </ExportSettings>
                <MasterTableView GroupLoadMode="Client" TableLayout="Fixed" DataKeyNames="User_Name" CommandItemDisplay="Top">
                 
                    <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowAddNewRecordButton="false"/>
                      <%-- <DetailTables>
                        <telerik:GridTableView DataKeyNames="ProjectId" Name="ProjectId" Width="100%" GroupLoadMode="Server">
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="ProjectId" HeaderText="ProjectId" HeaderButtonType="TextButton"
                                    DataField="ProjectId" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="ProjectNo" HeaderText="ProjectNo" HeaderButtonType="TextButton"
                                    DataField="ProjectNo">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="ProjectStartDate" HeaderText="ProjectStartDate" HeaderButtonType="TextButton"
                                    DataField="ProjectStartDate" UniqueName="ProjectStartDate" DataFormatString="{0:D}">
                                </telerik:GridBoundColumn>
                                <telerik:GridHyperLinkColumn FooterText="HyperLinkColumn footer" DataTextFormatString="{0}"
                                    DataNavigateUrlFields="Path" DataNavigateUrlFormatString="{0}" UniqueName="CompanyName"
                                    HeaderText="View File" DataTextField="Path" Text="Open File" ImageUrl="~/Images/Openfile2.jpg">
                                    <ItemStyle ForeColor="#0066FF" HorizontalAlign="Left" VerticalAlign="Middle" Width="20%" />
                                </telerik:GridHyperLinkColumn>
                            </Columns>
                        </telerik:GridTableView>
                    </DetailTables>--%>
                    <Columns>
                        <telerik:GridBoundColumn DataField="User_ID" EmptyDataText="NA" HeaderText="User ID"
                            ReadOnly="True" UniqueName="User_ID" Visible="true">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="User_Name" EmptyDataText="NA" HeaderText="User Name"
                            ReadOnly="True" UniqueName="User_Name">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="UserDept" EmptyDataText="NA" HeaderText="Department Name"                          ReadOnly="True" UniqueName="UserDept">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Datetime" EmptyDataText="NA" HeaderText="Login Date "
                            ReadOnly="True" UniqueName="Datetime">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </telerik:GridBoundColumn>
                                                
                                                                                            
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowGroupExpandCollapse="True" ReorderColumnsOnClient="True" AllowDragToGroup="True"
                    AllowColumnsReorder="True">
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="true" />
            </telerik:RadGrid>
             <telerik:RadWindowManager runat="server" ID="rwm1" Modal="true">
                        </telerik:RadWindowManager>
        </div>
    </center>
</asp:Content>
