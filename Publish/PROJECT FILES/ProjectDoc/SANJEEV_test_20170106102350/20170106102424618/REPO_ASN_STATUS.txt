
GO
/****** Object:  UserDefinedFunction [dbo].[VconnectNext_Get_REPO_ASN_Status]    Script Date: 07/31/2016 13:15:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[VconnectNext_Get_REPO_ASN_Status] (@Vendor varchar(50), @Plant varchar(50), @FromDate DateTime, @ToDate DateTime)
RETURNS TABLE
AS
RETURN
(
select  [tbl_REPO_Log_Master].ASN_Number as [ASN Number],[tbl_REPO_Log_Master].Delivery_note_number as [Invoice Number],dbo.VT_GetDateOnly([tbl_REPO_Log_Master].Doc_Date) as [Invoice Date],[tbl_REPO_Log_Details].[Sr_number] as [Line Item],[tbl_REPO_Log_Details].Material_Code as [Material Code], SUBSTRING([tbl_REPO_Log_Details].Material_Code, PATINDEX('%[^0 ]%', [tbl_REPO_Log_Details].Material_Code + ' '), LEN([tbl_REPO_Log_Details].Material_Code))   AS Material_Code2,[tbl_REPO_Log_Details].Description as [Material Description],[tbl_REPO_Log_Details].Qty as [ASN Qty], [tbl_RPO_GE_Master].[GE_NO] as [GE Number],[tbl_RPO_GR_Master].GR_NO as [GR Number],dbo.VT_GetDateOnly([tbl_RPO_GR_Master].ModifiedDateTime) as [GR Date],Convert(varchar(100),[tbl_RPO_GR_Details].Quantity) as [GR Qty],[tbl_RPO_GR_Master].[isShortGR],[tbl_RPO_GR_Master].PO_Number as [PO Number] from [tbl_REPO_Log_Details],[tbl_REPO_Log_Master],[tbl_RPO_GR_Master],[tbl_RPO_GR_Details], [tbl_RPO_GE_Master], ET_GR_DATA
where [tbl_REPO_Log_Master].DocID=[tbl_REPO_Log_Details].PO_DocId
and [tbl_REPO_Log_Master].Is_Cancelled <> 'Y'
and [tbl_REPO_Log_Master].Vendor_Code=@Vendor
and [tbl_REPO_Log_Details].Plant_Code=@Plant
and [tbl_REPO_Log_Master].Doc_Date BETWEEN @FromDate AND @ToDate
and [tbl_RPO_GR_Master].ASN_Number=[tbl_REPO_Log_Master].ASN_Number
and [tbl_RPO_GR_Master].DocID=[tbl_RPO_GR_Details].PO_DocId
and [tbl_RPO_GE_Master].GE_NO =ET_GR_DATA.[GE_NUMBER]
AND tbl_RPO_GR_Master.GR_NO =ET_GR_DATA.[GR_NUMBER]
AND tbl_RPO_GR_Master.GR_NO IN (SELECT GR_NUMBER FROM ET_GR_DATA)
)
UNION
(
(select  [tbl_REPO_Log_Master].ASN_Number as [ASN Number],[tbl_REPO_Log_Master].Delivery_note_number as [Invoice Number],dbo.VT_GetDateOnly([tbl_REPO_Log_Master].Doc_Date) as [Invoice Date],[tbl_REPO_Log_Details].[Sr_number] as [Line Item],[tbl_REPO_Log_Details].Material_Code as [Material Code],SUBSTRING([tbl_REPO_Log_Details].Material_Code, PATINDEX('%[^0 ]%', [tbl_REPO_Log_Details].Material_Code + ' '), LEN([tbl_REPO_Log_Details].Material_Code))   AS Material_Code2 ,[tbl_REPO_Log_Details].Description as [Material Description],[tbl_REPO_Log_Details].Qty as [ASN Qty],NULL as [GE Number], NULL as [GR Number],NULL as [GR Date],NULL as [GR Qty],NULL as [isShortGR],[tbl_REPO_Log_Master].PO_Number as [PO Number] from [tbl_REPO_Log_Details],[tbl_REPO_Log_Master]
where [tbl_REPO_Log_Master].DocID=[tbl_REPO_Log_Details].PO_DocId
and [tbl_REPO_Log_Master].Is_Cancelled <> 'Y'
and [tbl_REPO_Log_Master].Vendor_Code=@Vendor
and [tbl_REPO_Log_Details].Plant_Code=@Plant
and [tbl_REPO_Log_Master].Doc_Date BETWEEN @FromDate AND @ToDate
AND [tbl_REPO_Log_Master].ASN_Number NOT IN (SELECT [tbl_RPO_GR_Master].ASN_Number FROM [tbl_RPO_GR_Master])
)
UNION
(
select  [tbl_REPO_Log_Master].ASN_Number as [ASN Number],[tbl_REPO_Log_Master].Delivery_note_number as [Invoice Number],dbo.VT_GetDateOnly([tbl_REPO_Log_Master].Doc_Date) as [Invoice Date],[tbl_REPO_Log_Details].[Sr_number] as [Line Item],[tbl_REPO_Log_Details].Material_Code as [Material Code], SUBSTRING([tbl_REPO_Log_Details].Material_Code, PATINDEX('%[^0 ]%', [tbl_REPO_Log_Details].Material_Code + ' '), LEN([tbl_REPO_Log_Details].Material_Code))   AS Material_Code2,[tbl_REPO_Log_Details].Description as [Material Description],[tbl_REPO_Log_Details].Qty as [ASN Qty],NULL as [GE Number],NULL as [GR Number],NULL as [GR Date],NULL as [GR Qty],NULL as [isShortGR],[tbl_REPO_Log_Master].PO_Number as [PO Number] from [tbl_REPO_Log_Details],[tbl_REPO_Log_Master],[tbl_RPO_GR_Master],[tbl_RPO_GR_Details]
where [tbl_REPO_Log_Master].DocID=[tbl_REPO_Log_Details].PO_DocId
and [tbl_REPO_Log_Master].Is_Cancelled <> 'Y'
and [tbl_REPO_Log_Master].Vendor_Code=@Vendor
and [tbl_REPO_Log_Details].Plant_Code=@Plant
and [tbl_REPO_Log_Master].Doc_Date BETWEEN @FromDate AND @ToDate
and [tbl_RPO_GR_Master].ASN_Number=[tbl_REPO_Log_Master].ASN_Number
and [tbl_RPO_GR_Master].DocID=[tbl_RPO_GR_Details].PO_DocId
AND (tbl_RPO_GR_Master.GR_NO NOT IN (SELECT GR_NUMBER FROM ET_GR_DATA)
or tbl_RPO_GR_Master.Status='SCAN')
)
UNION
(
select  [tbl_REPO_Log_Master].ASN_Number as [ASN Number],[tbl_REPO_Log_Master].Delivery_note_number as [Invoice Number],dbo.VT_GetDateOnly([tbl_REPO_Log_Master].Doc_Date) as [Invoice Date],[tbl_REPO_Log_Details].[Sr_number] as [Line Item],[tbl_REPO_Log_Details].Material_Code as [Material Code], SUBSTRING([tbl_REPO_Log_Details].Material_Code, PATINDEX('%[^0 ]%', [tbl_REPO_Log_Details].Material_Code + ' '), LEN([tbl_REPO_Log_Details].Material_Code))   AS Material_Code2,[tbl_REPO_Log_Details].Description as [Material Description],[tbl_REPO_Log_Details].Qty as [ASN Qty],NULL as [GE Number],NULL as [GR Number],NULL as [GR Date],NULL as [GR Qty],NULL as [isShortGR],[tbl_REPO_Log_Master].PO_Number as [PO Number] from [tbl_REPO_Log_Details],[tbl_REPO_Log_Master],[tbl_RPO_GR_Master],[tbl_RPO_GR_Details],[tbl_RPO_GE_Master]
where [tbl_REPO_Log_Master].DocID=[tbl_REPO_Log_Details].PO_DocId
and [tbl_REPO_Log_Master].Is_Cancelled <> 'Y'
and [tbl_REPO_Log_Master].Vendor_Code=@Vendor
and [tbl_REPO_Log_Details].Plant_Code=@Plant
and [tbl_REPO_Log_Master].Doc_Date BETWEEN @FromDate AND @ToDate
and [tbl_RPO_GR_Master].ASN_Number=[tbl_REPO_Log_Master].ASN_Number
and [tbl_RPO_GR_Master].DocID=[tbl_RPO_GR_Details].PO_DocId
AND ([tbl_RPO_GE_Master].GE_NO NOT IN (SELECT [GE_NUMBER] FROM ET_GR_DATA)
or tbl_RPO_GR_Master.Status='SCAN')
)
)
