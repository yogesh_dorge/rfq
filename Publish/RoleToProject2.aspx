﻿<%@ page language="C#" autoeventwireup="true" masterpagefile="~/Site.master" inherits="RoleToProject, App_Web_p5j1juhm" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">--%>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<link href="Styles/Site.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<%--<head runat="server">
    <title></title>
    <link href="Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            height: 25px;
        }
    </style>
</head>--%>

<center>
    <form id="form1" runat="server">
    <div id ="FormDiv" class="divForm">
        
        <table class="style1">
            <tr>
                <td>
                    &nbsp;</td>
                <td style="text-align: right">
                    Project</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="dlProject" runat="server" DataTextField="ProjectName" 
                        DataValueField="ProjectId" Height="16px" Width="130px">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    </td>
                <td class="style2">
                    </td>
                <td class="style2">
                    </td>
                <td class="style2">
                    </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td colspan="2" style="text-align: left">
                    Select Roles:</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td colspan="2">
    
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
  
    
   <telerik:RadGrid ID="RadGrid1" runat="server"  AutoGenerateColumns="False" CellSpacing="0" 
                        GridLines="None" onneeddatasource="RadGrid1_NeedDataSource">
                <ClientSettings>
                <Selecting AllowRowSelect="True" />
<Selecting CellSelectionMode="None" AllowRowSelect="True"></Selecting>
            </ClientSettings>
            <MasterTableView DataKeyNames="RoleCode">
                <CommandItemSettings ExportToPdfText="Export to PDF" />
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridClientSelectColumn FilterControlAltText="Filter column column" 
                        UniqueName="column">
                    </telerik:GridClientSelectColumn>
                    <telerik:GridBoundColumn DataField="RoleCode" EmptyDataText="NA" 
                        FilterControlAltText="Filter PlantOrMine column" HeaderText="Role Code" 
                        ReadOnly="True" UniqueName="RoleCode">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="RoleDescription" EmptyDataText="NA" 
                        FilterControlAltText="Filter Shift1 column" HeaderText="Role Description" 
                        UniqueName="RoleDescription">
                    </telerik:GridBoundColumn>
                  
                </Columns>
               
<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
               
            </MasterTableView>
            
<FilterMenu EnableImageSprites="False"></FilterMenu>
            
</telerik:RadGrid>

                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        
    </div>
    </form>
    </center>

</asp:Content>
<%--</html>--%>
